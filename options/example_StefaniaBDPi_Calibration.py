OS_Muon_Eta       = 0.29258
OS_Muon_P0        = 0.00790061
OS_Muon_P1        = 0.98408
OS_Muon_P0Err     = 0.001728
OS_Muon_P1Err     = 0.0184992
OS_Muon_RhoP0P1   = 0.156009

OS_Electron_Eta       = 0.288992
OS_Electron_P0        = 0.0182111
OS_Electron_P1        = 1.06854
OS_Electron_P0Err     = 0.00323765
OS_Electron_P1Err     = 0.0492065
OS_Electron_RhoP0P1   = 0.11461

OS_Kaon_Eta       = 0.394109
OS_Kaon_P0        = -0.000277273
OS_Kaon_P1        = 1.00706
OS_Kaon_P0Err     = 0.00121893
OS_Kaon_P1Err     = 0.0200759
OS_Kaon_RhoP0P1   = 0.0526556

OS_nnetKaon_Eta       = 0.427856
OS_nnetKaon_P0        = 0.00457333
OS_nnetKaon_P1        = 0.956835
OS_nnetKaon_P0Err     = 0.000810315
OS_nnetKaon_P1Err     = 0.0132431
OS_nnetKaon_RhoP0P1   = 0.0415676

VtxCharge_Eta       = 0.3878
VtxCharge_P0        = 0.00432906
VtxCharge_P1        = 1.01203
VtxCharge_P0Err     = 0.00116288
VtxCharge_P1Err     = 0.0182979
VtxCharge_RhoP0P1   = 0.054327

OS_Charm_Eta       = 0.334168
OS_Charm_P0        = 0.0122511
OS_Charm_P1        = 0.781309
OS_Charm_P0Err     = 0.00304735
OS_Charm_P1Err     = 0.0676311
OS_Charm_RhoP0P1   = 0.0428288

SS_Kaon_Eta       = 0.344116
SS_Kaon_P0        = 0
SS_Kaon_P1        = 1
SS_Kaon_P0Err     = 0
SS_Kaon_P1Err     = 0
SS_Kaon_RhoP0P1   = -nan

SS_nnetKaon_Eta       = 0.439773
SS_nnetKaon_P0        = 0.00729293
SS_nnetKaon_P1        = 0.861737
SS_nnetKaon_P0Err     = 0.000694819
SS_nnetKaon_P1Err     = 0.010784
SS_nnetKaon_RhoP0P1   = 0.0417523

SS_Pion_Eta       = 0.374847
SS_Pion_P0        = 0.0306061
SS_Pion_P1        = 1.03345
SS_Pion_P0Err     = 0.00118357
SS_Pion_P1Err     = 0.0225579
SS_Pion_RhoP0P1   = 0.051225

