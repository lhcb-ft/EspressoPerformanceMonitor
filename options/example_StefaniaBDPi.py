########
# DATA #
########

# This is the file/directory that you want to run
# Multiple files can be specified by setting NumFiles = N
# and then setting RootFile_1, RootFile_2, ..., RootFile_N
  
RootFile = "root://eoslhcb.cern.ch//eos/lhcb/wg/FlavourTagging/tuples/calibration/data/BuDPi_stefania/MergedTree_Bu2D0Pi_2011e2012_v33r9_ALL_newSStagger_cutsANDweights_LHCb_2014_ANA_003.root"
TupleName = "DecayTree"
Selection = ""
Nmax  = 50000  # Events to run, -1 means all

########################
# CALIBRATION SETTINGS #
########################

CalibrationMode = "Bu"
DoCalibrations = 1
CalibrationLink = "MISTAG"
CalibrationDegree = 1
CalibrationModel = "POLY"
UseNewtonRaphson = 1

##########################
# BRANCH NAMES AND TYPES #
##########################

BranchID             = "lab0_ID"
UseWeight            = 1
WeightFormula        = "N_sig_sw"
UseTau  = 0
UseTauErr = 0

OS_Muon_Use = 1
OS_Muon_TypeDec          = "Short_t"
OS_Muon_BranchDec        = "lab0_OS_Muon_DEC"
OS_Muon_TypeProb        = "Float_t"
OS_Muon_BranchProb      = "lab0_OS_Muon_PROB"
OS_Electron_Use = 1
OS_Electron_TypeDec      = "Short_t"
OS_Electron_BranchDec    = "lab0_OS_Electron_DEC"
OS_Electron_TypeProb    = "Float_t"
OS_Electron_BranchProb  = "lab0_OS_Electron_PROB"
OS_nnetKaon_Use = 1
OS_nnetKaon_TypeDec        = "Short_t"
OS_nnetKaon_BranchDec      = "lab0_OS_nnetKaon_DEC"
OS_nnetKaon_TypeProb      = "Float_t"
OS_nnetKaon_BranchProb    = "lab0_OS_nnetKaon_PROB"
VtxCharge_Use = 1
VtxCharge_TypeDec     = "Short_t"
VtxCharge_BranchDec   = "lab0_VtxCharge_DEC"
VtxCharge_TypeProb   = "Float_t"
VtxCharge_BranchProb = "lab0_VtxCharge_PROB"
SS_nnetKaon_Use = 1
SS_nnetKaon_TypeDec      = "Short_t"
SS_nnetKaon_BranchDec    = "lab0_SS_nnetKaon_DEC"
SS_nnetKaon_TypeProb    = "Float_t"
SS_nnetKaon_BranchProb  = "lab0_SS_nnetKaon_PROB"
SS_Pion_Use = 1
SS_Pion_TypeDec      = "Short_t"
SS_Pion_BranchDec      = "lab0_SS_Pion_DEC"
SS_Pion_TypeProb      = "Float_t"
SS_Pion_BranchProb    = "lab0_SS_Pion_PROB"

######################
# CALIBRATION VALUES #
######################

#import example_StefaniaBDPi_Calibration.py
