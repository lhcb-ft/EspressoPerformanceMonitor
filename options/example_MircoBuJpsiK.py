########
# DATA #
########

# This is the file/directory that you want to run
# Multiple files can be specified by setting NumFiles = N
# and then setting RootFile_1, RootFile_2, ..., RootFile_N

RootFile = "root://eoslhcb.cern.ch//eos/lhcb/wg/FlavourTagging/tuples/calibration/data/BuJpsiK_mirco/BuJpsiK_2011and2012_3gaussMass.root"
TupleName = "DecayTree"
Selection = ""
Nmax = -1  # Events to run, -1 means all

########################
# CALIBRATION SETTINGS #
########################

CalibrationMode = "Bu"
DoCalibrations = 1
CalibrationLink = "MISTAG"
CalibrationDegree = 1
CalibrationModel = "POLY"
UseNewtonRaphson = 1

##########################
# BRANCH NAMES AND TYPES #
##########################

BranchID             = "B_id"
UseWeight            = 1
WeightFormula        = "sWeight"
UseTau  = 1
TypeTau = "Double_t"
BranchTau = "time"
UseTauErr = 0
Combination_Use              = 0
OS_Combination_Use            = 1
OS_Combination_BranchDec      = "tagdecision_os_wCBK"
OS_Combination_BranchProb    = "tagomega_os_wCBK"

######################
# CALIBRATION VALUES #
######################

#import example_MircoBuJpsiK_Calibration.py
