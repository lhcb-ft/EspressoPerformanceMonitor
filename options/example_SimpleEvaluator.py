########
# DATA #
########

# This is the file/directory that you want to run
# Multiple files can be specified by setting NumFiles = N
# and then setting RootFile_1, RootFile_2, ..., RootFile_N

RootFile = "root://eoslhcb.cern.ch//eos/lhcb/wg/FlavourTagging/tuples/calibration/data/flavour/BuJpsiK/2012/Up/DTT.root"
TupleName = "BTuple/DecayTree"
Selection   = "" # Selection to apply, i.e. none
Nmax = -1  # Events to run, -1 means all

########################
# CALIBRATION SETTINGS #
########################

CalibrationMode = "Bu"

##########################
# BRANCH NAMES AND TYPES #
##########################

BranchID             = "Bplus_ID"
UseWeight = 0
UseTau  = 0
UseTauErr = 0
OS_Muon_TypeDec          = "Short_t"
OS_Muon_BranchDec        = "Bplus_OS_Muon_DEC"
OS_Muon_TypeProb        = "Float_t"
OS_Muon_BranchProb      = "Bplus_OS_Muon_PROB"
OS_Electron_TypeDec      = "Short_t"
OS_Electron_BranchDec    = "Bplus_OS_Electron_DEC"
OS_Electron_TypeProb    = "Float_t"
OS_Electron_BranchProb  = "Bplus_OS_Electron_PROB"
OS_Kaon_TypeDec          = "Short_t"
OS_Kaon_BranchDec        = "Bplus_OS_Kaon_DEC"
OS_Kaon_TypeProb        = "Float_t"
OS_Kaon_BranchProb      = "Bplus_OS_Kaon_PROB"
OS_nnetKaon_TypeDec        = "Short_t"
OS_nnetKaon_BranchDec      = "Bplus_OS_nnetKaon_DEC"
OS_nnetKaon_TypeProb      = "Float_t"
OS_nnetKaon_BranchProb    = "Bplus_OS_nnetKaon_PROB"
VtxCharge_TypeDec     = "Short_t"
VtxCharge_BranchDec   = "Bplus_VtxCharge_DEC"
VtxCharge_TypeProb   = "Float_t"
VtxCharge_BranchProb = "Bplus_VtxCharge_PROB"
OS_Charm_TypeDec         = "Short_t"
OS_Charm_BranchDec       = "Bplus_OS_Charm_DEC"
OS_Charm_TypeProb       = "Float_t"
OS_Charm_BranchProb     = "Bplus_OS_Charm_PROB"
SS_Kaon_TypeDec        = "Short_t"
SS_Kaon_BranchDec      = "Bplus_SS_Kaon_DEC"
SS_Kaon_TypeProb      = "Float_t"
SS_Kaon_BranchProb    = "Bplus_SS_Kaon_PROB"
SS_nnetKaon_TypeDec      = "Short_t"
SS_nnetKaon_BranchDec    = "Bplus_SS_nnetKaon_DEC"
SS_nnetKaon_TypeProb    = "Float_t"
SS_nnetKaon_BranchProb  = "Bplus_SS_nnetKaon_PROB"
SS_Pion_TypeDec        = "Short_t"
SS_Pion_BranchDec      = "Bplus_SS_Pion_DEC"
SS_Pion_TypeProb      = "Float_t"
SS_Pion_BranchProb    = "Bplus_SS_Pion_PROB"
SS_PionBDT_TypeDec     = "Short_t"
SS_PionBDT_BranchDec   = "Bplus_SS_PionBDT_DEC"
SS_PionBDT_TypeProb   = "Float_t"
SS_PionBDT_BranchProb = "Bplus_SS_PionBDT_PROB"
SS_Proton_TypeDec      = "Short_t"
SS_Proton_BranchDec    = "Bplus_SS_Proton_DEC"
SS_Proton_TypeProb    = "Float_t"
SS_Proton_BranchProb  = "Bplus_SS_Proton_PROB"
