########
# DATA #
########

# This is the file/directory that you want to run
# Multiple files can be specified by setting NumFiles = N
# and then setting RootFile_1, RootFile_2, ..., RootFile_N
  
RootFile = "root://eoslhcb.cern.ch//eos/lhcb/wg/FlavourTagging/tuples/calibration/data/BuDPi_stefania/MergedTree_Bu2D0Pi_2011e2012_v33r9_ALL_newSStagger_cutsANDweights_LHCb_2014_ANA_003.root"
TupleName = "DecayTree"
Selection = "!(lab0_L0HadronDecision_TIS==1&&lab0_Hlt1TrackAllL0Decision_TIS==1&&(lab0_Hlt2Topo2BodyBBDTDecision_TIS==1||lab0_Hlt2Topo3BodyBBDTDecision_TIS==1||lab0_Hlt2Topo4BodyBBDTDecision_TIS==1))&&!(lab0_L0HadronDecision_TOS==1&&lab0_Hlt1TrackAllL0Decision_TOS==1&&(lab0_Hlt2Topo2BodyBBDTDecision_TOS==1||lab0_Hlt2Topo3BodyBBDTDecision_TOS==1||lab0_Hlt2Topo4BodyBBDTDecision_TOS==1))"
Nmax        = -1  # Events to run, -1 means all

########################
# CALIBRATION SETTINGS #
########################

CalibrationMode = "Bu"

##########################
# BRANCH NAMES AND TYPES #
##########################

BranchID             = "lab0_ID"
UseWeight            = 1
WeightFormula        = "N_sig_sw"
UseTau  = 1
BranchTau = "lab0_TAU"
UseTauErr = 0

OS_Combination_Use = 1
OS_Combination_BranchDec = "lab0_TAGDECISION_OS"
OS_Combination_BranchProb = "lab0_TAGOMEGA_OS"

######################
# CALIBRATION VALUES #
######################

#import example_StefaniaBDPi_Calibration.py
