########
# DATA #
########

# This is the file/directory that you want to run
# Multiple files can be specified by setting NumFiles = N
# and then setting RootFile_1, RootFile_2, ..., RootFile_N

RootFile = "root://eoslhcb.cern.ch//eos/lhcb/wg/FlavourTagging/tuples/calibration/data/BuDPi_stefania/MergedTree_Bu2D0Pi_2011e2012_v33r9_ALL_newSStagger_cutsANDweights_LHCb_2014_ANA_003.root"
TupleName = "DecayTree"
Nmax = -1  # Events to run, -1 means all

########################
# CALIBRATION SETTINGS #
########################

CalibrationMode = "Bu"
DoCalibrations = 1
CalibrationLink = "LOGIT"
CalibrationDegree = 2
CalibrationModel = "NSPLINE"
UseNewtonRaphson = 0 #use MINUIT

##########################
# PLOTTING               #
##########################

PlotLabel = "LHCb"
PlotTitle = 0
PlotExtension = ".pdf"
PlotStatBox = 0

##########################
# BRANCH NAMES AND TYPES #
##########################

BranchID             = "lab0_ID"
UseWeight            = 1
WeightFormula        = "N_sig_sw"

#Standard OS combination (OSe+OSmu+OSK+Vtx)
OS_Combination_Use            = 1
OS_Combination_BranchDec      = "lab0_TAGDECISION_OS"
OS_Combination_TypeDec       = "Int_t"
OS_Combination_BranchProb    = "lab0_TAGOMEGA_OS"
OS_Combination_TypeProb      = "Double_t"

#SSPionBDT
SS_PionBDT_Use           = 1
SS_PionBDT_BranchDec     = "lab0_SS_Pion_DEC"
SS_PionBDT_TypeDec       = "Short_t"
SS_PionBDT_BranchProb    = "lab0_SS_Pion_PROB"
SS_PionBDT_TypeProb      = "Float_t"

##########################
# I/O                    #
# (uncomment at step 1)  #
##########################

Selection = "eventNumber%2==0"
SaveCalibrationsToXML = 1

##########################
# I/O                    #
# (uncomment at step 2)  #
##########################

#Combine OS and SS
#PerformOfflineCombination_OSplusSS  = 1
#OS_Combination_InComb               = 1
#SS_PionBDT_InComb                   = 1

#Take other half, select input calibration
#Selection = "eventNumber%2!=0"
#OS_Combination_CalibrationArchive = "OS_Combination_Calibration.xml"
#SS_PionBDT_CalibrationArchive = "SS_PionBDT_Calibration.xml"
