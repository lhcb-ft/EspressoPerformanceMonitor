########
# DATA #
########

# This is the file/directory that you want to run
# Multiple files can be specified by setting NumFiles = N
# and then setting RootFile_1, RootFile_2, ..., RootFile_N

NumFiles = 2
RootFile_1 = "root://eoslhcb.cern.ch//eos/lhcb/wg/FlavourTagging/tuples/development/data/B2OC/sWeights/Bs2DsPi/DTT_2011_Reco14Strip21r1_BHADRONCOMPLETEEVENT_merged_sW.root"
RootFile_2 = "root://eoslhcb.cern.ch//eos/lhcb/wg/FlavourTagging/tuples/development/data/B2OC/sWeights/Bs2DsPi/DTT_2012_Reco14Strip21_BHADRONCOMPLETEEVENT_merged_sW.root"
TupleName = "DecayTree"
Nmax = -1  # Events to run, -1 means all

########################
# CALIBRATION SETTINGS #
########################

CalibrationMode = "Bs"
DoCalibrations = 1
CalibrationLink = "MISTAG"
CalibrationDegree = 1
CalibrationModel = "POLY"
UseNewtonRaphson = 0 #use MINUIT

##########################
# PLOTTING               #
##########################

PlotLabel = "LHCb"
PlotTitle = 0
PlotExtension = ".pdf"
PlotStatBox = 0

##########################
# DECAY TIME             #
##########################

#Resolution
UseTau                    = 1
UseTauErr                 = 1
ResolutionGaussian1_A     = 1.0262e-05
ResolutionGaussian1_B     = 1.28
TauUnits                  = "ns"
BranchTau                 = "lab0_TAU"
BranchTauErr              = "lab0_TAUERR"
DrawOscillationPlots      = 1

#Physics (all in ps, ps^-1 etc... always!!!)
DeltaM = 17.761
Lifetime = 1.512
DeltaGamma = 0.0913

##########################
# BRANCH NAMES AND TYPES #
##########################

BranchID             = "lab0_ID"
UseWeight            = 1
WeightFormula        = "nsig_sw"

#SSKaonNNet
SS_nnetKaon_Use           = 1
SS_nnetKaon_BranchDec     = "lab0_SS_nnetKaon_DEC"
SS_nnetKaon_BranchProb    = "lab0_SS_nnetKaon_PROB"
