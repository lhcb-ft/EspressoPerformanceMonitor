#include <iostream>
#include <fstream>
#include <map>

#include <TString.h>
#include <TFile.h>
#include <TTree.h>
#include <TEventList.h>
#include <TTreeFormula.h>
#include <TBranch.h>
#include <TF1.h>
#include <TH1F.h>
#include <TCanvas.h>

#include "globals.hh"
#include "Tagger.hh"
#include "CombinationAlgorithms.hh"
#include "EspressoPerformanceMonitor.hh"
#include "RandomNumberGeneration.hh"

#include "ToyTagger.hh"
#include "SumDistribution.hh"
#include "UniformDistribution.hh"
#include "GLMCalibration.hh"
#include "TaggingResultMetrics.hh"
#include "TaggingResultGraphics.hh"

#include "TagCollector.hh"

template <class TI, class TF>
double prob(TI dec, TF eta) {
  TF prob;
  if (dec == 0) {
    prob = 0.5;
  } else if (dec > 0) {
    prob = 1-eta;
  } else if (dec < 0) {
    prob = eta;
  } else {
    prob = 0.5;
  }
  return prob;
}

const bool enable_floatingcal_branches = false;

void CalibrationTest(const EspressoPerformanceMonitor& perfmon) {

  int degree = Config::CalibrationDegree;
  GLMModel::GLMType glmmodel = Config::CalibrationModel;
  Regression::LinkType link = Config::CalibrationLink;
  int numToys = Config::NumberOfToys;
  int numEventsPerToy = Config::EventsPerToy;
  bool verbose = Config::DBGLEVEL > 3;

  // SET PREDICTION DISTRIBUTION
  const double min = 0.15;
  const double max = 0.45;
  UniformDistribution pdf(min,max);
  const TaggingResult& toydist = perfmon.GetTaggingResult(Tagger::Toy);
  bool use_tr_dist = Tagger::IsActive(Tagger::Toy)
    and (toydist.GetNumTaggedEvents() > 0);

  // Get calibration
  std::shared_ptr<Espresso::Calibration> smcal;
  std::vector<double> smearCoeffs;
  std::vector<double> smearDeltaCoeffs;
  
  if (not Config::CreateSmearedCalibration) {
    smcal = Tagger::Calibration(Tagger::Toy);
  } else {
    int smdegree = Config::ToySmearDegree;
    GLMModel::GLMType smmodel = Config::ToySmearModel;
    Regression::LinkType smlink = Config::ToySmearLink;
    
    if (Config::CalibrationTest) {
      if (link != smlink) {
	std::cout << "WARNING: Calibration comparison where smear link != calibration link" << std::endl;
      }
    }

    std::unique_ptr<GLMModel> psmglm = (use_tr_dist)
      ? GLMModel::CreateModel(smmodel,smdegree,toydist,smlink)
      : GLMModel::CreateModel(smmodel,smdegree,pdf,smlink);

    // GET CONFIGURED SMEAR QUANTITIES
    std::vector<double> smearRatios = {
      Config::ToySmearP0Scale,
      Config::ToySmearP1Scale,
      Config::ToySmearP2Scale,
      Config::ToySmearP3Scale,
      Config::ToySmearP4Scale,
      Config::ToySmearP5Scale,
      Config::ToySmearP6Scale,
      Config::ToySmearP7Scale,
      Config::ToySmearP8Scale,
      Config::ToySmearP9Scale,
      Config::ToySmearP10Scale
    };

    std::vector<double> smearDeltaRatios = {
      Config::ToySmearDeltaP0Scale,
      Config::ToySmearDeltaP1Scale,
      Config::ToySmearDeltaP2Scale,
      Config::ToySmearDeltaP3Scale,
      Config::ToySmearDeltaP4Scale,
      Config::ToySmearDeltaP5Scale,
      Config::ToySmearDeltaP6Scale,
      Config::ToySmearDeltaP7Scale,
      Config::ToySmearDeltaP8Scale,
      Config::ToySmearDeltaP9Scale,
      Config::ToySmearDeltaP10Scale
    };

    // SET SMEAR FUNCTION
    // Estimate Cramer-Rao sensitivity
    Matrix tbasis = (use_tr_dist)
      ? OrthonormalBasis(toydist,*psmglm,smlink)
      : OrthonormalBasis(pdf,*psmglm,smlink);
    for (int k = 0; k < psmglm->NumParams(); k++) {
      double cramerRao = tbasis(k,k)/sqrt(Config::EventsPerToy);
      std::cout << "CRAMER RAO BOUND for P" << k << " = " << cramerRao << std::endl;
      double psmear = cramerRao*smearRatios[k];
      std::cout << "SETTING P" << k << " TO " << psmear << std::endl;
      smearCoeffs.push_back(psmear);
      std::cout << "CRAMER RAO BOUND for ΔP" << k << " = " << 2*cramerRao << std::endl;
      double dpsmear = 2*cramerRao*smearDeltaRatios[k];
      std::cout << "SETTING ΔP" << k << " TO " << dpsmear << std::endl;
      smearDeltaCoeffs.push_back(dpsmear);
    }
    
    Vector smearCoeffsVec(smearCoeffs);
    Vector smearDeltaCoeffsVec(smearDeltaCoeffs);
    smcal = std::make_shared<GLMCalibration>(*psmglm,smlink,smearCoeffsVec,smearDeltaCoeffsVec);
  }

  // Save graph of smear calibration
  auto graph = graphCalibration(*smcal,"ToySmearCalibration","Toy input mis-calibration",min,max);
  printCurve(graph);

  std::cout << "-----------------------------" << std::endl;
  std::cout << "----- SMEAR CALIBRATION -----" << std::endl;
  std::cout << "-----------------------------" << std::endl;
  std::cout << *smcal << std::endl;

  // CREATE TOY TAGGER
  BDecayModel model(Config::CalibrationMode);
  if(Config::DeltaM>0) { model.SetDeltaM( Config::DeltaM ); }
  if(Config::DeltaMErr>0) { model.SetDeltaMErr( Config::DeltaMErr ); }
  if(Config::Lifetime>0) { model.SetLifetime( Config::Lifetime ); }
  if(Config::LifetimeErr>0) { model.SetLifetimeErr( Config::LifetimeErr ); }
  if(Config::DeltaGamma>0) { model.SetDeltaGamma( Config::DeltaGamma ); }
  if(Config::DeltaGammaErr>0) { model.SetDeltaGammaErr( Config::DeltaGammaErr ); }
  model.SetAsymmetry(Config::ToyAsymmetry);
  Function acceptance = [] (double tau) -> double {
    double z = 1.0 - exp(-tau/0.1);
    return z;
  };
  //  model.SetAcceptanceFunc(acceptance);
  ResolutionModel resModel = [] (double tau, double tauerr) {
    auto pdf1 = std::make_unique<NormalDistribution>(tau,tauerr);
    auto pdf2 = std::make_unique<NormalDistribution>(tau,1.1*tauerr);
    auto res = std::make_unique<SumDistribution>(std::move(pdf1),std::move(pdf2),0.75,0.25);
    return res;
  };
  // model.SetResolutionModel(resModel);

  std::unique_ptr<Espresso::ToyTagger> tt;
  if (use_tr_dist) {
    tt = std::make_unique<Espresso::ToyTagger>(toydist);
  } else {
    tt = std::make_unique<Espresso::ToyTagger>(model,std::move(pdf),smcal.get());
    tt->SetBackgroundFraction(Config::BackgroundFraction);
    tt->SetSidebandRatio(Config::SidebandRatio);
    tt->ForgetResolution(false); // a lifetime resolution test
  }
  tt->SetVerbose(verbose);

  // PERFORM TOY STUDY
  TH1F* uG2_hist = new TH1F("uG2","uG2",100,-5,5);
  TH1F* uX2_hist = new TH1F("uX2","uX2",100,-5,5);
  TH1F* uCR_hist = new TH1F("uCR","uCR",100,-5,5);
  TH1F* uS_hist = new TH1F("uS","uS",100,-5,5);
  TH1F* uG2_fit_hist = new TH1F("uG2_fit","uG2_fit",100,-5,5);
  TH1F* uX2_fit_hist = new TH1F("uX2_fit","uX2_fit",100,-5,5);
  TH1F* uCR_fit_hist = new TH1F("uCR_fit","uCR_fit",100,-5,5);
  TH1F* uS_fit_hist = new TH1F("uS_fit","uS_fit",100,-5,5);

  TH1F* chi2_hist = new TH1F("cal_chi2","cal_chi2",100,0,30);

  std::vector<TH1F*> pulls_hist(degree+1,NULL);
  std::vector<TH1F*> delta_pulls_hist(degree+1,NULL);
  if (Config::CalibrationTest) {
    for (int k = 0; k <= degree; k++) {
      char name[200]; sprintf(name,"pull_%d",k);
      pulls_hist[k] = new TH1F(name,name,100,-5,5);
    }
    for (int k = 0; k <= degree; k++) {
      char name[200]; sprintf(name,"delta_pull_%d",k);
      delta_pulls_hist[k] = new TH1F(name,name,100,-5,5);
    }
  }

  std::ofstream asymm_table, fit_asymm_table, gof_table,fit_gof_table,cal_table;
  if (Config::AsymmetryNullTest) {
    asymm_table.open("AsymmetryTable.csv");
    asymm_table << "Asymmetry, ";
    asymm_table << "Error, ";
    asymm_table << "Pull\n";
  }
  if (Config::AsymmetryFittedTest) {
    fit_asymm_table.open("FittedAsymmetryTable.csv");
    fit_asymm_table << "Asymmetry, ";
    fit_asymm_table << "Error, ";
    fit_asymm_table << "Pull\n";
  }
  if (Config::MetricNullTest) {
    gof_table.open("GoodnessOfFitTable.csv");
    gof_table << "Ungrouped Deviance G2, ";
    gof_table << "Ungrouped Pearson X2, ";
    gof_table << "Ungrouped Cressie-Read, ";
    gof_table << "Ungrouped S\n";
  }
  if (Config::MetricFittedTest) {
    fit_gof_table.open("FittedGoodnessOfFitTable.csv");
    fit_gof_table << "Ungrouped Deviance G2, ";
    fit_gof_table << "Ungrouped Pearson X2, ";
    fit_gof_table << "Ungrouped Cressie-Read, ";
    fit_gof_table << "Ungrouped S\n";
  }
  if (Config::CalibrationTest) {
    cal_table.open("CalibrationPullTable.csv");
    cal_table << "Chi2, ";
    for (int k = 0; k <= degree; k++) {
      char name[200]; sprintf(name,"pull_%d",k);
      cal_table << name << ", ";
    }
    for (int k = 0; k <= degree; k++) {
      char name[200]; sprintf(name,"delta_pull_%d",k);
      cal_table << name;
      if (k != degree)
	cal_table << ", ";
      else
	cal_table << "\n";
    }
  }

  // TOY STUDY
  std::cout << std::endl;
  std::cout << "--------------------" << std::endl;
  std::cout << "BEGINNING TOY STUDY:" << std::endl;
  std::cout << "--------------------" << std::endl;

  for (int toy = 0; toy < numToys; toy++) {

    // TICKER
    if (toy % 1 == 0) {
      std::cout << std::endl;
      std::cout << " Toy #" << toy << std::endl;
    }

    // SAMPLE
    bool save = (toy == 0);
    TaggingResult tag = tt->GetToyTaggingResult(numEventsPerToy,save);

    //testBSplineCalibration(tag);

    // SOLVE
    Matrix rbasis(2,2);
    rbasis.SetIdentity();

    if (Config::AsymmetryNullTest) {
      double asymm, err;
      calcAsymmetry(asymm,err,tag.GetTagTable(),nullptr);
      asymm_table << asymm << ", ";
      asymm_table << err << ", ";
      double pull = (asymm-Config::ToyAsymmetry)/err;
      asymm_table << pull << "\n";
    }

    if (Config::MetricNullTest) {

      static bool savedGraph = false;
      if (!savedGraph) {

        auto cdists = std::make_unique<TCanvas>("ToyDistributions","ToyDistributions",800*2,800*2);
        cdists->Divide(2,2);
        cdists->cd(1);
        auto eta = graphEtaDist(tag,"ToyEta","Toy #eta distribution");
        eta->Draw("E");
        cdists->cd(2);
        auto etaR = graphEtaDistRight(tag,"ToyEtaRight","Toy #eta distribution (rightly tagged)");
        etaR->Draw("E");
        cdists->cd(3);
        auto etaW = graphEtaDistWrong(tag,"ToyEtaWrong","Toy #eta distribution (wrongly tagged)");
        etaW->Draw("E");
        cdists->cd(4);
        auto ip = graphIntegratedTaggingPower(tag,"ToyIntPower","Toy Integrated Tagging Power");
        ip->Draw("AL");
        cdists->SaveAs("ToyDistributions.png");
        auto roc = graphROC(tag,"ToyROC","Toy ROC curve");
        printCurve(roc);
        
        TaggingResult::BinTable binnedTable = tag.CreateBinTable();
        auto graph = graphBinTable(binnedTable,"ToyBinTable","Toy: #omega vs #eta",1);
        printGraph(graph);
        
        TaggingResult::BinTable smoothBinnedTable = tag.CreateKernelSmoothedBinTable();
        std::ofstream kernel_table;
        kernel_table.open("KernelSmoothedTable.csv");
        kernel_table << smoothBinnedTable << std::endl;
        kernel_table.close();
        
        printSmoothedBinTable(smoothBinnedTable,"ToySmoothedBinTable","Toy: smoothed #omega vs #eta");
        
        savedGraph = true;
      }

      TaggingResult::BinTable binnedTable = tag.CreateCalibratedBinTable();
      double uG2 = UngroupedDevianceTest(tag);
      double uX2 = UngroupedPearsonTest(tag);
      double uCR = UngroupedCressieReadTest(tag);
      double uS = UngroupedSTest(tag);

      gof_table << uG2 << ", ";
      gof_table << uX2 << ", ";
      gof_table << uCR << ", ";
      gof_table << uS << "\n";

      uG2_hist->Fill(uG2);
      uX2_hist->Fill(uX2);
      uCR_hist->Fill(uCR);
      uS_hist->Fill(uS);
    }

    if (Config::CalibrationTest or Config::MetricFittedTest or Config::AsymmetryFittedTest) {

      std::unique_ptr<GLMModel> pglm = GLMModel::CreateModel(glmmodel,degree,pdf,link);

      auto method = static_cast<TaggingLikelihood::CovarianceCorrectionMethod>(Config::CovCorrMethod);
      GLMCalibration cal = Espresso::TaggingCalibrationCalculator(tag,*pglm,link,method,Config::UseNewtonRaphson,verbose);

      std::cout << "SAVING CALIBRATION... ";
      std::ofstream ofs("ToyCalibrations.xml");
      cal.Serialize("MyToyCalibraiton",ofs);
      std::cout << "DONE!" << std::endl;

      std::cout << "-----------------------------" << std::endl;
      std::cout << "------ FIT CALIBRATION ------" << std::endl;
      std::cout << "-----------------------------" << std::endl;
      std::cout << cal << std::endl;

      static bool savedGraph = false;
      if (!savedGraph) {
        TaggingResult::BinTable binnedTable = tag.CreateBinTable();
        overlayDataAndCalibration(cal,binnedTable,"ToyCalibration","Toy calibration",min,max);
	TaggingResult::BinTable smoothBinnedTable = tag.CreateKernelSmoothedBinTable();
        overlaySmoothedDataAndCalibration(cal,smoothBinnedTable,"ToyCalibration_SmoothedData","Toy calibration (smoothed data)",min,max);
        savedGraph = true;
      }

      if (Config::AsymmetryFittedTest) {
        double asymm, err;
        calcAsymmetry(asymm,err,tag.GetTagTable(),&cal);
        fit_asymm_table << asymm << ", ";
        fit_asymm_table << err << ", ";
        double pull = (asymm-Config::ToyAsymmetry)/err;
        asymm_table << pull << "\n";
      }

      if (Config::MetricFittedTest) {
        tag.ApplyCalibration(cal);

        static bool makePlots = true;
        if (makePlots) {
          makePlots = false;
          auto cdists = std::make_unique<TCanvas>("CalibratedToyDistributions","CalibratedToyDistributions",800*2,800*2);
          cdists->Divide(2,2);
          cdists->cd(1);
          auto eta = graphEtaDist(tag,"CalibratedToyEta","Toy #eta distribution");
          eta->Draw("E");
          cdists->cd(2);
          auto etaR = graphEtaDistRight(tag,"CalibratedToyEtaRight","Toy #eta distribution (rightly tagged)");
          etaR->Draw("E");
          cdists->cd(3);
          auto etaW = graphEtaDistWrong(tag,"CalibratedToyEtaWrong","Toy #eta distribution (wrongly tagged)");
          etaW->Draw("E");
          cdists->cd(4);
          auto ip = graphIntegratedTaggingPower(tag,"CalibratedToyIntPower","Toy Integrated Tagging Power");
          ip->Draw("AL");
          cdists->SaveAs("CalibratedToyDistributions.png");
	  auto roc = graphROC(tag,"CalibratedToyROC","Toy ROC curve");
	  printCurve(roc);
        }
        TaggingResult::BinTable fittedBinnedTable = tag.CreateCalibratedBinTable();

        double uG2 = UngroupedDevianceTest(tag,false,true);
        double uX2 = UngroupedPearsonTest(tag,false,true);
        double uCR = UngroupedCressieReadTest(tag,false,true);
        double uS = UngroupedSTest(tag,false,true);
	
        fit_gof_table << uG2 << ", ";
        fit_gof_table << uX2 << ", ";
        fit_gof_table << uCR << ", ";
        fit_gof_table << uS << "\n";

        uG2_fit_hist->Fill(uG2);
        uX2_fit_hist->Fill(uX2);
        uCR_fit_hist->Fill(uCR);
        uS_fit_hist->Fill(uS);
      }

      if (Config::CalibrationTest) {
        std::vector<double> coeffs(degree+1,0.0);
        std::vector<double> errs(degree+1,0.0);
        std::vector<double> delta_coeffs(degree+1,0.0);
        std::vector<double> delta_errs(degree+1,0.0);
        std::vector<double> pulls(degree+1,0.0);
        std::vector<double> delta_pulls(degree+1,0.0);
        double chi2 = Espresso::CalibrationComparisonChi2(*smcal,cal);
        for (int k = 0; k <= degree; k++)  {
          coeffs[k] = cal.GetCoeff(k);
          errs[k] = cal.GetError(k);
          delta_coeffs[k] = cal.GetDeltaCoeff(k);
          delta_errs[k] = cal.GetDeltaError(k);
          pulls[k] = (coeffs[k] - smearCoeffs[k])/errs[k];
          delta_pulls[k] = (delta_coeffs[k] - smearDeltaCoeffs[k])/delta_errs[k];
        }
        chi2_hist->Fill(chi2);
        cal_table << chi2 << ", ";
        for (int k = 0; k <= degree; k++) {
          pulls_hist[k]->Fill(pulls[k]);
          cal_table << pulls[k] << ", ";
        }
        for (int k = 0; k <= degree; k++) {
          delta_pulls_hist[k]->Fill(delta_pulls[k]);
          cal_table << delta_pulls[k];
          if (k != degree)
            cal_table << ", ";
          else
            cal_table << "\n";
        }
      }
    }
  }
  
  if (Config::AsymmetryNullTest)
    asymm_table.close();
  if (Config::AsymmetryFittedTest)
    fit_asymm_table.close();
  if (Config::MetricNullTest)
    gof_table.close(); 
  if (Config::MetricFittedTest)
    fit_gof_table.close();
  if (Config::CalibrationTest)
    cal_table.close();
  
  // PLOT HISTOGRAMS
  if (Config::MetricNullTest) {
    TCanvas* metricnullc = new TCanvas("metricnullc","metricnullc",800*2,800*2);
    metricnullc->Divide(2,2);
    metricnullc->cd(1);
    uG2_hist->Draw("E");
    metricnullc->cd(2);
    uX2_hist->Draw("E");
    metricnullc->cd(3);
    uCR_hist->Draw("E");
    metricnullc->cd(4);
    uS_hist->Draw("E");
    metricnullc->SaveAs("metricNullTest.png");
    delete uG2_hist;
    delete uX2_hist;
    delete uCR_hist;
    delete uS_hist;
    delete metricnullc;
  }

  if (Config::MetricFittedTest) {
    TCanvas* metricfittedc = new TCanvas("metricfittedc","metricfittedc",800*2,800*2);
    metricfittedc->Divide(2,2);
    metricfittedc->cd(1);
    uG2_fit_hist->Draw("E");
    metricfittedc->cd(2);
    uX2_fit_hist->Draw("E");
    metricfittedc->cd(3);
    uCR_fit_hist->Draw("E");
    metricfittedc->cd(4);
    uS_fit_hist->Draw("E");
    metricfittedc->SaveAs("metricFittedTest.png");
    delete uG2_fit_hist;
    delete uX2_fit_hist;
    delete uCR_fit_hist;
    delete uS_fit_hist;
    delete metricfittedc;
  }

  if (Config::CalibrationTest) {
    std::cout << std::endl;
    std::cout << "--------------------" << std::endl;
    std::cout << "PLOTTING PULL HISTOS:" << std::endl;
    std::cout << "--------------------" << std::endl;
    

    TCanvas* chi2c = new TCanvas("chi2","chi2",800*(degree+1),800);
    chi2c->Divide(1,1);
    chi2c->cd(1);
    chi2_hist->Draw("E");
    chi2c->SaveAs("calibration_chi2.png");
    delete chi2_hist;
    delete chi2c;

    TCanvas* c = new TCanvas("pulls","pulls",800*(degree+1),800);
    c->Divide(degree+1,1);
    for (int k = 0; k <= degree; k++) {
      c->cd(k+1);
      pulls_hist[k]->Draw("E");
    }
    c->SaveAs("calibration_pulls.png");
    for (int k = 0; k <= degree; k++) {
      delete pulls_hist[k];
    }
    delete c;
      
    TCanvas* dc = new TCanvas("delta_pulls","delta_pulls",800*(degree+1),800);
    dc->Divide(degree+1,1);
    for (int k = 0; k <= degree; k++) {
      dc->cd(k+1);
      delta_pulls_hist[k]->Draw("E");
    }
    dc->SaveAs("calibration_delta_pulls.png");
    for (int k = 0; k <= degree; k++) {
      delete delta_pulls_hist[k];
    }
    delete dc;
  }
  
}
int main (int argc, char** argv) {
  
  // Load options file
  if (argc >= 2) {
    Config::ConfigFileName = static_cast<TString>(argv[1]);
  }
  std::cout << "READING CONFIGURATION FROM " << Config::ConfigFileName << std::endl;
  Config::readParameters();

  RandomNumberGeneration::SetSeed(Config::RandomSeed);

  int evCount = 0;

  Int_t ID = 1;
  Short_t ID_s = 1;
  TString TypeID = "";
  
  Double_t Weight = 1.0;
  
  Double_t Tau = 0.0;
  Float_t Tau_f = 0.0;
  TString TypeTau = "";

  Double_t TauErr = 0.0;
  Float_t TauErr_f = 0.0;
  TString TypeTauErr = "";

  Double_t scale = 1.0;

  std::map<Tagger::Type,Int_t> Dec = {
    {Tagger::Toy, 0},

    {Tagger::OS_Muon, 0},
    {Tagger::OS_Electron, 0},
    {Tagger::OS_Kaon, 0},
    {Tagger::OS_nnetKaon, 0},
    {Tagger::VtxCharge, 0},
    {Tagger::OS_Charm, 0},

    {Tagger::SS_Kaon, 0},
    {Tagger::SS_nnetKaon, 0},
    {Tagger::SS_Pion, 0},
    {Tagger::SS_PionBDT, 0},
    {Tagger::SS_Proton, 0},

    {Tagger::Inclusive, 0},
    
    {Tagger::OS_Combination, 0},
    {Tagger::Combination, 0}
  };

  std::map<Tagger::Type,Short_t> Dec_s = {
    {Tagger::Toy, 0},

    {Tagger::OS_Muon, 0},
    {Tagger::OS_Electron, 0},
    {Tagger::OS_Kaon, 0},
    {Tagger::OS_nnetKaon, 0},
    {Tagger::VtxCharge, 0},
    {Tagger::OS_Charm, 0},

    {Tagger::SS_Kaon, 0},
    {Tagger::SS_nnetKaon, 0},
    {Tagger::SS_Pion, 0},
    {Tagger::SS_PionBDT, 0},
    {Tagger::SS_Proton, 0},

    {Tagger::Inclusive, 0},
    
    {Tagger::OS_Combination, 0},
    {Tagger::Combination, 0}
  };

  std::map<Tagger::Type,TString> TypeDec = {
    { Tagger::Toy, "" },
    
    {Tagger::OS_Muon, ""},
    {Tagger::OS_Electron, ""},
    {Tagger::OS_Kaon, ""},
    {Tagger::OS_nnetKaon, ""},
    {Tagger::VtxCharge, ""},
    {Tagger::OS_Charm, ""},

    {Tagger::SS_Kaon, ""},
    {Tagger::SS_nnetKaon, ""},
    {Tagger::SS_Pion, ""},
    {Tagger::SS_PionBDT, ""},
    {Tagger::SS_Proton, ""},

    {Tagger::Inclusive, ""},

    {Tagger::OS_Combination, ""},
    {Tagger::Combination, ""}
    };

  std::map<Tagger::Type,Double_t> Prob = {
    {Tagger::Toy, 0},

    {Tagger::OS_Muon, 0},
    {Tagger::OS_Electron, 0},
    {Tagger::OS_Kaon, 0},
    {Tagger::OS_nnetKaon, 0},
    {Tagger::VtxCharge, 0},
    {Tagger::OS_Charm, 0},

    {Tagger::SS_Kaon, 0},
    {Tagger::SS_nnetKaon, 0},
    {Tagger::SS_Pion, 0},
    {Tagger::SS_PionBDT, 0},
    {Tagger::SS_Proton, 0},

    {Tagger::Inclusive, 0},
    
    {Tagger::OS_Combination, 0},
    {Tagger::Combination, 0}
  };
  
  std::map<Tagger::Type,Float_t> Prob_f = {
    {Tagger::Toy, 0},

    {Tagger::OS_Muon, 0},
    {Tagger::OS_Electron, 0},
    {Tagger::OS_Kaon, 0},
    {Tagger::OS_nnetKaon, 0},
    {Tagger::VtxCharge, 0},
    {Tagger::OS_Charm, 0},

    {Tagger::SS_Kaon, 0},
    {Tagger::SS_nnetKaon, 0},
    {Tagger::SS_Pion, 0},
    {Tagger::SS_PionBDT, 0},
    {Tagger::SS_Proton, 0},

    {Tagger::Inclusive, 0},
    
    {Tagger::OS_Combination, 0},
    {Tagger::Combination, 0}
  };
  
  std::map<Tagger::Type,TString> TypeProb = {
    {Tagger::Toy, ""},
    
    {Tagger::OS_Muon, ""},
    {Tagger::OS_Electron, ""},
    {Tagger::OS_Kaon, ""},
    {Tagger::OS_nnetKaon, ""},
    {Tagger::VtxCharge, ""},
    {Tagger::OS_Charm, ""},
    
    {Tagger::SS_Kaon, ""},
    {Tagger::SS_nnetKaon, ""},
    {Tagger::SS_Pion, ""},
    {Tagger::SS_PionBDT, ""},
    {Tagger::SS_Proton, ""},

    {Tagger::Inclusive, ""},
    
    {Tagger::OS_Combination, ""},
    {Tagger::Combination, ""} 
  };
  
  TBranch* b_ID;
  TBranch* b_Tau;
  TBranch* b_TauErr;

  std::map<Tagger::Type,TBranch*> BranchDec = {
    {Tagger::Toy, nullptr},

    {Tagger::OS_Muon, nullptr},
    {Tagger::OS_Electron, nullptr},
    {Tagger::OS_Kaon, nullptr},
    {Tagger::OS_nnetKaon, nullptr},
    {Tagger::VtxCharge, nullptr},
    {Tagger::OS_Charm, nullptr},

    {Tagger::SS_Kaon, nullptr},
    {Tagger::SS_nnetKaon, nullptr},
    {Tagger::SS_Pion, nullptr},
    {Tagger::SS_PionBDT, nullptr},
    {Tagger::SS_Proton, nullptr},

    {Tagger::Inclusive, nullptr},
    
    {Tagger::OS_Combination, nullptr},
    {Tagger::Combination, nullptr}
  };

  std::map<Tagger::Type,TBranch*> BranchProb = {
    {Tagger::Toy, nullptr},

    {Tagger::OS_Muon, nullptr},
    {Tagger::OS_Electron, nullptr},
    {Tagger::OS_Kaon, nullptr},
    {Tagger::OS_nnetKaon, nullptr},
    {Tagger::VtxCharge, nullptr},
    {Tagger::OS_Charm, nullptr},

    {Tagger::SS_Kaon, nullptr},
    {Tagger::SS_nnetKaon, nullptr},
    {Tagger::SS_Pion, nullptr},
    {Tagger::SS_PionBDT, nullptr},
    {Tagger::SS_Proton, nullptr},
    
    {Tagger::Inclusive, nullptr},
    
    {Tagger::OS_Combination, nullptr},
    {Tagger::Combination, nullptr}
  };

  EspressoPerformanceMonitor perfmon;
  
  std::cout << std::endl;
  std::cout << "------------------------------" << std::endl;
  std::cout << "----- BEGINNING ANALYSIS -----" << std::endl;
  std::cout << "------------------------------" << std::endl;
  std::cout << std::endl;

  std::ofstream csvtable;
  if (Config::WriteTaggerDecisionTable) {
    csvtable.open("tagger_table.csv");
    csvtable << "ID, ";
    for (int k = Tagger::Type_SMIN; k <= Tagger::Type_MAX; k++) {
      Tagger::Type t = static_cast<Tagger::Type>(k);
      if (Tagger::IsActive(t)
          or (t==Tagger::OS_Combination and Config::PerformOfflineCombination_OS)
          or (t==Tagger::SS_Combination and Config::PerformOfflineCombination_SS)
          or (t==Tagger::Combination and Config::PerformOfflineCombination_OSplusSS)) {
        csvtable << Tagger::Name(t) << ", ";
      }
    }
    if (Config::UseTau) csvtable << "Tau" << ", ";
    csvtable << "Weight" << std::endl;
  }
  
  TFile* calfile;
  TTree* caltree;
  std::map<Tagger::Type,std::shared_ptr<Calibration> > tagger_Calibration;
  std::map<Tagger::Type,Int_t> tagger_Dec;
  std::map<Tagger::Type,Double_t> tagger_Eta;
  std::map<Tagger::Type,Double_t> tagger_Omega;
  std::map<Tagger::Type,Double_t> tagger_DeltaOmega;
  std::map<Tagger::Type,Double_t> tagger_Offset;
  std::map<Tagger::Type,std::vector<Double_t> > tagger_Basis;
  std::map<Tagger::Type,TBranch*> tagger_Dec_Branch;
  std::map<Tagger::Type,TBranch*> tagger_Eta_Branch;
  std::map<Tagger::Type,TBranch*> tagger_Omega_Branch;
  std::map<Tagger::Type,TBranch*> tagger_DeltaOmega_Branch;
  std::map<Tagger::Type,TBranch*> tagger_Offset_Branch;
  std::map<Tagger::Type,std::vector<TBranch*> > tagger_Basis_Branch;

  bool WriteCalibratedMistagBranches = false;
  for (int k = Tagger::Type_SMIN; k <= Tagger::Type_MAX; k++) {
    Tagger::Type t = static_cast<Tagger::Type>(k);
    if (Tagger::Write(t) and (Tagger::IsActive(t)
                              or (t==Tagger::OS_Combination and Config::PerformOfflineCombination_OS)
			      or (t==Tagger::SS_Combination and Config::PerformOfflineCombination_SS)
			      or (t==Tagger::Combination and Config::PerformOfflineCombination_OSplusSS))) {
      WriteCalibratedMistagBranches = true;
      break;
    }
  }
  if (WriteCalibratedMistagBranches and Config::filelist.size()>1) {
    WriteCalibratedMistagBranches = false;
    std::cout << "Warning: Saving calibrated files to a friend tree is incompatible\n"
              << "with processing multiple files simultaneously. Run over each file\n"
              << "separately to produce separate friend trees for each input file." << std::endl;
  }
    
  if (WriteCalibratedMistagBranches) {
    calfile = TFile::Open(Config::CalibratedOutputFile.Data(),"RECREATE");
    caltree = new TTree("TaggingTree","TaggingTree");
    
    for (int k = Tagger::Type_SMIN; k <= Tagger::Type_MAX; k++) {
      Tagger::Type t = static_cast<Tagger::Type>(k);
    if (Tagger::Write(t) and (Tagger::IsActive(t)
                              or (t==Tagger::OS_Combination and Config::PerformOfflineCombination_OS)
                              or (t==Tagger::SS_Combination and Config::PerformOfflineCombination_SS)
                              or (t==Tagger::Combination and Config::PerformOfflineCombination_OSplusSS))) {
        tagger_Calibration.emplace(std::make_pair(t,Tagger::Calibration(t)));
        tagger_Dec.emplace(std::make_pair(t,0));
        TString decname = TString(Tagger::Name(t)) + "_DEC";
        tagger_Dec_Branch.emplace(std::make_pair(t,caltree->Branch(decname,&tagger_Dec[t],decname + "/I")));
        tagger_Eta.emplace(std::make_pair(t,0.5));
        TString etname = TString(Tagger::Name(t)) + "_ETA";
        tagger_Eta_Branch.emplace(std::make_pair(t,caltree->Branch(etname,&tagger_Eta[t],etname + "/D")));
        tagger_Omega.emplace(std::make_pair(t,0.5));
        TString omname = TString(Tagger::Name(t)) + "_OMEGA";
        tagger_Omega_Branch.emplace(std::make_pair(t,caltree->Branch(omname,&tagger_Omega[t],omname + "/D")));
        tagger_DeltaOmega.emplace(std::make_pair(t,0.5));
        TString domname = TString(Tagger::Name(t)) + "_DELTAOMEGA";
        tagger_DeltaOmega_Branch.emplace(std::make_pair(t,caltree->Branch(domname,&tagger_DeltaOmega[t],domname + "/D")));
        if (enable_floatingcal_branches) {
          tagger_Offset.emplace(std::make_pair(t,tagger_Calibration[t]->GetOffset(0.5)));
          TString offname = TString(Tagger::Name(t)) + "_OMEGA_OFFSET";
          tagger_Offset_Branch.emplace(std::make_pair(t,caltree->Branch(offname,&tagger_Offset[t],offname + "/D")));
          int np = tagger_Calibration[t]->NumParams();
          std::vector<Double_t> basis;
          std::vector<TBranch*> b_basis;
          for (int l = 0; l < np; ++l)
            basis.push_back(0.0);
          tagger_Basis.emplace(std::make_pair(t,basis));
          for (int l = 0; l < np; ++l) {
            TString bname = TString(Tagger::Name(t)) + TString::Format("_OMEGA_BASIS_%d",l);
            b_basis.push_back(caltree->Branch(bname,&tagger_Basis[t][l],bname + "/D"));
          }
          tagger_Basis_Branch.emplace(std::make_pair(t,b_basis));
        }
      }
    }
  }

  for (auto fileName : Config::filelist) {

    std::cout << "Reading file " << fileName << std::endl << std::endl;

    TFile* file = TFile::Open(fileName.Data(),"READ");
    TTree* tree;
    file->GetObject(Config::m_tupleName.Data(),tree);


    TEventList *myelist = new TEventList("myelist","myelist");
    tree->Draw(">>myelist",Config::Selection.Data());
    Int_t nevents_total = tree->GetEntries();
    Int_t nevents = myelist->GetN();

    double pass, fail, effpass, efffail;
    if (Config::UseWeight) {
      TH1F wkeep("wkeep","wkeep",1,-1,1);
      wkeep.Sumw2(kTRUE);
      TString selkeep = Config::WeightFormula;
      if (Config::Selection != "")
        selkeep += "*(" + Config::Selection + ")";
      tree->Project("wkeep","1",selkeep.Data());
      pass = wkeep.GetSum();

      TH1F wall("wall","wall",1,-1,1);
      wall.Sumw2(kTRUE);
      TString selall = Config::WeightFormula;
      tree->Project("wall","0",selall.Data());
      double total = wall.GetSum();
      fail = total-pass;
      
      double neff = wall.GetEffectiveEntries();
      effpass = pass/total * neff;
      efffail = fail/total * neff;
      
    } else {
      pass = nevents;
      fail = nevents_total - nevents;
      effpass = pass;
      efffail = fail;
    }
    double eff_low,eff,eff_high;
    std::tie(eff,eff_low,eff_high) = Espresso::EfficiencyConfidenceInterval(effpass,efffail);
    std::cout << "Cut " << Config::Selection << " keeps " << pass << " out of " << pass+fail << " events";
    if (Config::UseWeight)
      std::cout << " (weighted).";
    else
      std::cout << " (unweighted).";
    std::cout << std::endl;
    std::cout << "Corresponding efficiency: "
              << std::setprecision(4) << eff
              << " (+ "
              << std::setprecision(4) << eff_high-eff
              << ", -"
              << std::setprecision(4) << eff-eff_low
              << ")." << std::endl;
    
    if (Config::Nmax != -1 && nevents > Config::Nmax)
      nevents = Config::Nmax;
    TagCollector::setExpectedNumberOfEvents(nevents);

    auto setDiscreteBranch = [tree] (TString name, TBranch* branch, Int_t* vi, Short_t* vs, TString& type) {
      branch = tree->GetBranch(name);

      TObjArray * leaves = branch->GetListOfLeaves();
      TString leafName = leaves->At(0)->GetName();
      TString leafType = branch->GetLeaf(leafName)->GetTypeName();

      if (leafType == "Int_t")
        branch->SetAddress(vi);
      else if (leafType == "Short_t")
        branch->SetAddress(vs);
      else
        std::cerr << "ERROR: branch " << name << "must be Int_t or Short_t" << std::endl;

      type = leafType;
    };

    auto setFPBranch = [tree] (TString name, TBranch* branch, Double_t* vd, Float_t* vf, TString& type) {
      branch = tree->GetBranch(name);

      TObjArray * leaves = branch->GetListOfLeaves();
      TString leafName = leaves->At(0)->GetName();
      TString leafType = branch->GetLeaf(leafName)->GetTypeName();

      if (leafType == "Double_t")
        branch->SetAddress(vd);
      else if (leafType == "Float_t")
        branch->SetAddress(vf);
      else
        std::cerr << "ERROR: branch " << name << " must be Double_t or Float_t" << std::endl;

      type = leafType;
    };

    bool loadID = (Config::BranchID != "") or Config::DoCalibrations;
    if (loadID) {
      std::cout << "Loading ID branch" << std::endl;
      setDiscreteBranch(Config::BranchID,b_ID,&ID,&ID_s,TypeID);
    }

    TTreeFormula *wf;
    if (Config::UseWeight) {
      std::cout << "Loading Weight branch" << std::endl;
      wf = new TTreeFormula("wf",Config::WeightFormula,tree);
    } else {
      wf = new TTreeFormula("wf","1",tree);
    }

    if (Config::UseTau) {
      std::cout << "Loading Tau branch" << std::endl;
      setFPBranch(Config::BranchTau,b_Tau,&Tau,&Tau_f,TypeTau);
    }

    if (Config::UseTauErr) {
      std::cout << "Loading Tau branch" << std::endl;
      setFPBranch(Config::BranchTauErr,b_TauErr,&TauErr,&TauErr_f,TypeTauErr);
    }
    
    if (Config::UseTau or Config::UseTauErr) {
      if (Config::TauUnits == "ps")
        scale = 1.0;
      else if (Config::TauUnits == "ns")
        scale = 1000.0;
      else if (Config::TauUnits == "fs")
        scale = 0.001;
      else
        scale = 1.0;
    }
    
    for (int k = Tagger::Type_SMIN; k <= Tagger::Type_MAX; k++) {
      Tagger::Type t = static_cast<Tagger::Type>(k);
      if (Tagger::IsActive(t)) {
        std::string name = Tagger::Name(t);
        std::cout << "Loading " << name << " branches" << std::endl;
        setDiscreteBranch(Tagger::BranchDec(t),BranchDec[t],&Dec[t],&Dec_s[t],TypeDec[t]);
        setFPBranch(Tagger::BranchProb(t),BranchProb[t],&Prob[t],&Prob_f[t],TypeProb[t]);
      }
    }
    
    for (int ev = 0; ev < nevents; ev++) {

      evCount++;

      tree->GetEntry(myelist->GetEntry(ev));
      if (ev%10000 == 0) std::cout << "ON EVENT " << ev << std::endl;
      
      auto convertDiscrete = [] (TString type, Int_t i, Short_t s) -> Int_t {
        if (type == "Int_t")
          return i;
        else if (type == "Short_t")
          return static_cast<Int_t>(s);
        return i;
      };

      auto convertFP = [] (TString type, Double_t d, Float_t f) -> Double_t {
        if (type == "Double_t")
          return d;
        else if (type == "Float_t")
          return static_cast<Double_t>(f);
        return d;
      };
    
      
      if (loadID) {
        ID = convertDiscrete(TypeID,ID,ID_s);
        /*std::cout << "TypeID: " << TypeID << std::endl;
          std::cout << "ID: " << ID << std::endl;*/
      }
      if (Config::UseWeight) {
        Weight = wf->EvalInstance();
      }
      if (Config::UseTau) {
        Tau = convertFP(TypeTau,Tau,Tau_f);
        /*std::cout << "TypeTau: " << TypeTau << std::endl;
          std::cout << "Tau: " << Tau << std::endl;*/
      }
      if (Config::UseTauErr) {
        TauErr = convertFP(TypeTauErr,TauErr,TauErr_f);
        /*std::cout << "TypeTauErr: " << TypeTauErr << std::endl;
          std::cout << "TauErr: " << TauErr << std::endl;*/
      }
      for (int k = Tagger::Type_SMIN; k <= Tagger::Type_MAX; k++) {
        Tagger::Type t = static_cast<Tagger::Type>(k);
        Dec[t] = convertDiscrete(TypeDec[t],Dec[t],Dec_s[t]);
        /*std::cout << "TypeDec[t]: " << TypeDec[t] << std::endl;
          std::cout << "Dec[t]: " << Dec[t] << std::endl;*/
        Prob[t] = convertFP(TypeProb[t],Prob[t],Prob_f[t]);
        /*std::cout << "TypeProb[t]: " << TypeProb[t] << std::endl;
          std::cout << "Prob[t]: " << Prob[t] << std::endl;*/
      }

      // PRINT TO TABLE
      Int_t TrueTag = (ID > 0) ? 1 : ((ID < 0) ? -1 : 0);
      if (Config::WriteTaggerDecisionTable and Weight != 0) {
        csvtable << ((TrueTag>0) ? 1 : 0) << ", ";
      }
      
      if (WriteCalibratedMistagBranches) {
        for (auto& it : tagger_Eta)
          it.second = 0.5;
        for (auto& it : tagger_Omega)
          it.second = 0.5;
        for (auto& it : tagger_DeltaOmega)
          it.second = 0.0;
        if (enable_floatingcal_branches) {
          for (auto& it : tagger_Offset)
            it.second = tagger_Calibration[it.first]->GetOffset(0.5);
          for (auto& it : tagger_Basis) {
            for (auto& jt : it.second)
              jt = 0.0;
          }
        }
      }
      Taggers taggers; 
      std::map<Tagger::Type,Tagger> taggermap;
      for (int k = Tagger::Type_SMIN; k <= Tagger::Type_MAX; k++) {
        Tagger::Type t = static_cast<Tagger::Type>(k);
        if (Tagger::IsActive(t)
            or (t==Tagger::OS_Combination and Config::PerformOfflineCombination_OS)
	    or (t==Tagger::SS_Combination and Config::PerformOfflineCombination_SS)
	    or (t==Tagger::Combination and Config::PerformOfflineCombination_OSplusSS)) {
          /// Warning -- this code relises on the fact that the combination taggers are the last enums, and OS before total
          if (t == Tagger::OS_Combination and Config::PerformOfflineCombination_OS) 
            performOSCombination(taggers,Dec[t],Prob[t]);
          if (t == Tagger::SS_Combination and Config::PerformOfflineCombination_SS) 
            performSSCombination(taggers,Dec[t],Prob[t]);
          if (t == Tagger::Combination and Config::PerformOfflineCombination_OSplusSS)
            performOSplusSSCombination(taggers,Dec[t],Prob[t]);
          if (Config::CalibrationMode == CalibrationMode::ChargedBu) {
            if (t == Tagger::SS_Kaon or t == Tagger::SS_nnetKaon)
              Dec[t] = -1*Dec[t];
          }
          if (Config::WriteTaggerDecisionTable and Weight != 0)
            csvtable << prob(Dec[t],Prob[t]) << ", ";
          taggermap.emplace(std::make_pair(t,Tagger(t,Dec[t],Prob[t])));
          //std::cout << "=============================================" << std::endl;
          //std::cout << "t,Dec[t],Prob[t] " << t << " " << Dec[t] << " " << Prob[t] << std::endl;
          taggers.push_back(&taggermap[t]);
          if (WriteCalibratedMistagBranches and Tagger::Write(t)) {
            if (Dec[t] and Prob[t] != 0.5) {
              tagger_Dec[t] = Dec[t];
              tagger_Eta[t] = taggermap[t].eta();
              //std::cout << "tagger_Eta[t] = " << tagger_Eta[t] << std::endl;
              tagger_Omega[t] = taggermap[t].omega();
              //std::cout << "tagger_Omega[t] = " << tagger_Omega[t] << std::endl;
              //std::cout << "NumParams: " << tagger_Calibration[t]->NumParams() << std::endl;
              //std::cout << "p0: " << tagger_Calibration[t]->GetCoeff(0) << std::endl;
              //std::cout << "p1: " << tagger_Calibration[t]->GetCoeff(1) << std::endl;
              //std::cout << "dp0: " << tagger_Calibration[t]->GetDeltaCoeff(0) << std::endl;
              //std::cout << "dp1: " << tagger_Calibration[t]->GetDeltaCoeff(1) << std::endl;
              tagger_DeltaOmega[t] = tagger_Calibration[t]->Value(taggermap[t].eta(),Flavour::Pos)
                - tagger_Calibration[t]->Value(taggermap[t].eta(),Flavour::Neg);
              if (enable_floatingcal_branches) {
                tagger_Offset[t] = tagger_Calibration[t]->GetOffset(taggermap[t].eta());
                int np = tagger_Calibration[t]->NumParams();
                Vector basis = tagger_Calibration[t]->GetBasis(taggermap[t].eta());
                for (int l = 0; l < np; ++l) {
                  tagger_Basis[t][l] = basis[l];
                }
              }
            } else {
              tagger_Dec[t] = 0;
              tagger_Eta[t] = 0.5;
              tagger_Omega[t] = 0.5;
              tagger_DeltaOmega[t] = 0.0;
              if (enable_floatingcal_branches) {
                tagger_Offset[t] = 0.0;
                int np = tagger_Calibration[t]->NumParams();
                for (int l = 0; l < np; ++l) {
                  tagger_Basis[t][l] = 0.0;
                }
              }
            }
          }
        }
      }

      if (WriteCalibratedMistagBranches) {
        caltree->Fill();
        //for (const auto& it : tagger_Eta_Branch)
        //  it.second->Fill();
        // for (const auto& it : tagger_Omega_Branch)
        //   it.second->Fill();
        // for (const auto& it : tagger_DeltaOmega_Branch)
        //   it.second->Fill();
        // if (enable_floatingcal_branches) {
        //   for (const auto& it : tagger_Offset_Branch)
        //     it.second->Fill();
        //   for (const auto& it : tagger_Basis_Branch) {
        //     for (const auto& jt : it.second)
        //       jt->Fill();
        //   }
        // }
      }

      if (Config::WriteTaggerDecisionTable and Weight != 0) {
        if (Config::UseTau) csvtable << Tau << ", ";
        csvtable << Weight << std::endl;
      }

      Flavour decayFlavour = FlavourFromInt(TrueTag);
      perfmon.increment(taggers,decayFlavour,Tau*scale,TauErr*scale,Weight);
    }

    // Write CP Analysis instructions
    if (WriteCalibratedMistagBranches and enable_floatingcal_branches) {
      std::ofstream instr("CPAnalysisInstructions.txt");
      for (int k = Tagger::Type_SMIN; k <= Tagger::Type_MAX; k++) {
        Tagger::Type t = static_cast<Tagger::Type>(k);
        if (Tagger::IsActive(t) and Tagger::Write(t)) {
          instr << " --- CALIBRATION FOR " << Tagger::Name(t) << " ---" << std::endl;
          instr << "ETA branch: " << tagger_Eta_Branch[t]->GetName() << std::endl;
          instr << "OMEGA branch: " << tagger_Omega_Branch[t]->GetName() << std::endl;
          instr << "DELTAOMEGA branch: " << tagger_DeltaOmega_Branch[t]->GetName() << std::endl;
          instr << "OFFSET branch: " << tagger_Offset_Branch[t]->GetName() << std::endl;
          instr << "BASIS branches: ";
          for (int l = 0; l < tagger_Calibration[t]->NumParams(); ++l) {
            if (l > 0) instr << ", ";
            instr << tagger_Basis_Branch[t][l]->GetName();
          }
	  
          instr << std::endl << std::endl;
          instr << *tagger_Calibration[t] << std::endl << std::endl;
	  
          instr << "ROOFIT INSTRUCTIONS:" << std::endl;
          instr << "Create variables P0, P1, etc for each calibration paramter." << std::endl;
          instr << "Let each be equal to p0 + flavor*Δp0, etc." << std::endl;
          instr << "Give p0 and Δp0 gaussian constraints and the proper correlations." << std::endl;
          instr << "Create a RooFormulaVar for the quantity X = OFFSET + P0*BASIS_0 + P1*BASIS_1 + ..." << std::endl; 
          instr << "Use the proper link function to create OMEGA = LINK(X)" << std::endl;
	  
          instr << std::endl << std::endl << std::endl;
        }
      }
    }

    delete file;
  }
  
  if (Config::WriteTaggerDecisionTable)
    csvtable.close();
  
  perfmon.printStats();

  // Write and close files
  if (WriteCalibratedMistagBranches) {
    std::cout << "Saving ROOT file with calibrated mistag" << std::endl;
    calfile->Write();
    calfile->Close();
    delete calfile;
  }

  // TOY STUDIES
  if (Config::AsymmetryNullTest || Config::AsymmetryFittedTest || Config::MetricNullTest || Config::MetricFittedTest || Config::CalibrationTest)
    CalibrationTest(perfmon);
}
