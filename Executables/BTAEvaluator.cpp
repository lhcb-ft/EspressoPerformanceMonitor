#include <iostream>

#include <TString.h>
#include <TFile.h>
#include <TTree.h>
#include <TEventList.h>
#include <TTreeFormula.h>
#include <TBranch.h>
#include <TF1.h>
#include <TH1F.h>

#include "globals.hh"
#include "Tagger.hh"
#include "EspressoPerformanceMonitor.hh"
#include "RandomNumberGeneration.hh"

#include "TagCollector.hh"

typedef std::vector<Tagger*> Taggers;

int main (int argc, char** argv) {

  // Load options file
  if (argc >= 2) {
    Config::ConfigFileName = static_cast<TString>(argv[1]);
  }
  std::cout << "READING CONFIGURATION FROM " << Config::ConfigFileName << std::endl;
  Config::readParameters();
  
  RandomNumberGeneration::SetSeed(Config::RandomSeed);

  int evCount = 0;

  Int_t ID = 1;
  Short_t ID_s = 1;
  
  Double_t Weight = 1.0;
  
  Double_t Tau = 0.0;
  Float_t Tau_f = 0.0;

  Double_t TauErr = 0.0;
  Float_t TauErr_f = 0.0;

  Double_t scale = 1.0;

  Int_t           T;
  Short_t         T_s;
 
  Int_t           TaggerType[10];   //[T]
  Short_t         TaggerType_s[10];   //[T]
  Double_t        TaggerType_d[10];   //[T]
  Float_t         TaggerType_f[10];   //[T]

  Int_t           TaggerDecision[10];   //[T]
  Short_t         TaggerDecision_s[10];   //[T]
  Double_t        TaggerDecision_d[10];   //[T]
  Float_t         TaggerDecision_f[10];   //[T]

  Double_t        TaggerOmega[10];   //[T]
  Float_t         TaggerOmega_f[10];   //[T]

  Int_t MuonDec;
  Double_t MuonOmega;

  Int_t ElectronDec;
  Double_t ElectronOmega;

  Int_t KaonDec;
  Double_t KaonOmega;

  Int_t KaonNNDec;
  Double_t KaonNNOmega;

  Int_t VtxChargeDec;
  Double_t VtxChargeOmega;

  Int_t CharmDec;
  Double_t CharmOmega;

  Int_t SSKaonDec;
  Double_t SSKaonOmega;

  Int_t SSKaonNNDec;
  Double_t SSKaonNNOmega;

  Int_t SSPionDec;
  Double_t SSPionOmega;

  Int_t SSPionBDTDec;
  Double_t SSPionBDTOmega;

  Int_t SSProtonDec;
  Double_t SSProtonOmega;

  Int_t InclusiveDec;
  Double_t InclusiveOmega;
  
  TBranch* b_ID;
  TBranch* b_Tau;
  TBranch* b_TauErr;
  TBranch* b_T;   //!
  TBranch* b_TaggerType;   //!
  TBranch* b_TaggerDecision;   //!
  TBranch* b_TaggerOmega;   //!

  EspressoPerformanceMonitor perfmon;

  std::cout << std::endl;
  std::cout << "------------------------------" << std::endl;
  std::cout << "----- BEGINNING ANALYSIS -----" << std::endl;
  std::cout << "------------------------------" << std::endl;
  std::cout << std::endl;
  
  for (auto fileName : Config::filelist) {

    std::cout << "Reading file " << fileName << std::endl << std::endl;

    TFile* file = TFile::Open(fileName.Data(),"READ");
    TTree* tree;
    file->GetObject(Config::m_tupleName.Data(),tree);
    TEventList *myelist = new TEventList("myelist","myelist");
    tree->Draw(">>myelist",Config::Selection.Data());
    Int_t nevents_total = tree->GetEntries();
    Int_t nevents = myelist->GetN();

    double pass, fail, effpass, efffail;
    if (Config::UseWeight) {
      TH1F wkeep("wkeep","wkeep",1,-1,1);
      wkeep.Sumw2(kTRUE);
      TString selkeep = Config::WeightFormula;
      if (Config::Selection != "")
        selkeep += "*(" + Config::Selection + ")";
      tree->Project("wkeep","1",selkeep.Data());
      pass = wkeep.GetSum();

      TH1F wall("wall","wall",1,-1,1);
      wall.Sumw2(kTRUE);
      TString selall = Config::WeightFormula;
      tree->Project("wall","0",selall.Data());
      double total = wall.GetSum();
      fail = total-pass;
      
      double neff = wall.GetEffectiveEntries();
      effpass = pass/total * neff;
      efffail = fail/total * neff;
      
    } else {
      pass = nevents;
      fail = nevents_total - nevents;
      effpass = pass;
      efffail = fail;
    }
    double eff_low,eff,eff_high;
    std::tie(eff,eff_low,eff_high) = Espresso::EfficiencyConfidenceInterval(effpass,efffail);
    std::cout << "Cut " << Config::Selection << " keeps " << pass << " out of " << pass+fail << " events";
    if (Config::UseWeight)
      std::cout << " (weighted).";
    else
      std::cout << " (unweighted).";
    std::cout << std::endl;
    std::cout << "Corresponding efficiency: "
              << std::setprecision(4) << eff
              << " (+ "
              << std::setprecision(4) << eff_high-eff
              << ", -"
              << std::setprecision(4) << eff-eff_low
              << ")." << std::endl;
    
    if (Config::Nmax != -1 && nevents > Config::Nmax)
      nevents = Config::Nmax;
    TagCollector::setExpectedNumberOfEvents(nevents);

    auto setDiscreteBranch = [tree] (TString name, TString type, TBranch* branch, Int_t* vi, Short_t* vs) {
      branch = tree->GetBranch(name);
      if (type == "Int_t")
        branch->SetAddress(vi);
      else if (type == "Short_t")
        branch->SetAddress(vs);
      else
        std::cerr << "ERROR: branch " << name << "must be Int_t or Short_t" << std::endl;
    };

    auto setFPBranch = [tree] (TString name, TString type, TBranch* branch, Double_t* vd, Float_t* vf) {
      branch = tree->GetBranch(name);
      if (type == "Double_t")
        branch->SetAddress(vd);
      else if (type == "Float_t")
        branch->SetAddress(vf);
      else
        std::cerr << "ERROR: branch " << name << " must be Double_t or Float_t" << std::endl;
    };

    auto setFPArrayBranch = [tree] (TString name, TString type, TBranch* branch, Double_t (*vd)[10], Float_t (*vf)[10]) {
      branch = tree->GetBranch(name);
      if (type == "Double_t")
        branch->SetAddress(vd);
      else if (type == "Float_t")
        branch->SetAddress(vf);
      else
        std::cerr << "ERROR: branch " << name << " must be Double_t or Float_t" << std::endl;
    };

    auto setArrayBranch = [tree] (TString name, TString type, TBranch* branch, Int_t (*vi)[10], Short_t (*vs)[10], Double_t (*vd)[10], Float_t (*vf)[10]) {
      branch = tree->GetBranch(name);
      if (type == "Int_t")
        branch->SetAddress(vi);
      else if (type == "Short_t")
        branch->SetAddress(vs);
      else if (type == "Double_t")
        branch->SetAddress(vd);
      else if (type == "Float_t")
        branch->SetAddress(vf);
      else
        std::cerr << "ERROR: branch " << name << " must be Int_t, Short_t, Double_t, Float_t" << std::endl;
    };

    bool loadID = (Config::BranchID != "") or Config::DoCalibrations;
    if (loadID) {
      std::cout << "Loading ID branch" << std::endl;
      setDiscreteBranch(Config::BranchID,Config::TypeID,b_ID,&ID,&ID_s);
    }
    
    TTreeFormula *wf;
    if (Config::UseWeight) {
      std::cout << "Loading Weight branch" << std::endl;
      wf = new TTreeFormula("wf",Config::WeightFormula,tree);
    } else {
      wf = new TTreeFormula("wf","1",tree);
    }

    if (Config::UseTau) {
      std::cout << "Loading Tau branch" << std::endl;
      setFPBranch(Config::BranchTau,Config::TypeTau,b_Tau,&Tau,&Tau_f);
    }

    if (Config::UseTauErr) {
      std::cout << "Loading Tau branch" << std::endl;
      setFPBranch(Config::BranchTauErr,Config::TypeTauErr,b_TauErr,&TauErr,&TauErr_f);
    }
    
    if (Config::UseTau or Config::UseTauErr) {
      if (Config::TauUnits == "ps")
        scale = 1.0;
      else if (Config::TauUnits == "ns")
        scale = 1000.0;
      else if (Config::TauUnits == "fs")
        scale = 0.001;
      else
        scale = 1.0;
    }

    std::cout << "Loading T branch" << std::endl;
    setDiscreteBranch("T",Config::TypeT,b_T,&T,&T_s);

    std::cout << "Loading TaggerType branch" << std::endl;
    setArrayBranch("TaggerType",Config::TypeTaggerType,b_TaggerType,&TaggerType,&TaggerType_s,&TaggerType_d,&TaggerType_f);

    std::cout << "Loading TaggerDecision branch" << std::endl;
    setArrayBranch("TaggerDecision",Config::TypeTaggerDecision,b_TaggerDecision,&TaggerDecision,&TaggerDecision_s,&TaggerDecision_d,&TaggerDecision_f);

    std::cout << "Loading TaggerOmega branch" << std::endl;
    setFPArrayBranch("TaggerOmega",Config::TypeTaggerOmega,b_TaggerOmega,&TaggerOmega,&TaggerOmega_f);

    for (int ev = 0; ev < nevents; ev++) {

      evCount++;

      tree->GetEntry(myelist->GetEntry(ev));
      if (ev%10000 == 0) std::cout << "ON EVENT " << ev << "\n";

      auto convertDiscrete = [] (TString type, Int_t i, Short_t s) -> Int_t {
        if (type == "Int_t")
          return i;
        else if (type == "Short_t")
          return static_cast<Int_t>(s);
        return i;
      };

      auto convertFP = [] (TString type, Double_t d, Float_t f) -> Double_t {
        if (type == "Double_t")
          return d;
        else if (type == "Float_t")
          return static_cast<Double_t>(f);
        return d;
      };

      auto convertArbitrary = [] (TString type, Int_t i, Short_t s, Double_t d, Float_t f) -> Int_t {
        if (type == "Int_t")
          return i;
        else if (type == "Short_t")
          return static_cast<Int_t>(s);
        else if (type == "Double_t")
          return static_cast<Int_t>(d);
        else if (type == "Float_t")
          return static_cast<Int_t>(f);
        return i;
      };

      if (loadID) {
	ID = convertDiscrete(Config::TypeID,ID,ID_s);
      }
      if (Config::UseWeight) {
	Weight = wf->EvalInstance();
      }
      if (Config::UseTau)
        Tau = convertFP(Config::TypeTau,Tau,Tau_f);
      if (Config::UseTauErr)
        TauErr = convertFP(Config::TypeTauErr,TauErr,TauErr_f);

      InclusiveDec = MuonDec = ElectronDec = KaonDec = KaonNNDec = VtxChargeDec = CharmDec = 0;
      SSKaonDec = SSKaonNNDec = SSPionDec = SSPionBDTDec = SSProtonDec = 0;
      InclusiveOmega = MuonOmega = ElectronOmega = KaonOmega = KaonNNOmega = VtxChargeOmega = CharmOmega = 0.5;
      SSKaonOmega = SSKaonNNOmega = SSPionOmega = SSPionBDTOmega = SSProtonOmega = 0.5;

      for (int i = 0; i < T; i++) {
        Int_t type;
        Int_t dec;
        Double_t omega;
	
	type = convertArbitrary(Config::TypeTaggerType,TaggerType[i],TaggerType_s[i],TaggerType_d[i],TaggerType_f[i]);
	dec = convertArbitrary(Config::TypeTaggerDecision,TaggerDecision[i],TaggerDecision_s[i],TaggerDecision_d[i],TaggerDecision_f[i]);

	omega = convertFP(Config::TypeTaggerOmega,TaggerOmega[i],TaggerOmega_f[i]);

        Tagger::Type ttype = Tagger::GetTaggerFromBTACode(type);
        if (ttype == Tagger::OS_Muon) {
          MuonDec = dec; MuonOmega = omega;
        } else if (ttype == Tagger::OS_Electron) {
          ElectronDec = dec; ElectronOmega = omega;
        } else if (ttype == Tagger::OS_Kaon) {
          KaonDec = dec; KaonOmega = omega;
        } else if (ttype == Tagger::VtxCharge) {
          VtxChargeDec = dec; VtxChargeOmega = omega;
        } else if (ttype == Tagger::OS_Charm) {
          CharmDec = dec; CharmOmega = omega;
        } else if (ttype == Tagger::SS_Pion) {
          SSPionDec = dec; SSPionOmega = omega;
        } else if (ttype == Tagger::SS_Kaon) {
          if (Config::CalibrationMode == CalibrationMode::ChargedBu)
	    dec *= -1;
	  SSKaonDec = dec;
	  SSKaonOmega = omega;
        } else if (ttype == Tagger::Inclusive) {
          InclusiveDec = dec; InclusiveOmega = omega;
        }
        
      }
      
      Tagger tMuon(Tagger::OS_Muon,MuonDec,MuonOmega);
      Tagger tElectron(Tagger::OS_Electron,ElectronDec,ElectronOmega);
      Tagger tKaon(Tagger::OS_Kaon,KaonDec,KaonOmega);
      Tagger tKaonNN(Tagger::OS_nnetKaon,KaonNNDec,KaonNNOmega);
      Tagger tVtxCharge(Tagger::VtxCharge,VtxChargeDec,VtxChargeOmega);
      Tagger tCharm(Tagger::OS_Charm,CharmDec,CharmOmega);
      Tagger tSSKaon(Tagger::SS_Kaon,SSKaonDec,SSKaonOmega);
      Tagger tSSKaonNN(Tagger::SS_nnetKaon,SSKaonNNDec,SSKaonNNOmega);
      Tagger tSSPion(Tagger::SS_Pion,SSPionDec,SSPionOmega);
      Tagger tSSPionBDT(Tagger::SS_PionBDT,SSPionBDTDec,SSPionBDTOmega);
      Tagger tSSProton(Tagger::SS_Proton,SSProtonDec,SSProtonOmega);
      Tagger tInclusive(Tagger::Inclusive,InclusiveDec,InclusiveOmega);

      Taggers taggers;
      if (Tagger::IsActive(Tagger::Type::OS_Muon))
        taggers.push_back(&tMuon); 
      if (Tagger::IsActive(Tagger::Type::OS_Electron))
        taggers.push_back(&tElectron); 
      if (Tagger::IsActive(Tagger::Type::OS_Kaon))
        taggers.push_back(&tKaon); 
      if (Tagger::IsActive(Tagger::Type::OS_nnetKaon))
        taggers.push_back(&tKaonNN); 
      if (Tagger::IsActive(Tagger::Type::VtxCharge))
        taggers.push_back(&tVtxCharge); 
      if (Tagger::IsActive(Tagger::Type::OS_Charm))
        taggers.push_back(&tCharm); 
      if (Tagger::IsActive(Tagger::Type::SS_Kaon))
        taggers.push_back(&tSSKaon); 
      if (Tagger::IsActive(Tagger::Type::SS_nnetKaon))
        taggers.push_back(&tSSKaonNN); 
      if (Tagger::IsActive(Tagger::Type::SS_Pion))
        taggers.push_back(&tSSPion); 
      if (Tagger::IsActive(Tagger::Type::SS_PionBDT))
        taggers.push_back(&tSSPionBDT); 
      if (Tagger::IsActive(Tagger::Type::SS_Proton))
        taggers.push_back(&tSSProton); 
      if (Tagger::IsActive(Tagger::Type::Inclusive))
        taggers.push_back(&tInclusive); 
    
      Int_t TrueTag = (ID > 0) ? 1 : ((ID < 0) ? -1 : 0);
      Flavour decayFlavour = FlavourFromInt(TrueTag);
      perfmon.increment(taggers,decayFlavour,Tau*scale,TauErr*scale,Weight);
    }

    perfmon.printStats();

  }
}
