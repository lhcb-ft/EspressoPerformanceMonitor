#include "CombinationAlgorithms.hh"

Tagger::TypeSet performOSCombination(const Taggers& taggers, int& decision, double& omega) {
  auto in_os_comb = [] (Tagger::Type t) {
    return Tagger::InOSComb(t);
  };
  return performCombination(taggers, decision, omega, in_os_comb);
}

Tagger::TypeSet performSSCombination(const Taggers& taggers, int& decision, double& omega) {
  auto in_ss_comb = [] (Tagger::Type t) {
    return Tagger::InSSComb(t);
  };
  return performCombination(taggers, decision, omega, in_ss_comb);
}

Tagger::TypeSet performOSplusSSCombination(const Taggers& taggers, int& decision, double& omega) {
  auto in_comb = [] (Tagger::Type t) {
    return Tagger::InComb(t);
  };
  return performCombination(taggers, decision, omega, in_comb);
}

