#include "PrintUtilities.hh"
#include <sstream>
#include <iomanip>
#include <cmath>

std::string Utilities::formatPercent(double value, bool latex) {
  int precision = (latex) ? 3 : 4;
  std::stringstream ss;
  if (latex) ss << "$";
  ss << std::fixed << std::setprecision(precision) << value*100;
  if (latex) ss << "\\";
  ss << "%";
  if (latex) ss << "$";
  std::string s = ss.str();
  return s;
}
  
std::string Utilities::formatPercentWithBinomErr(double _numPass, double _numTot, bool latex) {
  int precision = (latex) ? 3 : 4;
  double value = _numPass / _numTot;
  double error = sqrt(_numPass) / _numTot;
  std::stringstream ss;
  if (latex) ss << "$";
  ss << "(" << std::fixed << std::setprecision(precision) << value*100;
  if (latex)
    ss << "\\pm";
  else
    ss << " \u00b1 ";
  ss << std::fixed << std::setprecision(precision) << error*100 << ")";
  if (latex) ss << "\\";
  ss << "%";
  if (latex) ss << "$";
  std::string s = ss.str();
  return s;
}
  
std::string Utilities::formatPercentWithErr(double value, double error, bool latex) {
  int precision = (latex) ? 3 : 4;
  std::stringstream ss;
  if (latex) ss << "$";
  ss << "(" << std::fixed << std::setprecision(precision) << value*100;
  if (latex)
    ss << "\\pm";
  else
    ss << " \u00b1 ";
  ss << std::fixed << std::setprecision(precision) << error*100 << ")";
  if (latex) ss << "\\";
  ss << "%";
  if (latex) ss << "$";
  std::string s = ss.str();
  return s;
}

std::string Utilities::formatPercentWithDoubleErr(double value,
						  double error1, std::string name1,
						  double error2, std::string name2,
						  bool latex) {
  int precision = (latex) ? 3 : 4;
  std::stringstream ss;
  if (latex) ss << "$";
  ss << "(" << std::fixed << std::setprecision(precision) << value*100;
  if (latex)
    ss << "\\pm";
  else
    ss << " \u00b1 ";
  ss << std::fixed << std::setprecision(precision) << error1*100;
  ss << "(";
  if (latex) ss << "\\textrm{";
  ss << name1;
  if (latex) ss << "}";
  ss << ")";
  if (latex)
    ss << "\\pm";
  else
    ss << " \u00b1 ";
  ss << std::fixed << std::setprecision(precision) << error2*100;
  ss << "(";
  if (latex) ss << "\\textrm{";
  ss << name2;
  if (latex) ss << "}";
  ss << "))";
  if (latex) ss << "\\";
  ss << "%";
  if (latex) ss << "$";
  std::string s = ss.str();
  return s;
}

std::string Utilities::formatNumberWithErr(double value, double error, bool latex) {
  int precision = (latex) ? 3 : 4;
  std::stringstream ss;
  ss << std::fixed << std::setprecision(precision) << value;
  if (latex)
    ss << "\\pm";
  else
    ss << " \u00b1 ";
  ss << std::fixed << std::setprecision(precision) << error;
  std::string s = ss.str();
  return s;
}

std::string Utilities::formatNumberWithDoubleErr(double value,
						 double error1, std::string name1,
						 double error2, std::string name2,
						 bool latex) {
  int precision = (latex) ? 3 : 4;
  std::stringstream ss;
  if (latex) ss << "$";
  ss << std::fixed << std::setprecision(precision) << value*100;
  if (latex)
    ss << "\\pm";
  else
    ss << " \u00b1 ";
  ss << std::fixed << std::setprecision(precision) << error1*100;
  ss << "(";
  if (latex) ss << "\\textrm{";
  ss << name1;
  if (latex) ss << "}";
  ss << ")";
  if (latex)
    ss << "\\pm";
  else
    ss << " \u00b1 ";
  ss << std::fixed << std::setprecision(precision) << error2*100;
  ss << "(";
  if (latex) ss << "\\textrm{";
  ss << name2;
  if (latex) ss << "}";
  if (latex) ss << "$";
  std::string s = ss.str();
  return s;
}
