#include "globals.hh"
#include "GaudiIOTools.hh"
#include "Tagger.hh"

namespace Config {

  // IO SETTINGS
  std::string ConfigFileName = "./options/example.py";
  std::string BinaryDir = "@CMAKE_BINARY_DIR@";
  int DBGLEVEL = 5; //global

  // GENERIC
  int NumFiles = 1;
  std::vector<TString> filelist(0);
  TString m_tupleName = "mytagging";
  int Nmax = -1;
  TString Selection = "";

  // CALIBRATIONS
  bool DoCalibrations = false;
  int CalibrationDegree = 1;
  bool UseInputModel = false;
  Espresso::GLMModel::GLMType CalibrationModel = Espresso::GLMModel::GLMType::Polynomial;
  Espresso::Regression::LinkType CalibrationLink = Espresso::Regression::LinkType::Mistag;
  Espresso::CalibrationMode CalibrationMode = Espresso::CalibrationMode::ChargedBu;
  unsigned int CovCorrMethod = 2;
  bool UseNewtonRaphson = false;
  bool SaveCalibrationsToXML = false;

  bool WriteCalibratedMistagBranches = false;

  bool PerformOfflineCombination_OS = false;
  bool PerformOfflineCombination_SS = false;
  bool PerformOfflineCombination_OSplusSS = false;

  TString CalibratedOutputFile = "EspressoCalibratedOutput.root";

  double ProductionAsymmetry = 0.0;

  double ResolutionGaussian1_A = 0.0;
  double ResolutionGaussian2_A = 0.0;
  double ResolutionGaussian3_A = 0.0;
  double ResolutionGaussian1_B = 1.0;
  double ResolutionGaussian2_B = 1.0;
  double ResolutionGaussian3_B = 1.0;
  double ResolutionGaussian1_C = 0.0;
  double ResolutionGaussian2_C = 0.0;
  double ResolutionGaussian3_C = 0.0;
  double ResolutionGaussian1_Frac = 1.0;
  double ResolutionGaussian2_Frac = 0.0;

  // PHYSICS
  double DeltaM = -1.0;
  double DeltaMErr = -1.0;
  double Lifetime = -1.0;
  double LifetimeErr = -1.0;
  double DeltaGamma = -1.0;
  double DeltaGammaErr = -1.0;

  // SIMPLEEVALUATOR

  bool WriteTaggerDecisionTable = false;
  int NumKernelSmoothingPoints = 1000;
  double KernelSmoothingWidth = 0.01;
  bool DrawOscillationPlots = false; // important for blinding
  double OscillationPlotsMaximum = 15.0; // in ps; only applies to bd

  /* Weight formula */
  TString WeightFormula        = "";

  /* branch names */
  TString BranchID             = "";
  TString BranchTau            = "";
  TString BranchTauErr         = "";
  TString TauUnits             = "ps";

  TString BranchPrefix = "Bplus_BuTaggingTool_";

  /* data type */
  TString TypeID             = "Int_t";
  TString TypeTau            = "Double_t";
  TString TypeTauErr         = "Double_t";
  TString TypeT              = "Int_t";
  TString TypeTaggerType     = "Float_t";
  TString TypeTaggerDecision = "Float_t";
  TString TypeTaggerOmega    = "Float_t";

  /* use or not */
  bool UseWeight      = false;
  bool UseTau         = false;
  bool UseTauErr      = false;

  // BTAEVALUATOR

  // EVALUATOR
  
  TString NNetTrain = "none";
  TString MLPtype = "MultiLayerPerceptron";
  TString VarTransform = "none";
  
  int requireL0 = 1;
  int requireHlt1 = 0;
  int requireHlt2 = 0;
  int requireTis = 0;
  
  int UseModBtime = 0;
  int usesWeights = 0;
  int useTMVA = 0;
  int useDaVinciTaggers = 0;

  bool IsControlChannel = false;
  bool checkDV = false;
  bool removeFiles = false;
  double m_IPPU_cut = 1.0;
  double m_thetaMin = 0.012;
  double m_distphi_cut = 0.000;
  double m_Pmaxtot_cut = 200000;
  double m_Ptmaxtot_cut = 10000;
  
  TString histoFileName = "output/tag.root";
  TString trainFileName = "output/train.root";  

  // TESTSTATCAL
  unsigned int RandomSeed = 5489u;
  bool OscillationTest = false;
  bool AsymmetryNullTest = false;
  bool AsymmetryFittedTest = false;
  bool MetricNullTest = false;
  bool MetricFittedTest = false;
  bool CalibrationTest = false;
  int NumberOfToys = 250;
  int EventsPerToy = 50000;
  double BackgroundFraction = 0.0;
  double SidebandRatio = 1.0;
  double ToyAsymmetry = 0;

  bool CreateSmearedCalibration = false;
  int ToySmearDegree = 0;
  Espresso::GLMModel::GLMType ToySmearModel = Espresso::GLMModel::GLMType::Polynomial;
  Espresso::Regression::LinkType ToySmearLink = Espresso::Regression::LinkType::Mistag;

  double ToySmearP0Scale = 0.0;
  double ToySmearP1Scale = 0.0;
  double ToySmearP2Scale = 0.0;
  double ToySmearP3Scale = 0.0;
  double ToySmearP4Scale = 0.0;
  double ToySmearP5Scale = 0.0;
  double ToySmearP6Scale = 0.0;
  double ToySmearP7Scale = 0.0;
  double ToySmearP8Scale = 0.0;
  double ToySmearP9Scale = 0.0;
  double ToySmearP10Scale = 0.0;
  double ToySmearDeltaP0Scale = 0.0;
  double ToySmearDeltaP1Scale = 0.0;
  double ToySmearDeltaP2Scale = 0.0;
  double ToySmearDeltaP3Scale = 0.0;
  double ToySmearDeltaP4Scale = 0.0;
  double ToySmearDeltaP5Scale = 0.0;
  double ToySmearDeltaP6Scale = 0.0;
  double ToySmearDeltaP7Scale = 0.0;
  double ToySmearDeltaP8Scale = 0.0;
  double ToySmearDeltaP9Scale = 0.0;
  double ToySmearDeltaP10Scale = 0.0;

  TString PlotLabel = "";
  TString PlotExtension = ".png";
  bool PlotTitle = true;
  bool PlotStatBox = true;
}

void Config::readParameters() {

  /////////////
  // IO //
  /////////////

  declareProperty( "DBGLEVEL",  DBGLEVEL );

  /////////////
  // GENERIC //
  /////////////

  // For backwards compatibility
  filelist.push_back("");
  declareProperty( "datafile", filelist[0]);
  if (filelist[0] == "") {
    declareProperty( "RootFile", filelist[0]);
    if (filelist[0] == "") {
      declareProperty( "NumFiles", NumFiles);
      filelist.resize(NumFiles);
      for (int k = 0; k < NumFiles; ++k) {
        char name[12]; sprintf(name,"RootFile_%d",k+1);
        declareProperty(name, filelist[k]);
      }
    }
  }

  declareProperty( "TupleName", m_tupleName);

  declareProperty( "Nmax", Nmax);

  declareProperty( "Selection", Selection);
  
  /////////////////
  // CALIBRATION //
  /////////////////

  declareProperty( "DoCalibrations", DoCalibrations);
  declareProperty( "CalibrationDegree", CalibrationDegree);
  declareProperty( "UseInputModel", UseInputModel);

  TString CalibrationModelName = "POLY";
  declareProperty( "CalibrationModel", CalibrationModelName);
  if (CalibrationModelName == "POLY")
    CalibrationModel = Espresso::GLMModel::GLMType::Polynomial;
  else if (CalibrationModelName == "BSPLINE")
    CalibrationModel = Espresso::GLMModel::GLMType::BSpline;
  else if (CalibrationModelName == "NSPLINE")
    CalibrationModel = Espresso::GLMModel::GLMType::NSpline;
  else
    std::cerr << "ERROR: NOT A VALID GLM MODEL" << std::endl;

  TString CalibrationLinkName = "MISTAG";
  declareProperty( "CalibrationLink", CalibrationLinkName);
  if (CalibrationLinkName == "MISTAG")
    CalibrationLink = Espresso::Regression::LinkType::Mistag;
  else if (CalibrationLinkName == "LOGIT")
    CalibrationLink = Espresso::Regression::LinkType::Logit;
  else if (CalibrationLinkName == "RLOGIT")
    CalibrationLink = Espresso::Regression::LinkType::RLogit;
  else if (CalibrationLinkName == "PROBIT")
    CalibrationLink = Espresso::Regression::LinkType::Probit;
  else if (CalibrationLinkName == "RPROBIT")
    CalibrationLink = Espresso::Regression::LinkType::RProbit;
  else if (CalibrationLinkName == "CAUCHIT")
    CalibrationLink = Espresso::Regression::LinkType::Cauchit;
  else if (CalibrationLinkName == "RCAUCHIT")
    CalibrationLink = Espresso::Regression::LinkType::RCauchit;
  else
    std::cerr << "ERROR: NOT A VALID LINK FUNCTION" << std::endl;
  
  TString CalModeName = "Bu";
  declareProperty( "CalibrationMode", CalModeName);
  if (CalModeName == "ChargedBu" or CalModeName == "Bu")
    CalibrationMode = Espresso::CalibrationMode::ChargedBu;
  else if (CalModeName == "NeutralBd" or CalModeName == "Bd")
    CalibrationMode = Espresso::CalibrationMode::NeutralBd;
  else if (CalModeName == "NeutralBs" or CalModeName == "Bs")
    CalibrationMode = Espresso::CalibrationMode::NeutralBs;
  else
    std::cerr << "ERROR: NOT A VALID B TYPE" << std::endl;
  
  declareProperty( "CovCorrMethod", CovCorrMethod = 2);
  declareProperty( "UseNewtonRaphson", UseNewtonRaphson = true);
  declareProperty( "SaveCalibrationsToXML", SaveCalibrationsToXML = false);

  declareProperty( "WriteCalibratedMistagBranches", WriteCalibratedMistagBranches = false);

  declareProperty( "PerformOfflineCombination_OS", PerformOfflineCombination_OS = false);
  declareProperty( "PerformOfflineCombination_SS", PerformOfflineCombination_SS = false);
  declareProperty( "PerformOfflineCombination_OSplusSS", PerformOfflineCombination_OSplusSS = false);

  declareProperty( "CalibratedOutputFile", CalibratedOutputFile);

  declareProperty( "ProductionAsymmetry", ProductionAsymmetry);

  declareProperty( "ResolutionGaussian1_A", ResolutionGaussian1_A);
  declareProperty( "ResolutionGaussian1_B", ResolutionGaussian1_B);
  declareProperty( "ResolutionGaussian1_C", ResolutionGaussian1_C);
  declareProperty( "ResolutionGaussian2_A", ResolutionGaussian2_A);
  declareProperty( "ResolutionGaussian2_B", ResolutionGaussian2_B);
  declareProperty( "ResolutionGaussian2_C", ResolutionGaussian2_C);
  declareProperty( "ResolutionGaussian3_A", ResolutionGaussian3_A);
  declareProperty( "ResolutionGaussian3_B", ResolutionGaussian3_B);
  declareProperty( "ResolutionGaussian3_C", ResolutionGaussian3_C);
  declareProperty( "ResolutionGaussian1_Frac", ResolutionGaussian1_Frac);
  declareProperty( "ResolutionGaussian2_Frac", ResolutionGaussian2_Frac);

  /////////////
  // PHYSICS //
  /////////////
  
  declareProperty( "DeltaM", DeltaM);
  declareProperty( "DeltaMErr", DeltaMErr);
  declareProperty( "Lifetime", Lifetime);
  declareProperty( "LifetimeErr", LifetimeErr);
  declareProperty( "DeltaGamma", DeltaGamma);
  declareProperty( "DeltaGammaErr", DeltaGammaErr);

  //////////////////////
  // SIMPLE EVALUATOR //
  //////////////////////

  declareProperty( "WriteTaggerDecisionTable", WriteTaggerDecisionTable = false);
  declareProperty( "NumKernelSmoothingPoints", NumKernelSmoothingPoints);
  declareProperty( "KernelSmoothingWidth", KernelSmoothingWidth);
  declareProperty( "DrawOscillationPlots", DrawOscillationPlots);
  declareProperty( "OscillationPlotsMaximum", OscillationPlotsMaximum);

  /* Weight formula */
  declareProperty( "WeightFormula",   WeightFormula);
  if (WeightFormula == "") {
    declareProperty( "BranchWeight",   WeightFormula);
  }

  /* branch names */
  declareProperty( "BranchID",   BranchID);
  declareProperty( "BranchTau",   BranchTau);
  declareProperty( "BranchTauErr",   BranchTauErr);
  declareProperty( "TauUnits", TauUnits);

  /* data type */
  declareProperty( "TypeID",   TypeID);
  declareProperty( "TypeTau",   TypeTau);
  declareProperty( "TypeTauErr",   TypeTauErr);

  /* use or not */
  declareProperty( "UseWeight",   UseWeight);
  declareProperty( "UseTau",   UseTau);
  declareProperty( "UseTauErr",   UseTauErr);

  ///////////////
  // EVALUATOR //
  ///////////////

  declareProperty( "IsControlChannel",  IsControlChannel);

  declareProperty( "RemoveCorruptedFiles", removeFiles);
  declareProperty( "NNetTrain",   NNetTrain);
  declareProperty( "MLPtype",     MLPtype);

  declareProperty( "requireL0",   requireL0);
  declareProperty( "requireHlt1", requireHlt1);
  declareProperty( "requireHlt2", requireHlt2);
  declareProperty( "requireTis",  requireTis);

  declareProperty( "HistoFileName",    histoFileName); 
  declareProperty( "TrainFileName",    trainFileName); 
  declareProperty( "checkDV", checkDV);

  declareProperty( "IPPU_cut",     m_IPPU_cut);
  declareProperty( "thetaMin_cut", m_thetaMin);
  declareProperty( "distphi_cut",  m_distphi_cut);
  declareProperty( "Pmaxtot",      m_Pmaxtot_cut);
  declareProperty( "Ptmaxtot",     m_Ptmaxtot_cut);

  declareProperty( "UseModBtime",  UseModBtime);
  declareProperty( "usesWeights",  usesWeights);
  declareProperty ("useTMVA"   ,  useTMVA);
  declareProperty ("UseDaVinciTaggers" , useDaVinciTaggers);

  declareProperty ("VarTransform" , VarTransform);

  /////////////////
  // TESTSTATCAL //
  /////////////////

  declareProperty( "RandomSeed", RandomSeed );
  declareProperty( "OscillationTest", OscillationTest);
  declareProperty( "AsymmetryNullTest", AsymmetryNullTest);
  declareProperty( "AsymmetryFittedTest", AsymmetryFittedTest);
  declareProperty( "MetricNullTest", MetricNullTest);
  declareProperty( "MetricFittedTest", MetricFittedTest);
  declareProperty( "CalibrationTest", CalibrationTest);
  declareProperty( "NumberOfToys", NumberOfToys);
  declareProperty( "EventsPerToy", EventsPerToy);
  declareProperty( "BackgroundFraction", BackgroundFraction);
  declareProperty( "SidebandRatio", SidebandRatio);
  declareProperty( "ToyAsymmetry", ToyAsymmetry);

  declareProperty( "CreateSmearedCalibration", CreateSmearedCalibration);
  declareProperty( "ToySmearDegree", ToySmearDegree);

  TString ToySmearModelName = "POLY";
  declareProperty( "ToySmearModel", ToySmearModelName);
  if (ToySmearModelName == "POLY")
    ToySmearModel = Espresso::GLMModel::GLMType::Polynomial;
  else if (ToySmearModelName == "BSPLINE")
    ToySmearModel = Espresso::GLMModel::GLMType::BSpline;
  else if (ToySmearModelName == "NSPLINE")
    ToySmearModel = Espresso::GLMModel::GLMType::NSpline;
  else
    std::cerr << "ERROR: NOT A VALID GLM MODEL" << std::endl;

  TString ToySmearLinkName = "MISTAG";
  declareProperty( "ToySmearLink", ToySmearLinkName);
  if (ToySmearLinkName == "MISTAG")
    ToySmearLink = Espresso::Regression::LinkType::Mistag;
  else if (ToySmearLinkName == "LOGIT")
    ToySmearLink = Espresso::Regression::LinkType::Logit;
  else if (ToySmearLinkName == "RLOGIT")
    ToySmearLink = Espresso::Regression::LinkType::RLogit;
  else if (ToySmearLinkName == "PROBIT")
    ToySmearLink = Espresso::Regression::LinkType::Probit;
  else if (ToySmearLinkName == "RPROBIT")
    ToySmearLink = Espresso::Regression::LinkType::RProbit;
  else if (ToySmearLinkName == "CAUCHIT")
    ToySmearLink = Espresso::Regression::LinkType::Cauchit;
  else if (ToySmearLinkName == "RCAUCHIT")
    ToySmearLink = Espresso::Regression::LinkType::RCauchit;
  else
    std::cerr << "ERROR: NOT A VALID LINK FUNCTION" << std::endl;

  declareProperty( "ToySmearP0Scale", ToySmearP0Scale);
  declareProperty( "ToySmearP1Scale", ToySmearP1Scale);
  declareProperty( "ToySmearP2Scale", ToySmearP2Scale);
  declareProperty( "ToySmearP3Scale", ToySmearP3Scale);
  declareProperty( "ToySmearP4Scale", ToySmearP4Scale);
  declareProperty( "ToySmearP5Scale", ToySmearP5Scale);
  declareProperty( "ToySmearP6Scale", ToySmearP6Scale);
  declareProperty( "ToySmearP7Scale", ToySmearP7Scale);
  declareProperty( "ToySmearP8Scale", ToySmearP8Scale);
  declareProperty( "ToySmearP9Scale", ToySmearP9Scale);
  declareProperty( "ToySmearP10Scale", ToySmearP10Scale);
  declareProperty( "ToySmearDeltaP0Scale", ToySmearDeltaP0Scale);
  declareProperty( "ToySmearDeltaP1Scale", ToySmearDeltaP1Scale);
  declareProperty( "ToySmearDeltaP2Scale", ToySmearDeltaP2Scale);
  declareProperty( "ToySmearDeltaP3Scale", ToySmearDeltaP3Scale);
  declareProperty( "ToySmearDeltaP4Scale", ToySmearDeltaP4Scale);
  declareProperty( "ToySmearDeltaP5Scale", ToySmearDeltaP5Scale);
  declareProperty( "ToySmearDeltaP6Scale", ToySmearDeltaP6Scale);
  declareProperty( "ToySmearDeltaP7Scale", ToySmearDeltaP7Scale);
  declareProperty( "ToySmearDeltaP8Scale", ToySmearDeltaP8Scale);
  declareProperty( "ToySmearDeltaP9Scale", ToySmearDeltaP9Scale);
  declareProperty( "ToySmearDeltaP10Scale", ToySmearDeltaP10Scale);
  
  ////////////////////////////
  // CALIBRATION PARAMETERS //
  ////////////////////////////  

  for (int k = Tagger::Type_SMIN; k <= Tagger::Type_MAX; k++) {
    Tagger::Type t = static_cast<Tagger::Type>(k);
    std::string name = Tagger::Name(t);

    // Evaluator settings
    declareProperty( name + "_Use",   Tagger::IsActive(t));
    declareProperty( name + "_BranchDec",   Tagger::BranchDec(t));
    declareProperty( name + "_BranchProb",   Tagger::BranchProb(t));
    declareProperty( name + "_TypeDec",   Tagger::TypeDec(t));
    declareProperty( name + "_TypeProb",   Tagger::TypeProb(t));
    
    // Calibration settings
    declareProperty( name + "_CalibrationArchive", Tagger::CalibrationArchive(t));

    TString LinkName = "MISTAG";
    declareProperty( name + "_Link", LinkName);
    if (LinkName == "MISTAG")
      Tagger::StandardCalibration(t).Link() = Espresso::Regression::LinkType::Mistag;
    else if (LinkName == "LOGIT")
      Tagger::StandardCalibration(t).Link() = Espresso::Regression::LinkType::Logit;
    else if (LinkName == "PROBIT")
      Tagger::StandardCalibration(t).Link() = Espresso::Regression::LinkType::Probit;
    else if (LinkName == "CAUCHIT")
      Tagger::StandardCalibration(t).Link() = Espresso::Regression::LinkType::Cauchit;
    else
      std::cerr << "ERROR: NOT A VALID LINK FUNCTION" << std::endl;

    declareProperty( name + "_Eta", Tagger::StandardCalibration(t).Eta());
    declareProperty( name + "_P0", Tagger::StandardCalibration(t).P0());
    declareProperty( name + "_P0Err", Tagger::StandardCalibration(t).P0Err());
    declareProperty( name + "_P1", Tagger::StandardCalibration(t).P1());
    declareProperty( name + "_P1Err", Tagger::StandardCalibration(t).P1Err());
    declareProperty( name + "_DeltaP0", Tagger::StandardCalibration(t).DeltaP0());
    declareProperty( name + "_DeltaP0Err", Tagger::StandardCalibration(t).DeltaP0Err());
    declareProperty( name + "_DeltaP1", Tagger::StandardCalibration(t).DeltaP1());
    declareProperty( name + "_DeltaP1Err", Tagger::StandardCalibration(t).DeltaP1Err());
    declareProperty( name + "_RhoP0P1", Tagger::StandardCalibration(t).P0P1Corr());
    declareProperty( name + "_RhoDeltaP0DeltaP1", Tagger::StandardCalibration(t).DeltaP0DeltaP1Corr());

    // Combination settings
    declareProperty( name + "_InComb", Tagger::InComb(t));
    declareProperty( name + "_InOSComb", Tagger::InOSComb(t));
    declareProperty( name + "_InSSComb", Tagger::InSSComb(t));
    declareProperty( name + "_CombPow", Tagger::CombPow(t));

    // Write settings
    declareProperty( name + "_Write", Tagger::Write(t));
    
    // Histogram settings
    declareProperty( name + "_NumBins", Tagger::Binning(t));

    //Plot settings
    declareProperty("PlotLabel", PlotLabel);
    declareProperty("PlotExtension", PlotExtension);
    declareProperty("PlotTitle", PlotTitle);
    declareProperty("PlotStatBox", PlotStatBox);
    
  }

}

