#include "EspressoPerformanceMonitorImpl.hh"

#include <algorithm>
#include <iostream>
#include <sstream>
#include <fstream>

#include "GLMPolynomial.hh"
#include "GLMNSpline.hh"
#include "GLMBSpline.hh"
#include "GLMCalibration.hh"
#include "StandardCalibration.hh"

#include <TObjString.h>
#include <TGraphAsymmErrors.h>
#include <TVectorD.h>
#include <TFile.h>
#include <TString.h>
#include <TCanvas.h>
#include <TStyle.h>
#include <TROOT.h>

#include "globals.hh"
#include "Tagger.hh"
#include "CombinationAlgorithms.hh"
#include "GaudiIOTools.hh"
#include "PrintUtilities.hh"

#include "NormalDistribution.hh"
#include "UniformDistribution.hh"
#include "SumDistribution.hh"

#include "TaggingResult.hh"
#include "GLMCalibration.hh"
#include "TaggingResultMetrics.hh"
#include "TaggingResultGraphics.hh"

using namespace Espresso;
using namespace Utilities;

EspressoPerformanceMonitorImpl::EspressoPerformanceMonitorImpl()
  : model(Config::CalibrationMode),
    verbose(false),
    _numEvents(0),
    _numEventsW2(0),
    _effNumEvents(0)
{
  
  double scale = 0.0;
  if (Config::TauUnits == "ps")
    scale = 1.0;
  else if (Config::TauUnits == "ns")
    scale = 1000.0;
  else if (Config::TauUnits == "fs")
    scale = 0.001;
  else
    scale = 1.0;
  ResolutionModel resModel = [=] (double tau, double tauerr) {
    double w1 = Config::ResolutionGaussian1_A*scale + Config::ResolutionGaussian1_B*tauerr + Config::ResolutionGaussian1_C/scale*tauerr*tauerr;
    double w2 = Config::ResolutionGaussian2_A*scale + Config::ResolutionGaussian2_B*tauerr + Config::ResolutionGaussian2_C/scale*tauerr*tauerr;
    double w3 = Config::ResolutionGaussian3_A*scale + Config::ResolutionGaussian3_B*tauerr + Config::ResolutionGaussian3_C/scale*tauerr*tauerr;
    double f1 = Config::ResolutionGaussian1_Frac;
    double f2 = Config::ResolutionGaussian2_Frac;
    double f3 = 1-f1-f2;
    auto res = std::make_unique<SumDistribution>(std::make_unique<SumDistribution>(std::make_unique<NormalDistribution>(tau,w1),
                                                                                   std::make_unique<NormalDistribution>(tau,w2),f1/(f1+f2),f2/(f1+f2)),
                                                 std::make_unique<NormalDistribution>(tau,w3),f1+f2,f3);
    return res;
  };
  model.SetAsymmetry(Config::ProductionAsymmetry);
  model.SetResolutionModel(resModel);
  //Take user-defined physical parameters, if any
  if(Config::DeltaM>0)       { model.SetDeltaM( Config::DeltaM ); }
  if(Config::DeltaMErr>0)    { model.SetDeltaMErr( Config::DeltaMErr ); }
  if(Config::Lifetime>0)     { model.SetLifetime( Config::Lifetime ); }
  if(Config::LifetimeErr>0)  { model.SetLifetimeErr( Config::LifetimeErr ); }
  if(Config::DeltaGamma>0)   { model.SetDeltaGamma( Config::DeltaGamma ); }
  if(Config::DeltaGammaErr>0){ model.SetDeltaGammaErr( Config::DeltaGammaErr ); }
  
}

int EspressoPerformanceMonitorImpl::N() { return int(_numEvents); }

void EspressoPerformanceMonitorImpl::setStyle() {
  
  gROOT->SetStyle("Plain");
  TStyle *theStyle= new TStyle("theStyle","theStyle");

  theStyle->SetFillColor(1);
  theStyle->SetFillStyle(1001);
  theStyle->SetFrameFillColor(0);
  theStyle->SetFrameBorderMode(0);
  theStyle->SetPadBorderMode(0);
  theStyle->SetPadColor(0);
  theStyle->SetCanvasBorderMode(0);
  theStyle->SetCanvasColor(0);
  theStyle->SetStatColor(0);
  theStyle->SetLegendBorderSize(0);
  theStyle->SetPaperSize(20,26);
  theStyle->SetPadTopMargin(0.05);
  theStyle->SetPadRightMargin(0.05);
  theStyle->SetPadBottomMargin(0.16);
  theStyle->SetPadLeftMargin(0.14);
  theStyle->SetTextFont(132);
  theStyle->SetTextSize(0.06);
  theStyle->SetLabelFont(132,"x");
  theStyle->SetLabelFont(132,"y");
  theStyle->SetLabelFont(132,"z");
  theStyle->SetLabelSize(0.06,"x");
  theStyle->SetLabelSize(0.06,"y");
  theStyle->SetLabelSize(0.06,"z");
  theStyle->SetTitleFont(132);
  theStyle->SetTitleFont(132,"x");
  theStyle->SetTitleFont(132,"y");
  theStyle->SetTitleFont(132,"z");
  theStyle->SetTitleSize(1.2*0.06,"x");
  theStyle->SetTitleSize(1.2*0.06,"y");
  theStyle->SetTitleSize(1.2*0.06,"z");
  //theStyle->SetLineWidth(2);
  theStyle->SetFrameLineWidth(2);
  theStyle->SetHistLineWidth(2);
  //theStyle->SetFuncWidth(2);
  theStyle->SetGridWidth(2);
  theStyle->SetLineStyleString(2,"[12 12]");
  theStyle->SetMarkerStyle(20);
  theStyle->SetMarkerSize(1.0);
  theStyle->SetLabelOffset(0.010,"X");
  theStyle->SetLabelOffset(0.010,"Y");
  theStyle->SetTitleOffset(0.95,"X");
  theStyle->SetTitleOffset(0.95,"Y");
  theStyle->SetTitleOffset(1.2,"Z");
  theStyle->SetTitleFillColor(0);
  theStyle->SetTitleStyle(0);
  theStyle->SetTitleBorderSize(0);
  theStyle->SetTitleFont(132,"title");
  theStyle->SetTitleX(0.0);
  theStyle->SetTitleY(1.0);
  theStyle->SetTitleW(1.0);
  theStyle->SetTitleH(0.05);
  theStyle->SetPadTickX(1);
  theStyle->SetPadTickY(1);
  theStyle->SetNdivisions(505,"x");
  theStyle->SetNdivisions(510,"y");

  gROOT->SetStyle("theStyle");
  //gROOT->ForceStyle();

} 

void EspressoPerformanceMonitorImpl::increment(const Taggers& taggers, Flavour decayFlavour, double Btau, double BtauErr, double weight) {

  // Increment _number _tagged
  _numEvents += weight;
  _numEventsW2 += weight*weight;

  // Get arrays of _taggers, _decisions, omegas, powers, etc.
  TaggerMap<int> _tag_dec;
  TaggerMap<double> _tag_omega;

  /* Loop over taggers
   * Note that this might not be all taggers
   * e.g. BTaggingAnalysis only includes taggers with tag != 0
   */

  for (Tagger *t : taggers) {
    
    // Get values
    auto type = static_cast<Tagger::Type>(t->type());
    
    if (Tagger::Name(type) == "none")
      continue;

    int dec = t->decision();
    double eta = t->eta();
    double omega = t->omega();
    double dilution = 1.0 - 2.0*omega;

    //std::cout << "eta=" << eta << std::endl;
    //std::cout << "omega=" << omega << std::endl;

    // Save values
    _tag_dec[type] = dec;
    _tag_omega[type] = omega;

    if (dec) {
      
      Flavour predFlavour  = FlavourFromInt(dec);
      add_tag(type,decayFlavour,predFlavour,eta,weight,Btau,BtauErr); // have to add eta to calculate calibration systematics
      
    } else {
      add_tag(type,decayFlavour,Flavour::Unknown,0.5,weight,Btau,BtauErr);
    }
    
    // Update fire / decision vectors
    safe_increment(_tag_vector,type,weight*abs(dec));
    safe_increment(_dec_vector, type, weight*dec);
    safe_increment(_dil_vector, type, weight*dilution);
    safe_increment(_decdil_vector, type, weight*dec*dilution);
    safe_increment(_decdil2_vector, type, weight*abs(dec)*dilution*dilution);
  }

  // Finish combined decision for tagging set
  if (Config::PerformOfflineCombination_OS) {

    int decOS = 0;
    double omegaOS = 0.5;
    Tagger::TypeSet typeSet = performOSCombination(taggers,decOS,omegaOS);
    
    if (decOS and omegaOS < 0.5) {
      Flavour combPredFlavour  = FlavourFromInt(decOS);
      add_tag_os(typeSet,decayFlavour,combPredFlavour,omegaOS,weight,Btau,BtauErr);
      //add_tag(Tagger::OS_Combination,decayFlavour,combPredFlavour,omegaOS,weight,Btau,BtauErr);
    } else {
      //add_tag(Tagger::OS_Combination,decayFlavour,Flavour::Unknown,0.0,weight,Btau,BtauErr);
    }
  }

  // Finish combined decision for tagging set
  if (Config::PerformOfflineCombination_SS) {

    int decSS = 0;
    double omegaSS = 0.5;
    Tagger::TypeSet typeSet = performSSCombination(taggers,decSS,omegaSS);

    if (decSS and omegaSS < 0.5) {
      Flavour combPredFlavour  = FlavourFromInt(decSS);
      add_tag_ss(typeSet,decayFlavour,combPredFlavour,omegaSS,weight,Btau,BtauErr);
      //add_tag(Tagger::SS_Combination,decayFlavour,combPredFlavour,omegaSS,weight,Btau,BtauErr);
    } else {
      //add_tag(Tagger::SS_Combination,decayFlavour,Flavour::Unknown,0.0,weight,Btau,BtauErr);
    }
  }
    
  // Finish combined decision for tagging set
  if (Config::PerformOfflineCombination_OSplusSS) {

    int dec = 0;
    double omega = 0.5;
    Tagger::TypeSet typeSet = performOSplusSSCombination(taggers,dec,omega);

    if (dec and omega < 0.5) {
      Flavour combPredFlavour  = FlavourFromInt(dec);
      add_tag_osss(typeSet,decayFlavour,combPredFlavour,omega,weight,Btau,BtauErr);
      //add_tag(Tagger::Combination,decayFlavour,combPredFlavour,omega,weight,Btau,BtauErr);
    } else {
      //add_tag(Tagger::Combination,decayFlavour,Flavour::Unknown,0.0,weight,Btau,BtauErr);
    }
  }

  // Correlation Matrices
  for (int i = Tagger::Type_SMIN; i <= Tagger::Type_MAX; i++) {
    for (int j = Tagger::Type_SMIN; j <= Tagger::Type_MAX; j++) {
   
      auto trow = static_cast<Tagger::Type>(i);
      auto tcol = static_cast<Tagger::Type>(j);
      auto pair = std::make_pair(trow,tcol);

      bool both = bool(_tag_dec[trow]) and bool(_tag_dec[tcol]);
      safe_increment(_tag_matrix, pair, weight*both);
      safe_increment(_dec_matrix, pair, weight*_tag_dec[trow]*_tag_dec[tcol]);
      if (both) {
        safe_increment(_dec_comatrix_left, pair, weight*_tag_dec[trow]);
        safe_increment(_dec_comatrix_right, pair, weight*_tag_dec[tcol]);
      }

      double _dil_row = 1.0 - 2.0*_tag_omega[trow];
      double _dil_col = 1.0 - 2.0*_tag_omega[tcol];
      safe_increment(_decdil_matrix, pair, weight*_tag_dec[trow]*_tag_dec[tcol]*_dil_row*_dil_col);
    }
  }
  
}
  
const TaggingResult& EspressoPerformanceMonitorImpl::GetTaggingResult(Tagger::Type type) const {
  auto it = _taggerCollectors.find(type);
  if (it == _taggerCollectors.end()) {
    _taggerCollectors.emplace(std::make_pair(type,TagCollector(model,Tagger::Calibration(type))));
    it = _taggerCollectors.find(type);
    
  }
  return it->second.GetResult();
}

void EspressoPerformanceMonitorImpl::printStats() {

  std::cout << BOLD
            << "--------------------------------------------------\n"
            << "----- ESPRESSO PERFORMANCE MONITOR STATISTICS ----\n"
            << "--------------------------------------------------\n"
            << ENDC;

  setStyle();
  if(!Config::PlotStatBox) gStyle->SetOptStat(0);

  if (_numEvents == 0) {
    std::cout << "No events selected." << std::endl;
    return;
  } else {
    std::cout << _numEvents << " events selected.\n\n";
  }
  _effNumEvents = _numEvents*_numEvents / _numEventsW2;
  
  auto printOSPerformanceTable = [&] (TaggerPairMap<double> values) {
    
    std::cout << std::setw(15) << "";
    for (int i = Tagger::Type_SMIN; i <= Tagger::Type_MAX; i++) {
      auto type = static_cast<Tagger::Type>(i);
      auto it = values.find(std::make_pair(type,type));
      if (it == values.end())
        continue;
      std::cout << std::setw(15) << Tagger::Name(type);
    }
    std::cout << ENDC << std::endl;
    for (int i = Tagger::Type_SMIN; i <= Tagger::Type_MAX; i++) {
      auto type = static_cast<Tagger::Type>(i);
      auto it = values.find(std::make_pair(type,type));
      if (it == values.end())
        continue;
      std::cout << BOLD << std::setw(15) << Tagger::Name(type);
      std::cout << ENDC;
      for (int j = Tagger::Type_SMIN; j <= Tagger::Type_MAX; j++) {
        auto type2 = static_cast<Tagger::Type>(j);
        auto pair = std::make_pair(type,type2);
        auto it = values.find(std::make_pair(type2,type2));
        if (it == values.end())
          continue;
        double val = values[pair];
        std::cout << std::setw(15) << formatPercent(val);
      }
      std::cout << std::endl;      
    }
  };
  
  ///////////////////////////////////
  // CREATING CORRELATION MATRICES //
  ///////////////////////////////////
  
  /* There are two interesting kinds of correlation to report:
     1) Correlation in firing
     Here, there is a variable equal to 0 or 1 for each tagger
     2) Correlation in decision
     Here, there is a variable equal to 0, -1, or 1 for each tagger
     3) Correlation when at least one fires
     Here, there is a variable equal to 0, -1 or 1
     Everything is equivalent to #2 except numEvents should be replaced with the number of events for which either fires
     4) Correlation when bothfire
     Here, there is a variable equal to just 1 or 1
     Because 0*anything = 0  and 0 + anything = anything ...
     Everything is equivalent to #2 except numEvents should be replaced with the number of events for which they both fire
     
     Before, only an attempt at 1) was made, and this wasn't quite the correlation matrix
     Instead, for two taggers X and Y, what was shown was 
     
     E[X && Y] = E[X*Y]
       
     First, we must find the covariance
       
     cov(X,Y) = E[(X-<X>)*(Y-<Y>)] = E[X*Y] - E[X]*E[Y]
       
     (For 2), E[X] and E[Y] should both be close to 0).

     Then the correlation is found by

     corr(X,Y) = cov(X,Y) / sqrt(cov(X,X)*cov(Y,Y))

     By definition,

     cov(X,X) = E[X^2] - E[X]^2

     But since X1 = 0/1 in the firing case and X2 = 0/-1/1 in the decision case,

     X1^2 = X1 and X2^2 = X1
     
     So,

     E[X1^2] = E[X2^2] = E[X1] = efftag

     So, the variance is given by

     cov(X1,X1) = cov(X2,X2) = efftag*(1-efftag)

     and therefore

     corr(X,Y) = cov(X,Y) / sqrt(efftagX*(1-efftag_X)*eff_tagY*(1-eff_tag_Y))

     where for 1)

     cov(X1,Y1) = E[X1*Y1] - eff_tag_X*eff_tag_Y

     and for 2)

     cov(X2,Y2) = E[X2*Y2] - E[X2]*E[Y2]

  */

  // FIRING CO-FRACTIONS

  TaggerPairMap<double> _fire_corr;

  TaggerPairMap<double> _dec_corr;

  TaggerPairMap<double> _dec_COcorr;

  TaggerPairMap<double> _decdil_corr;

  // CORRELATION CALCULATOR
  auto correlation = [&] (double sXY, double sX, double sX2, double sY, double sY2, double effN) {
    double corr = sXY/effN - sX/effN * sY/effN;
    corr /= sqrt(sX2/effN - pow(sX/effN,2));
    corr /= sqrt(sY2/effN - pow(sY/effN,2));
    return corr;
  };

  for (int k = Tagger::Type_SMIN; k <= Tagger::Type_MAX; k++) {
    for (int l = Tagger::Type_SMIN; l <= Tagger::Type_MAX; l++) {

      Tagger::Type row = static_cast<Tagger::Type>(k);
      Tagger::Type col = static_cast<Tagger::Type>(l);
      Tagger::Pair pair = std::make_pair(row,col);

      // FIRING CORRELATIONS
      double sXY, sX, sX2, sY, sY2, effN;
      
      auto it = _tag_matrix.find(pair);
      if (it == _tag_matrix.end())
        continue;

      sXY = _tag_matrix[pair];
      sX = _tag_vector[row];
      sX2 = _tag_vector[row];
      sY = _tag_vector[col];
      sY2 = _tag_vector[col];

      /* Overall */
      effN = _numEvents;
      _fire_corr[pair] = correlation(sXY, sX, sX2, sY, sY2, effN);

      // DECISION CORRELATIONS
      
      /* Overall */
      effN = _numEvents;
      sXY = _dec_matrix[pair];
      sX = _dec_vector[row];
      sX2 = _tag_vector[row];
      sY = _dec_vector[col];
      sY2 = _tag_vector[col];
      _dec_corr[pair] = correlation(sXY, sX, sX2, sY, sY2, effN);

      /* Overall - Dilution Weighted*/
      effN = _numEvents;
      sXY = _decdil_matrix[pair];
      sX = _decdil_vector[row];
      sX2 = _decdil2_vector[row];
      sY = _decdil_vector[col];
      sY2 = _decdil2_vector[col];
      _decdil_corr[pair] = correlation(sXY, sX, sX2, sY, sY2, effN);

      /* When both fire */
      effN = _tag_matrix[pair];
      sXY = _dec_matrix[pair]; // because only non-zero contributions come when neither = 0
      sX = _dec_comatrix_left[pair];
      sX2 = effN; // because (+-1)^2 = 1
      sY = _dec_comatrix_right[pair];
      sY2 = effN; // ditto
      _dec_COcorr[pair] = correlation(sXY, sX, sX2, sY, sY2, effN);

    }
  }

  ///////////////////////////////////
  // PRINTING CORRELATION MATRICES //
  ///////////////////////////////////

  std::cout << std::endl << std::endl;
  std::cout << BOLD << "//////////////////////////////////////////////////////////////////////////////////////////\n";
  std::cout << "Tagger Fire Correlations [%]" << std::endl;
  std::cout << BOLD << "//////////////////////////////////////////////////////////////////////////////////////////\n";
  printOSPerformanceTable(_fire_corr);
  std::cout << BOLD << "//////////////////////////////////////////////////////////////////////////////////////////\n";

  std::cout << std::endl << std::endl;
  std::cout << BOLD << "//////////////////////////////////////////////////////////////////////////////////////////\n";
  std::cout << "Tagger decision Correlations [%]" << std::endl;
  std::cout << BOLD << "//////////////////////////////////////////////////////////////////////////////////////////\n";
  printOSPerformanceTable(_dec_corr);
  std::cout << BOLD << "//////////////////////////////////////////////////////////////////////////////////////////\n";

  std::cout << std::endl << std::endl;
  std::cout << BOLD << "//////////////////////////////////////////////////////////////////////////////////////////\n";
  std::cout << "Tagger decision Correlations (dilution weighted) [%]" << std::endl;
  std::cout << BOLD << "//////////////////////////////////////////////////////////////////////////////////////////\n";
  printOSPerformanceTable(_decdil_corr);
  std::cout << BOLD << "//////////////////////////////////////////////////////////////////////////////////////////\n";

  std::cout << std::endl << std::endl;
  std::cout << BOLD << "//////////////////////////////////////////////////////////////////////////////////////////\n";
  std::cout << "Tagger decision Correlations (when both fire) [%]" << std::endl;
  std::cout << BOLD << "//////////////////////////////////////////////////////////////////////////////////////////\n";
  printOSPerformanceTable(_dec_COcorr);
  std::cout << BOLD << "//////////////////////////////////////////////////////////////////////////////////////////\n";

  std::cout << ENDC << std::endl << std::endl;

  /////////////////////////////////////////
  /// GET TAGGER RESULTS FROM COLLECTORS //
  /////////////////////////////////////////

  TaggerMap<TaggingResult> _taggerResults;
  for (auto& pair : _taggerCollectors) {
    Tagger::Type type = pair.first;
    const TagCollector& c = pair.second;
    _taggerResults.emplace(type,c.GetResult());
  }
  for (auto& pair : _taggerResults) {
    const TaggingResult& res = pair.second;
    res.sortByEta();
  }

  TaggerSetMap<TaggingResult> _taggerOSCombSetResults;
  for (auto& pair : _taggerOSCombCollectors) {
    Tagger::TypeSet typeSet = pair.first;
    const TagCollector& c = pair.second;
    _taggerOSCombSetResults.emplace(typeSet,c.GetResult());
  }
  for (auto& pair : _taggerOSCombSetResults) {
    const TaggingResult& res = pair.second;
    res.sortByEta();
  }

  TaggerSetMap<TaggingResult> _taggerSSCombSetResults;
  for (auto& pair : _taggerSSCombCollectors) {
    Tagger::TypeSet typeSet = pair.first;
    const TagCollector& c = pair.second;
    _taggerSSCombSetResults.emplace(typeSet,c.GetResult());
  }
  for (auto& pair : _taggerSSCombSetResults) {
    const TaggingResult& res = pair.second;
    res.sortByEta();
  }

  TaggerSetMap<TaggingResult> _taggerOSSSCombSetResults;
  for (auto& pair : _taggerOSSSCombCollectors) {
    Tagger::TypeSet typeSet = pair.first;
    const TagCollector& c = pair.second;
    _taggerOSSSCombSetResults.emplace(typeSet,c.GetResult());
  }
  for (auto& pair : _taggerOSSSCombSetResults) {
    const TaggingResult& res = pair.second;
    res.sortByEta();
  }

  /////////////////////////////////////////
  /////////// OPEN OUTPUT FILE ////////////
  /////////////////////////////////////////
  
  TFile* sysFile = new TFile("EspressoHistograms.root","RECREATE");
  sysFile->cd();


  /////////////////////////////////////////
  // PRINT TABLE OF TAGGING PERFORMANCES //
  /////////////////////////////////////////

  std::string table, latextable, csvtable;
  std::tie(table,latextable,csvtable) = printPerformanceTable(_taggerResults);
  std::ofstream perftable, perfdatatable;
  perftable.open("EspressoPerformanceTable.tex");
  perftable << latextable;
  perftable.close();
  perfdatatable.open("EspressoPerformanceSummary.csv");
  perfdatatable << csvtable;
  perfdatatable.close();
  std::cout << BOLD
            << "--------------------------------------------------\n"
            << "-------------- TAGGING PERFORMANCES --------------\n"
            << "--------------------------------------------------\n"
            << ENDC;
  std::cout << std::endl;
  std::cout << table;
  std::cout << std::endl;


  /////////////////////////////////////////
  ///// PRINT TABLE OF ETA PERCENTILES ////
  /////////////////////////////////////////
  
  std::string pertable, perlatextable;
  std::tie(pertable,perlatextable) = printPercentilesTable(_taggerResults);
  std::ofstream perLatexTableFile;
  perLatexTableFile.open("EspressoEtaPercentilesTable.tex");
  perLatexTableFile << perlatextable;
  perLatexTableFile.close();
  std::cout << BOLD
            << "--------------------------------------------------\n"
            << "----------------- ETA PERCENTILES ----------------\n"
            << "--------------------------------------------------\n"
            << ENDC;
  std::cout << std::endl;
  std::cout << pertable;
  std::cout << std::endl;

  if (Config::PerformOfflineCombination_OS) {
    std::string combtable, comblatextable, combcsvtable;
    std::tie(combtable,comblatextable,combcsvtable) = printPerformanceTable(_taggerOSCombSetResults);
    std::ofstream combLatexTableFile,combcsvtablefile;
    combLatexTableFile.open("EspressoPerformanceTable_OSComb.tex");
    combLatexTableFile << comblatextable;
    combLatexTableFile.close();
    combcsvtablefile.open("EspressoPerformanceSummary_OSComb.csv");
    combcsvtablefile << combcsvtable;
    combcsvtablefile.close();

    std::cout << BOLD
              << "---------------------------------------------------------\n"
              << "-------------- OS COMBINATION PERFORMANCES --------------\n"
              << "---------------------------------------------------------\n"
              << ENDC;
    std::cout << std::endl;
    std::cout << combtable;
    std::cout << std::endl;
  }

  if (Config::PerformOfflineCombination_SS) {
    std::string combtable, comblatextable, combcsvtable;
    std::tie(combtable,comblatextable,combcsvtable) = printPerformanceTable(_taggerSSCombSetResults);
    std::ofstream combLatexTableFile, combcsvtablefile;
    combLatexTableFile.open("EspressoPerformanceTable_SSComb.tex");
    combLatexTableFile << comblatextable;
    combLatexTableFile.close();
    combcsvtablefile.open("EspressoPerformanceSummary_SSComb.csv");
    combcsvtablefile << combcsvtable;
    combcsvtablefile.close();
    std::cout << BOLD
              << "---------------------------------------------------------\n"
              << "-------------- SS COMBINATION PERFORMANCES --------------\n"
              << "---------------------------------------------------------\n"
              << ENDC;
    std::cout << std::endl;
    std::cout << combtable;
    std::cout << std::endl;
  }

  if (Config::PerformOfflineCombination_OSplusSS) {
    std::string combtable, comblatextable, combcsvtable;
    std::tie(combtable,comblatextable,combcsvtable) = printPerformanceTable(_taggerOSSSCombSetResults);
    std::ofstream combLatexTableFile, combcsvtablefile;
    combLatexTableFile.open("EspressoPerformanceTable_OSplusSSComb.tex");
    combLatexTableFile << comblatextable;
    combLatexTableFile.close();
    combcsvtablefile.open("EspressoPerformanceSummary_OSplusSSComb.csv");
    combcsvtablefile << combcsvtable;
    combcsvtablefile.close();
    std::cout << BOLD
              << "------------------------------------------------------------\n"
              << "-------------- OS+SS COMBINATION PERFORMANCES --------------\n"
              << "------------------------------------------------------------\n"
              << ENDC;
    std::cout << std::endl;
    std::cout << combtable;
    std::cout << std::endl;
  }
    
  /////////////////////////////////////////
  // PERFORM CALIBRATION FOR EACH TAGGER //
  /////////////////////////////////////////
  
  TaggerMap<GLMCalibration> _taggerCalibrations;
    
  bool loadID = (Config::BranchID != "") or Config::DoCalibrations;
  if (loadID) {

    std::ofstream calibrations;
    if (Config::DoCalibrations) {
      // Open calibrations file
      calibrations.open("EspressoCalibrations.py");
    }

    std::cout << std::endl;
    std::cout << BOLD
              << "-----------------------------------------------------------\n"
              << "--------------- TAGGER CALIBRATION METRICS ----------------\n"
              << "-----------------------------------------------------------\n"
              << ENDC;
    

    for (auto& pair : _taggerResults) {
    
      Tagger::Type type = pair.first;
      TaggingResult& res = pair.second;
    
      int len = 8 + Tagger::Name(type).size();
      std::cout << BOLD << AZUL;
      for (int i = 0; i < len; i++) 
        std::cout << "-";
      std::cout << "\n--- " << Tagger::Name(type) << " ---\n";
      for (int i = 0; i < len; i++) 
        std::cout << "-";
      std::cout << ENDC << std::endl;
      std::cout << std::endl;
    
      /////////////////////////////////////////
      ////////// PRINT GOODNESS OF FIT ////////
      /////////////////////////////////////////
    
      // double tag_power_actual = WeightTaggingPower(res,_numEvents,_numEventsW2);
      // double tag_power_actual_err = WeightTaggingPowerError(res,_numEvents,_numEventsW2);    

      std::cout << "-- INPUT CALIBRATION G.O.F. -- " << std::endl;
    
      std::ios::fmtflags state(std::cout.flags());
      std::streamsize prec = std::cout.precision();
      std::streamsize width = std::cout.width();
      std::cout << "DEVIANCE        = " << std::fixed << std::setprecision(2) << Deviance(res) << std::endl;
      std::cout << "Brier Score     = " << std::fixed << std::setprecision(2) << BrierScore(res) << std::endl;
      std::cout.flags(state);
      std::cout.precision(prec);
      std::cout.width(width);
      std::cout << "PEARSON X2 test = " << UngroupedPearsonTest(res) << std::endl;
      std::cout << "DEVIANCE G2 test = " << UngroupedDevianceTest(res) << std::endl;
      std::cout << "CRESSIE-READ test = " << UngroupedCressieReadTest(res) << std::endl;
      std::cout << "lCvHCH S test   = " << UngroupedSTest(res) << std::endl;
      std::cout << std::endl;
    
      if (Config::DoCalibrations) {
	
        Tagger::Type type = pair.first;
        TaggingResult& res = pair.second;
	
        /////////////////////////////////////////
        //////////// PRINT CALIBRATION //////////
        /////////////////////////////////////////
	
        std::cout << "-- OUTPUT CALIBRATION --- " << std::endl;
        auto method = static_cast<TaggingLikelihood::CovarianceCorrectionMethod>(Config::CovCorrMethod);
        GLMCalibration cal;
        bool createNewModel = true;
        if (Config::UseInputModel) {
          std::shared_ptr<Calibration> ical = Tagger::Calibration(type);
          if (auto pcal = std::dynamic_pointer_cast<GLMCalibration>(ical)) {
            Regression::LinkType L = pcal->CalibrationLink();
            std::unique_ptr<GLMModel> glm = pcal->CalibrationModel();
            cal = TaggingCalibrationCalculator(res,*glm,L,
                                               method,Config::UseNewtonRaphson,Config::DBGLEVEL>3);
            createNewModel = false;
          } else if (auto pcal = std::dynamic_pointer_cast<StandardCalibration>(ical)) {
            Matrix mat(2,2);
            mat(0,0) = 1.0;
            mat(0,1) = 0.0;
            mat(1,0) = -1.0*pcal->Eta();
            mat(1,1) = 1.0;
            GLMPolynomial model(1,mat);
            Regression::LinkType L = pcal->Link();
            cal = TaggingCalibrationCalculator(res,model,L,
                                               method,Config::UseNewtonRaphson,Config::DBGLEVEL>3);
            createNewModel = false;
          }
        }
        if (createNewModel) {
          int degree = Config::CalibrationDegree;
          const GLMModel::GLMType& model = Config::CalibrationModel;
          const Regression::LinkType& link = Config::CalibrationLink;
          cal = TaggingCalibrationCalculator(res,degree,model,link,
                                             method,Config::UseNewtonRaphson,Config::DBGLEVEL>3);
        }
        std::cout << cal << std::endl;
        _taggerCalibrations.insert(std::make_pair(type,cal));
	
        /////////////////////////////////////////
        ////////// PRINT GOODNESS OF FIT ////////
        /////////////////////////////////////////
	
        std::cout << "-- OUTPUT CALIBRATION G.O.F. -- " << std::endl;
        res.ApplyCalibration(cal);
        TaggingResult::BinTable binnedTableA = res.CreateCalibratedBinTable();
        int numParams = cal.NumParams(); // the 2 is for the delta parameters, should this be included??
        std::ios::fmtflags state(std::cout.flags());
        std::streamsize prec = std::cout.precision();
        std::streamsize width = std::cout.width();
        std::cout << "DEVIANCE        = " << std::fixed << std::setprecision(2) << Deviance(res) << std::endl;
        std::cout << "AIC             = " << std::fixed << std::setprecision(2) << AIC(res,numParams) << std::endl;
        std::cout << "BIC             = " << std::fixed << std::setprecision(2) << BIC(res,numParams) << std::endl;
        std::cout << "Brier Score     = " << std::fixed << std::setprecision(2) << BrierScore(res) << std::endl;
        std::cout.flags(state); 
        std::cout.precision(prec);
        std::cout.width(width);
        if (Config::CalibrationLink == Regression::LinkType::Mistag)
          std::cout << "PEARSON X2 test is inapplicable to the mistag (e.g. identity) link" << std::endl;
        else
          std::cout << "PEARSON X2 test = " << UngroupedPearsonTest(res,false,true) << std::endl;
        if (Config::CalibrationLink == Regression::LinkType::Logit)
          std::cout << "DEVIANCE G2 test is inapplicable to the logit link" << std::endl;
        else
          std::cout << "DEVIANCE G2 test = " << UngroupedDevianceTest(res,false,true) << std::endl;
        std::cout << "CRESSIE-READ test = " << UngroupedCressieReadTest(res,false,true) << std::endl;
        std::cout << "lCvHCH S test   = " << UngroupedSTest(res,false,true) << std::endl;
        std::cout << std::endl;

        /////////////////////////////////////////
        ////////// PRINT TO OPTIONS FILE ////////
        /////////////////////////////////////////
	
        if (Config::SaveCalibrationsToXML) {
          std::string name = Tagger::Name(type) + "_Calibration";
          std::ofstream calibration(name + ".xml");
          cal.Serialize(name,calibration);
          calibrations << Tagger::Name(type) + "_CalibrationArchive = \"" + name  + ".xml\"" << std::endl;
        } else {
          std::string name = Tagger::Name(type);
          if (Config::CalibrationModel == Espresso::GLMModel::GLMType::Polynomial and Config::CalibrationDegree == 1) {
            /* HACK TO CALCULATE <f(eta)>
               Can get the glmBasis by dividing derivative by Derivative by dLinkdEta
            */
            double eta = 0.25; // to avoid going out of range
            Vector d = cal.Derivative(eta);
            Vector basis = d/d[0];
            double avg = Regression::InvLink(eta,Config::CalibrationLink) - basis[1];
	    
            calibrations << std::left;
            calibrations << name << std::setw(10) << "_Link"
                         << " = ";
            if (Config::CalibrationLink == Espresso::Regression::LinkType::Mistag)
              calibrations << "\"MISTAG\"";
            else if (Config::CalibrationLink == Espresso::Regression::LinkType::Logit)
              calibrations << "\"LOGIT\"";
            else if (Config::CalibrationLink == Espresso::Regression::LinkType::Probit)
              calibrations << "\"PROBIT\"";
            else if (Config::CalibrationLink == Espresso::Regression::LinkType::Cauchit)
              calibrations << "\"CAUCHIT\"";
            calibrations << std::endl;
            calibrations << name << std::setw(10) << "_Eta"
                         << " = " << avg << std::endl;
            calibrations << name << std::setw(10) << "_P0"
                         << " = " << cal.GetCoeff(0) << std::endl;
            calibrations << name << std::setw(10) << "_P1"
                         << " = " << 1+cal.GetCoeff(1) << std::endl;
            calibrations << name << std::setw(10) << "_P0Err"
                         << " = " << cal.GetError(0) << std::endl;
            calibrations << name << std::setw(10) << "_P1Err"
                         << " = " << cal.GetError(1) << std::endl;
            calibrations << name << std::setw(10) << "_RhoP0P1"
                         << " = " << cal.GetCorrelation(0,1) << std::endl;
            calibrations << std::endl;
          } else {
            calibrations << "# No python export possible for the " << name << " calibration model" << std::endl;
          }
        }
      }
    }

    if (Config::DoCalibrations) {
      std::string caltable, latexcaltable, csvtable;
      std::tie(caltable,latexcaltable, csvtable) = printPerformanceTable(_taggerResults);
      std::ofstream perfcaltable, csvtablefile;
      perfcaltable.open("EspressoCalibratedPerformanceTable.tex");
      perfcaltable << latexcaltable;
      perfcaltable.close();
      csvtablefile.open("EspressoCalibratedPerformanceSummary.csv");
      csvtablefile << csvtable;
      csvtablefile.close();
      
      std::cout << std::endl;
      std::cout << BOLD
                << "--------------------------------------------------\n"
                << "-------- CALIBRATED TAGGING PERFORMANCES ---------\n"
                << "--------------------------------------------------\n"
                << ENDC;
      std::cout << std::endl;
      std::cout << caltable;
      std::cout << std::endl;
      
    }
    
  }
  
  
  std::cout << std::endl;
  std::cout << BOLD
            << "--------------------------------------------------------\n"
            << "-------------- COMPUTING FINAL HISTOGRAMS --------------\n"
            << "--------------------------------------------------------\n"
            << ENDC;

  for (auto& pair : _taggerResults) {
    
    Tagger::Type type = pair.first;
    std::string name = Tagger::Name(type);
    const TaggingResult& res = pair.second;
    
    int len = 8 + Tagger::Name(type).size();
    std::cout << BOLD << AZUL;
    for (int i = 0; i < len; i++) 
      std::cout << "-";
    std::cout << "\n--- " << Tagger::Name(type) << " ---\n";
    for (int i = 0; i < len; i++) 
      std::cout << "-";
    std::cout << ENDC << std::endl;

    /////////////////////////////////////////
    /////////// PRINT BINNED TABLES /////////
    /////////////////////////////////////////

    // PRINT BIN TABLE
    if (loadID) {
      int numGroups = Tagger::Binning(type);
      TaggingResult::BinTable binnedTable = res.CreateBinTable(numGroups);
      std::string namep = name + "_BinTable";
      std::string titlep = Config::PlotTitle ? Tagger::RootName(type) + " #omega vs #eta" : "";
      auto graph = graphBinTable(binnedTable,namep,titlep);
      printGraph(graph,true,Config::PlotLabel,Config::PlotExtension);

      // ALSO PRINT TABLE TO FILE AND SCREEN
      std::stringstream output;
      output << binnedTable;
      std::ofstream csvtable;
      csvtable.open(Tagger::Name(type) + "_BinTable.csv");
      csvtable << output.str();
      csvtable.close();
      // std::cout << std::endl;
      // std::cout << output.str();
      // std::cout << std::endl;
    
      // PLOT KERNEL SMOOTHED BIN TABLE
      TaggingResult::SmoothingOptions opts;
      opts.num_points = Config::NumKernelSmoothingPoints;
      opts.sigma = Config::KernelSmoothingWidth;
      TaggingResult::BinTable smoothBinnedTable = res.CreateKernelSmoothedBinTable(opts);
      std::string sname = name + std::string("_SmoothedBinTable");
      std::string stitle = Config::PlotTitle ? Tagger::RootName(type) + std::string(" smoothed #omega vs #eta") : "";
      printSmoothedBinTable(smoothBinnedTable,sname,stitle,true,Config::PlotLabel,Config::PlotExtension);

      if (Config::DrawOscillationPlots) {

        // PRINT TAU TABLE
        TaggingResult::WrappingOptions wopts;
        double freq = ModeFrequency.at(CalibrationMode::NeutralBs);
        wopts.period = (Config::CalibrationMode == CalibrationMode::NeutralBs)
          ? 2*PI/freq : 0.0;
        double scale = 0.0;
        if (Config::TauUnits == "ps")
          scale = 1.0;
        else if (Config::TauUnits == "ns")
          scale = 1000.0;
        else if (Config::TauUnits == "fs")
          scale = 0.001;
        else
          scale = 1.0;
        wopts.maximum= (Config::CalibrationMode == CalibrationMode::NeutralBd)
          ? Config::OscillationPlotsMaximum*scale : -1.0;
        
        wopts.frequency = (Config::DeltaM>0) ? Config::DeltaM : -1;
        wopts.lifetime = (Config::Lifetime>0) ? Config::Lifetime : -1;
        wopts.deltagamma = (Config::DeltaGamma>0) ? Config::DeltaGamma : -1;
        if(Config::CalibrationMode == CalibrationMode::ChargedBu)
          wopts.decayMode = CalibrationMode::ChargedBu;
        if(Config::CalibrationMode == CalibrationMode::NeutralBd)
          wopts.decayMode = CalibrationMode::NeutralBd;
        if(Config::CalibrationMode == CalibrationMode::NeutralBs)
          wopts.decayMode = CalibrationMode::NeutralBs;

        TaggingResult::TauTable tauTable = res.CreateTauTable(numGroups,wopts);
        std::string taunamep = name + "_TauTable";
        std::string tautitlep = Config::PlotTitle ? Tagger::RootName(type) + " A vs #tau" : "";
        auto taugraph = graphTauTable(tauTable,taunamep,tautitlep,1);
        printGraph(taugraph,true,Config::PlotLabel,Config::PlotExtension);
        
        // PLOT KERNEL SMOOTHED TAU TABLE
        TaggingResult::TauTable smoothedTauTable = res.CreateKernelSmoothedTauTable(opts,wopts);
        std::string tausname = name + std::string("_SmoothedTauTable");
        std::string taustitle = Config::PlotTitle ? Tagger::RootName(type) + std::string(" smoothed A vs #tau") : "";
        printSmoothedTauTable(smoothedTauTable,tausname,taustitle,true,Config::PlotLabel,Config::PlotExtension);
      }
      
      // badly sorted after this
        
      /////////////////////////////////////////
      ///////// PRINT INPUT CALIBRATIONS //////
      /////////////////////////////////////////
      
      res.sortByEta();
      const TaggingResult::TagTable& table = res.GetTagTable();
      double min, max;
      double upper_shift = 0.0;

      if(Config::CalibrationLink==Espresso::Regression::LinkType::RLogit 
         || Config::CalibrationLink==Espresso::Regression::LinkType::RProbit 
         || Config::CalibrationLink==Espresso::Regression::LinkType::RCauchit)
        upper_shift = 0.005;

      if (table.size() == 0) {
        min = 0.0;
        max = 0.5;
      } else {
        min = table[0].eta;
        max = table[table.size()-1].eta - upper_shift;
      }
      
      std::shared_ptr<Calibration> pcal = Tagger::Calibration(type);
      
      // PLOT CALIBRATION AGAINST BIN TABLE
      std::string icnamep = Tagger::Name(type) + "_InputCalibration";
      std::string ictitlep = Config::PlotTitle ? Tagger::RootName(type) + " input calibration" : "";
      overlayDataAndCalibration(*pcal,binnedTable,icnamep,ictitlep,min,max,true,Config::PlotLabel,Config::PlotExtension);
      
      // PLOT CALIBRATION AGAINST SMOOTHED BIN TABLE
      std::string icnames = Tagger::Name(type) + "_InputCalibration_SmoothedData";
      std::string ictitles = Config::PlotTitle ? Tagger::RootName(type) + " input calibration (smoothed data)" : "";
      overlaySmoothedDataAndCalibration(*pcal,smoothBinnedTable,icnames,ictitles,min,max,true,Config::PlotLabel,Config::PlotExtension);
      
      /////////////////////////////////////////
      /////// PRINT OUTPUT CALIBRATIONS ///////
      /////////////////////////////////////////
      
      if (Config::DoCalibrations) {
	
        const auto it = _taggerCalibrations.find(type);
        if (it != _taggerCalibrations.end()) {
          const GLMCalibration& cal = it->second;
	  
          // PLOT CALIBRATION AGAINST BIN TABLE
          std::string cnamep = Tagger::Name(type) + "_Calibration";
          std::string ctitlep = Config::PlotTitle ? Tagger::RootName(type) + " calibration" : "";
          overlayDataAndCalibration(cal,binnedTable,cnamep,ctitlep,min,max,true,Config::PlotLabel,Config::PlotExtension);
	  
          // PLOT CALIBRATION AGAINST SMOOTHED BIN TABLE
          std::string cnames = Tagger::Name(type) + "_Calibration_SmoothedData";
          std::string ctitles = Config::PlotTitle ? Tagger::RootName(type) + " calibration (smoothed data)" : "";
          overlaySmoothedDataAndCalibration(cal,smoothBinnedTable,cnames,ctitles,min,max,true,Config::PlotLabel,Config::PlotExtension);
        }
      }

    }
    
    /////////////////////////////////////////
    ///////// PRINT OTHER GRAPHICS //////////
    /////////////////////////////////////////

    // ETA DISTRIBUTIONS
    std::string etatitle = Config::PlotTitle ? Tagger::RootName(type)+std::string(" #eta distribution") : "";
    auto eta = graphEtaDist(res,Tagger::Name(type)+std::string("_EtaDist"),etatitle);
    printHistogram(eta,true,Config::PlotLabel,Config::PlotExtension);

    // INTEGRATED TAGGING POWER
    std::string iptitle = Config::PlotTitle ? Tagger::RootName(type) +std::string(" integrated tagging power") : "";
    auto ip = graphIntegratedTaggingPower(res,Tagger::Name(type)+std::string("_IntPower"),iptitle);
    printCurve(ip,true,Config::PlotLabel,Config::PlotExtension);

    // ROC CURVE
    if (loadID) {
      std::string etaRtitle = Config::PlotTitle ? Tagger::RootName(type)+std::string(" #eta distribution (rightly tagged)") : "";
      auto etaR = graphEtaDistRight(res,Tagger::Name(type)+std::string("_EtaDistRight"),etaRtitle);
      printHistogram(etaR,true,Config::PlotLabel,Config::PlotExtension);
      
      std::string etaWtitle = Config::PlotTitle ? Tagger::RootName(type)+std::string(" #eta distribution (wrongly tagged)") : "";
      auto etaW = graphEtaDistWrong(res,Tagger::Name(type)+std::string("_EtaDistWrong"),etaWtitle);
      printHistogram(etaW,true,Config::PlotLabel,Config::PlotExtension);

      std::string roctitle = Config::PlotTitle ? Tagger::RootName(type) +std::string(" ROC curve") : "";
      auto roc = graphROC(res,Tagger::Name(type)+std::string("_ROC"),roctitle);
      printCurve(roc,true,Config::PlotLabel,Config::PlotExtension);
    }
  }
  
  sysFile->Write();
  sysFile->Close();
}
  

void EspressoPerformanceMonitorImpl::add_tag(Tagger::Type type, Flavour decayFlavour, Flavour predFlavour, double eta, double weight, double Btau, double BtauErr) { 
  auto it = _taggerCollectors.find(type);
  if (it == _taggerCollectors.end()) {
    _taggerCollectors.emplace(std::make_pair(type,TagCollector(model,Tagger::Calibration(type))));
    _taggerCollectors.at(type).SetVerbose(verbose);
    it = _taggerCollectors.find(type);
  }
  it->second.AddTag(decayFlavour,predFlavour,eta,weight,Btau,BtauErr);
}

void EspressoPerformanceMonitorImpl::add_tag_os(Tagger::TypeSet typeSet, Flavour decayFlavour, Flavour predFlavour,
                                                double omega, double weight, double Btau, double BtauErr) { 
  auto it = _taggerOSCombCollectors.find(typeSet);
  if (it == _taggerOSCombCollectors.end()) {
    _taggerOSCombCollectors.emplace(std::make_pair(typeSet,TagCollector(model,Tagger::Calibration(Tagger::OS_Combination))));
    _taggerOSCombCollectors.at(typeSet).SetVerbose(verbose);
    it = _taggerOSCombCollectors.find(typeSet);
  }
  it->second.AddTag(decayFlavour,predFlavour,omega,weight,Btau,BtauErr);
}

void EspressoPerformanceMonitorImpl::add_tag_ss(Tagger::TypeSet typeSet, Flavour decayFlavour, Flavour predFlavour,
                                                double omega, double weight, double Btau, double BtauErr) { 
  auto it = _taggerSSCombCollectors.find(typeSet);
  if (it == _taggerSSCombCollectors.end()) {
    _taggerSSCombCollectors.emplace(std::make_pair(typeSet,TagCollector(model,Tagger::Calibration(Tagger::SS_Combination))));
    _taggerSSCombCollectors.at(typeSet).SetVerbose(verbose);
    it = _taggerSSCombCollectors.find(typeSet);
  }
  it->second.AddTag(decayFlavour,predFlavour,omega,weight,Btau,BtauErr);
}

void EspressoPerformanceMonitorImpl::add_tag_osss(Tagger::TypeSet typeSet, Flavour decayFlavour, Flavour predFlavour,
                                                  double omega, double weight, double Btau, double BtauErr) { 
  auto it = _taggerOSSSCombCollectors.find(typeSet);
  if (it == _taggerOSSSCombCollectors.end()) {
    _taggerOSSSCombCollectors.emplace(std::make_pair(typeSet,TagCollector(model,Tagger::Calibration(Tagger::Combination))));
    _taggerOSSSCombCollectors.at(typeSet).SetVerbose(verbose);
    it = _taggerOSSSCombCollectors.find(typeSet);
  }
  it->second.AddTag(decayFlavour,predFlavour,omega,weight,Btau,BtauErr);
}

