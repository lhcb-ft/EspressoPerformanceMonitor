#include "Tagger.hh"

#include "GaudiIOTools.hh"

#include "GLMCalibration.hh"

std::map<Tagger::Type,TString> Tagger::m_archiveMap = {
  {Tagger::None, ""},
  {Tagger::Placeholder, ""},

  {Tagger::Toy, ""},
  {Tagger::OS_Muon, ""},
  {Tagger::OS_Electron, ""},
  {Tagger::OS_Kaon, ""},
  {Tagger::OS_nnetKaon, ""},
  {Tagger::VtxCharge, ""},
  {Tagger::OS_Charm, ""},
  {Tagger::SS_Kaon, ""},
  {Tagger::SS_nnetKaon, ""},
  {Tagger::SS_Pion, ""},
  {Tagger::SS_PionBDT, ""},
  {Tagger::SS_Proton, ""},
  {Tagger::Inclusive, ""},

  {Tagger::OS_Combination, ""},
  {Tagger::SS_Combination, ""},
  {Tagger::Combination, ""}
};

std::map<Tagger::Type,std::shared_ptr<Espresso::Calibration> > Tagger::m_gcalMap = {
  {Tagger::None, nullptr},
  {Tagger::Placeholder, nullptr},

  {Tagger::Toy, nullptr},
  {Tagger::OS_Muon, nullptr},
  {Tagger::OS_Electron, nullptr},
  {Tagger::OS_Kaon, nullptr},
  {Tagger::OS_nnetKaon, nullptr},
  {Tagger::VtxCharge, nullptr},
  {Tagger::OS_Charm, nullptr},
  {Tagger::SS_Kaon, nullptr},
  {Tagger::SS_nnetKaon, nullptr},
  {Tagger::SS_Pion, nullptr},
  {Tagger::SS_PionBDT, nullptr},
  {Tagger::SS_Proton, nullptr},
  {Tagger::Inclusive, nullptr},

  {Tagger::OS_Combination, nullptr},
  {Tagger::SS_Combination, nullptr},
  {Tagger::Combination, nullptr}
};

std::map<Tagger::Type,Espresso::StandardCalibration> Tagger::m_calMap = {
  {Tagger::None, Espresso::StandardCalibration()},
  {Tagger::Placeholder, Espresso::StandardCalibration()},

  {Tagger::Toy, Espresso::StandardCalibration()},
  {Tagger::OS_Muon, Espresso::StandardCalibration()},
  {Tagger::OS_Electron, Espresso::StandardCalibration()},
  {Tagger::OS_Kaon, Espresso::StandardCalibration()},
  {Tagger::OS_nnetKaon, Espresso::StandardCalibration()},
  {Tagger::VtxCharge, Espresso::StandardCalibration()},
  {Tagger::OS_Charm, Espresso::StandardCalibration()},  
  {Tagger::SS_Kaon, Espresso::StandardCalibration()},
  {Tagger::SS_nnetKaon, Espresso::StandardCalibration()},
  {Tagger::SS_Pion, Espresso::StandardCalibration()},
  {Tagger::SS_PionBDT, Espresso::StandardCalibration()},
  {Tagger::SS_Proton, Espresso::StandardCalibration()},
  {Tagger::Inclusive, Espresso::StandardCalibration()},

  {Tagger::OS_Combination, Espresso::StandardCalibration()},  
  {Tagger::SS_Combination, Espresso::StandardCalibration()},  
  {Tagger::Combination, Espresso::StandardCalibration()}  
};

std::shared_ptr<Espresso::Calibration> Tagger::Calibration(Tagger::Type m_Type) {

  auto it = m_gcalMap.find(m_Type);
  if (it != m_gcalMap.end()) {
    if (m_gcalMap[m_Type] != nullptr)
      return m_gcalMap[m_Type];
  }

  if (Tagger::CalibrationArchive(m_Type) != "") {
    try {
      std::ifstream ifs(Tagger::CalibrationArchive(m_Type));
      std::string name = Tagger::Name(m_Type) + "_Calibration";
      Espresso::GLMCalibration cal = Espresso::GLMCalibration::Deserialize(name,ifs);
      auto pcal = std::make_shared<Espresso::GLMCalibration>(cal);
      m_gcalMap[m_Type] = pcal;
      return pcal;
    } catch (Espresso::GLMCalibration::DeserializationException ex) {
      return m_calMap[m_Type].clone();
    }
  } else {
    return m_calMap[m_Type].clone();
  }
}

std::map<Tagger::Type,std::string> Tagger::m_shortNameMap = {
  {Tagger::None, ""},
  {Tagger::Placeholder, ""},

  {Tagger::Toy, "T"},
  {Tagger::OS_Muon, "M"},
  {Tagger::OS_Electron, "E"},
  {Tagger::OS_Kaon, "K"},
  {Tagger::OS_nnetKaon, "Kn"},
  {Tagger::VtxCharge, "V"},
  {Tagger::OS_Charm, "C"},
  {Tagger::SS_Kaon, "SK"},
  {Tagger::SS_nnetKaon, "SKn"},
  {Tagger::SS_Pion, "SPi"},
  {Tagger::SS_PionBDT, "SPib"},
  {Tagger::SS_Proton, "SPr"},
  {Tagger::Inclusive, "Incl"},

  {Tagger::OS_Combination, "OS"},
  {Tagger::SS_Combination, "SS"},
  {Tagger::Combination, "All"}
};

std::map<Tagger::Type,std::string> Tagger::m_nameMap = {
  {Tagger::None, "None"},
  {Tagger::Placeholder, "Placeholder"},

  {Tagger::Toy, "Toy"},
  {Tagger::OS_Muon, "OS_Muon"},
  {Tagger::OS_Electron, "OS_Electron"},
  {Tagger::OS_Kaon, "OS_Kaon"},
  {Tagger::OS_nnetKaon, "OS_nnetKaon"},
  {Tagger::VtxCharge, "VtxCharge"},
  {Tagger::OS_Charm, "OS_Charm"},
  {Tagger::SS_Kaon, "SS_Kaon"},
  {Tagger::SS_nnetKaon, "SS_nnetKaon"},
  {Tagger::SS_Pion, "SS_Pion"},
  {Tagger::SS_PionBDT, "SS_PionBDT"},
  {Tagger::SS_Proton, "SS_Proton"},
  {Tagger::Inclusive, "Inclusive"},

  {Tagger::OS_Combination, "OS_Combination"},
  {Tagger::SS_Combination, "SS_Combination"},
  {Tagger::Combination, "Combination"}
};

std::map<Tagger::Type,std::string> Tagger::m_rootNameMap = {
  {Tagger::None, ""},
  {Tagger::Placeholder, ""},

  {Tagger::Toy, "Toy"},
  {Tagger::OS_Muon, "OS #mu"},
  {Tagger::OS_Electron, "OS e"},
  {Tagger::OS_Kaon, "OS K"},
  {Tagger::OS_nnetKaon, "OS K NN"},
  {Tagger::VtxCharge, "Vertex Charge"},
  {Tagger::OS_Charm, "OS c"},
  {Tagger::SS_Kaon, "SS K"},
  {Tagger::SS_nnetKaon, "SS K NN"},
  {Tagger::SS_Pion, "SS #pi"},
  {Tagger::SS_PionBDT, "SS #pi BDT"},
  {Tagger::SS_Proton, "SS p"},
  {Tagger::Inclusive, "Inclusive"},

  {Tagger::OS_Combination, "OS Combination"},
  {Tagger::SS_Combination, "SS Combination"},
  {Tagger::Combination, "Combination"}
};

std::map<Tagger::Type,std::string> Tagger::m_latexNameMap = {
  {Tagger::None, ""},
  {Tagger::Placeholder, ""},

  {Tagger::Toy, "Toy"},
  {Tagger::OS_Muon, "OS $\\mu$"},
  {Tagger::OS_Electron, "OS $e$"},
  {Tagger::OS_Kaon, "OS $K$"},
  {Tagger::OS_nnetKaon, "OS $K$ NN"},
  {Tagger::VtxCharge, "Vertex Charge"},
  {Tagger::OS_Charm, "OS $c$"},
  {Tagger::SS_Kaon, "SS $K$"},
  {Tagger::SS_nnetKaon, "SS $K$ NN"},
  {Tagger::SS_Pion, "SS $\\pi$"},
  {Tagger::SS_PionBDT, "SS $\\pi$ BDT"},
  {Tagger::SS_Proton, "SS $p$"},
  {Tagger::Inclusive, "Inclusive"},

  {Tagger::OS_Combination, "OS Combination"},
  {Tagger::SS_Combination, "SS Combination"},
  {Tagger::Combination, "Combination"}
};

std::map<Tagger::Type,bool> Tagger::m_isActive = {
  {Tagger::None, false},
  {Tagger::Placeholder, false},

  {Tagger::Toy, false},
  {Tagger::OS_Muon, false},
  {Tagger::OS_Electron, false},
  {Tagger::OS_Kaon, false},
  {Tagger::OS_nnetKaon, false},
  {Tagger::VtxCharge, false},
  {Tagger::OS_Charm, false},
  {Tagger::SS_Kaon, false},
  {Tagger::SS_nnetKaon, false},
  {Tagger::SS_Pion, false},
  {Tagger::SS_PionBDT, false},
  {Tagger::SS_Proton, false},
  {Tagger::Inclusive, false},

  {Tagger::OS_Combination, false},
  {Tagger::SS_Combination, false},
  {Tagger::Combination, false}
};

TString BranchPrefix = "Bplus_BuTaggingTool_";

std::map<Tagger::Type,TString> Tagger::m_BranchDec = {
  {Tagger::Toy, BranchPrefix + "Toy_DEC"},

  {Tagger::OS_Muon, BranchPrefix + "OS_Muon_DEC"},
  {Tagger::OS_Electron, BranchPrefix + "OS_Electron_DEC"},
  {Tagger::OS_Kaon, BranchPrefix + "OS_Kaon_DEC"},
  {Tagger::OS_nnetKaon, BranchPrefix + "OS_nnetKaon_DEC"},
  {Tagger::VtxCharge, BranchPrefix + "VtxCharge_DEC"},
  {Tagger::OS_Charm, BranchPrefix + "OS_Charm_DEC"},

  {Tagger::SS_Kaon, BranchPrefix + "SS_Kaon_DEC"},
  {Tagger::SS_nnetKaon, BranchPrefix + "SS_nnetKaon_DEC"},
  {Tagger::SS_Pion, BranchPrefix + "SS_Pion_DEC"},
  {Tagger::SS_PionBDT, BranchPrefix + "SS_PionBDT_DEC"},
  {Tagger::SS_Proton, BranchPrefix + "SS_Proton_DEC"},

  {Tagger::Inclusive, BranchPrefix + "Inclusive_DEC"},

  {Tagger::OS_Combination, BranchPrefix + "OS_Combination_DEC"},
  {Tagger::SS_Combination, BranchPrefix + "SS_Combination_DEC"},
  {Tagger::Combination, BranchPrefix + "Combination_DEC"},
};

std::map<Tagger::Type,TString> Tagger::m_BranchProb = {
  {Tagger::Toy, BranchPrefix + "Toy_PROB"},

  {Tagger::OS_Muon, BranchPrefix + "OS_Muon_PROB"},
  {Tagger::OS_Electron, BranchPrefix + "OS_Electron_PROB"},
  {Tagger::OS_Kaon, BranchPrefix + "OS_Kaon_PROB"},
  {Tagger::OS_nnetKaon, BranchPrefix + "OS_nnetKaon_PROB"},
  {Tagger::VtxCharge, BranchPrefix + "VtxCharge_PROB"},
  {Tagger::OS_Charm, BranchPrefix + "OS_Charm_PROB"},

  {Tagger::SS_Kaon, BranchPrefix + "SS_Kaon_PROB"},
  {Tagger::SS_nnetKaon, BranchPrefix + "SS_nnetKaon_PROB"},
  {Tagger::SS_Pion, BranchPrefix + "SS_Pion_PROB"},
  {Tagger::SS_PionBDT, BranchPrefix + "SS_PionBDT_PROB"},
  {Tagger::SS_Proton, BranchPrefix + "SS_Proton_PROB"},

  {Tagger::Inclusive, BranchPrefix + "Inclusive_PROB"},

  {Tagger::OS_Combination, BranchPrefix + "OS_Combination_PROB"},
  {Tagger::SS_Combination, BranchPrefix + "SS_Combination_PROB"},
  {Tagger::Combination, BranchPrefix + "Combination_PROB"},
};

std::map<Tagger::Type,TString> Tagger::m_TypeDec = {
  {Tagger::Toy, "Int_t"},

  {Tagger::OS_Muon, "Int_t"},
  {Tagger::OS_Electron, "Int_t"},
  {Tagger::OS_Kaon, "Int_t"},
  {Tagger::OS_nnetKaon, "Int_t"},
  {Tagger::VtxCharge, "Int_t"},
  {Tagger::OS_Charm, "Int_t"},

  {Tagger::SS_Kaon, "Int_t"},
  {Tagger::SS_nnetKaon, "Int_t"},
  {Tagger::SS_Pion, "Int_t"},
  {Tagger::SS_PionBDT, "Int_t"},
  {Tagger::SS_Proton, "Int_t"},

  {Tagger::Inclusive, "Int_t"},

  {Tagger::OS_Combination, "Int_t"},
  {Tagger::SS_Combination, "Int_t"},
  {Tagger::Combination, "Int_t"}
};

std::map<Tagger::Type,TString> Tagger::m_TypeProb = {
  {Tagger::Toy, "Double_t"},

  {Tagger::OS_Muon, "Double_t"},
  {Tagger::OS_Electron, "Double_t"},
  {Tagger::OS_Kaon, "Double_t"},
  {Tagger::OS_nnetKaon, "Double_t"},
  {Tagger::VtxCharge, "Double_t"},
  {Tagger::OS_Charm, "Double_t"},

  {Tagger::SS_Kaon, "Double_t"},
  {Tagger::SS_nnetKaon, "Double_t"},
  {Tagger::SS_Pion, "Double_t"},
  {Tagger::SS_PionBDT, "Double_t"},
  {Tagger::SS_Proton, "Double_t"},

  {Tagger::Inclusive, "Double_t"},

  {Tagger::OS_Combination, "Double_t"},
  {Tagger::SS_Combination, "Double_t"},
  {Tagger::Combination, "Double_t"}
};

std::map<int,Tagger::Type> Tagger::m_numBTA = {
  {2, Tagger::OS_Muon},
  {3, Tagger::OS_Electron},
  {6, Tagger::SS_Pion},
  {10, Tagger::VtxCharge},
  {12,Tagger::OS_Kaon},
  {13,Tagger::SS_Kaon},
  {15,Tagger::OS_Charm},
  {16, Tagger::Inclusive},
};

std::map<Tagger::Type,bool> Tagger::m_inComb = {
  {Tagger::None, false},
  {Tagger::Placeholder, false},

  {Tagger::Toy, false},
  {Tagger::OS_Muon, false},
  {Tagger::OS_Electron, false},
  {Tagger::OS_Kaon, false},
  {Tagger::OS_nnetKaon, false},
  {Tagger::VtxCharge, false},
  {Tagger::OS_Charm, false},
  {Tagger::SS_Kaon, false},
  {Tagger::SS_nnetKaon, false},
  {Tagger::SS_Pion, false},
  {Tagger::SS_PionBDT, false},
  {Tagger::SS_Proton, false},
  {Tagger::Inclusive, false},

  {Tagger::OS_Combination, false},
  {Tagger::SS_Combination, false},
  {Tagger::Combination, false}
};

std::map<Tagger::Type,bool> Tagger::m_write = {
  {Tagger::None, false},
  {Tagger::Placeholder, false},

  {Tagger::Toy, false},
  {Tagger::OS_Muon, false},
  {Tagger::OS_Electron, false},
  {Tagger::OS_Kaon, false},
  {Tagger::OS_nnetKaon, false},
  {Tagger::VtxCharge, false},
  {Tagger::OS_Charm, false},
  {Tagger::SS_Kaon, false},
  {Tagger::SS_nnetKaon, false},
  {Tagger::SS_Pion, false},
  {Tagger::SS_PionBDT, false},
  {Tagger::SS_Proton, false},
  {Tagger::Inclusive, false},

  {Tagger::OS_Combination, false},
  {Tagger::SS_Combination, false},
  {Tagger::Combination, false}
};

std::map<Tagger::Type,bool> Tagger::m_inOSComb = {
  {Tagger::None, false},
  {Tagger::Placeholder, false},

  {Tagger::Toy, false},
  {Tagger::OS_Muon, false},
  {Tagger::OS_Electron, false},
  {Tagger::OS_Kaon, false},
  {Tagger::OS_nnetKaon, false},
  {Tagger::VtxCharge, false},
  {Tagger::OS_Charm, false},
  {Tagger::SS_Kaon, false},
  {Tagger::SS_nnetKaon, false},
  {Tagger::SS_Pion, false},
  {Tagger::SS_PionBDT, false},
  {Tagger::SS_Proton, false},
  {Tagger::Inclusive, false},

  {Tagger::OS_Combination, false},
  {Tagger::SS_Combination, false},
  {Tagger::Combination, false}
};

std::map<Tagger::Type,bool> Tagger::m_inSSComb = {
  {Tagger::None, false},
  {Tagger::Placeholder, false},

  {Tagger::Toy, false},
  {Tagger::OS_Muon, false},
  {Tagger::OS_Electron, false},
  {Tagger::OS_Kaon, false},
  {Tagger::OS_nnetKaon, false},
  {Tagger::VtxCharge, false},
  {Tagger::OS_Charm, false},
  {Tagger::SS_Kaon, false},
  {Tagger::SS_nnetKaon, false},
  {Tagger::SS_Pion, false},
  {Tagger::SS_PionBDT, false},
  {Tagger::SS_Proton, false},
  {Tagger::Inclusive, false},

  {Tagger::OS_Combination, false},
  {Tagger::SS_Combination, false},
  {Tagger::Combination, false}
};

std::map<Tagger::Type,double> Tagger::m_combPowMap = {
  {Tagger::None, 0.0},
  {Tagger::Placeholder, 0.0},

  {Tagger::Toy, 0.0},
  {Tagger::OS_Muon, 0.0},
  {Tagger::OS_Electron, 0.0},
  {Tagger::OS_Kaon, 0.0},
  {Tagger::OS_nnetKaon, 0.0},
  {Tagger::VtxCharge, 0.0},
  {Tagger::OS_Charm, 0.0},
  {Tagger::SS_Kaon, 0.0},
  {Tagger::SS_nnetKaon, 0.0},
  {Tagger::SS_Pion, 0.0},
  {Tagger::SS_PionBDT, 0.0},
  {Tagger::SS_Proton, 0.0},
  {Tagger::Inclusive, 0.0},

  {Tagger::OS_Combination, 0.0},
  {Tagger::SS_Combination, 0.0},
  {Tagger::Combination, 0.0}
};

std::map<Tagger::Type,int> Tagger::m_binningMap = {
  {Tagger::None, 10},
  {Tagger::Placeholder, 10},
  
  {Tagger::Toy, 10},
  {Tagger::OS_Muon, 10},
  {Tagger::OS_Electron, 10},
  {Tagger::OS_Kaon, 10},
  {Tagger::OS_nnetKaon, 10},
  {Tagger::VtxCharge, 10},
  {Tagger::OS_Charm, 10},
  {Tagger::SS_Kaon, 10},
  {Tagger::SS_nnetKaon, 10},
  {Tagger::SS_Pion, 10},
  {Tagger::SS_PionBDT, 10},
  {Tagger::SS_Proton, 10},
  {Tagger::Inclusive, 10},
  
  {Tagger::OS_Combination, 10},
  {Tagger::SS_Combination, 10},
  {Tagger::Combination, 10}
};

// READ CALIBRATION PARAMETERS FROM FILE

// CONSTRUCTORS

Tagger::Tagger()
  : m_Type(0),
    m_Omega(0.5),
    m_Decision(0)
{
} ///< Blank constructor

Tagger::Tagger(int type)
  : m_Type(type),
    m_Omega(0.5),
    m_Decision(0)
{  
} ///< Constructor with name

Tagger::Tagger(int type, int decision, double omega ) 
  : m_Type(type),
    m_Omega(omega),
    m_Decision(decision)
{ 
  if (omega==0)
  {
    m_Omega = 0.5;
    m_Decision = 0;
  }
} ///< Constructor specifying type

void Tagger::reset() {
  m_Type = 0;
  m_Omega = 0.5;
  m_Decision = 0;
} ///< resets tagger members

double Tagger::eta() {
  return m_Omega;
}

double Tagger::omega() {
  auto type = static_cast<Tagger::Type>(m_Type);
  std::shared_ptr<Espresso::Calibration> p = Tagger::Calibration(type);
  double om = (*p)(m_Omega);
  return om;
}

void Tagger::Print() {
  if (Config::DBGLEVEL<3 && this->type() ) {
    std::cout << "--- Tagger type=" << this->type()
	      << " decision="<< this->decision() 
	      << " omega="<< this->omega()
	      << std::endl;
  }
} ///< dump tagger information


std::string Tagger::Name(const Tagger::TypeSet& typeSet) {
  std::string name = "";
  bool first = true;
  for (const auto& val : typeSet) {
    if (first) 
      first = false;
    else
      name += "+";
    name += Tagger::ShortName(val);
  }
  return name;
}
 
std::string Tagger::LatexName(const Tagger::TypeSet& typeSet) {
  std::string name = "";
  bool first = true;
  for (const auto& val : typeSet) {
    if (first) 
      first = false;
    else
      name += "+";
    name += Tagger::ShortName(val);
  }
  return name;
}

