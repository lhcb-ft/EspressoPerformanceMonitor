#include "EspressoPerformanceMonitor.hh"
#include "EspressoPerformanceMonitorImpl.hh"

EspressoPerformanceMonitor::EspressoPerformanceMonitor()
  : pImpl(new EspressoPerformanceMonitorImpl())
{
}

int EspressoPerformanceMonitor::N() {
  return pImpl->N();
}

void EspressoPerformanceMonitor::increment(const Taggers& taggers, Flavour decayFlavour, double Btau, double BtauErr, double weight) {
  pImpl->increment(taggers,decayFlavour,Btau,BtauErr,weight);
}

const TaggingResult& EspressoPerformanceMonitor::GetTaggingResult(Tagger::Type type) const {
  return pImpl->GetTaggingResult(type);
}

void EspressoPerformanceMonitor::printStats() {
  pImpl->printStats();
}

void EspressoPerformanceMonitor::SetVerbose(bool _verbose) {
  pImpl->SetVerbose(_verbose);
}
