/**
 * @addtogroup gr_fttools
 * @{
 */

/**
 * @file
 * @brief This header file defines types and methods used in reading options from a python-like configuration file.
 */

#ifndef GaudiIOTools_H
#define GaudiIOTools_H 1

// C/C++ includes
#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include <map>

/////////////////
///// TYPES /////
/////////////////

template<typename Ch, typename Traits = std::char_traits<Ch> >
struct basic_nullbuf : std::basic_streambuf<Ch, Traits> {
     typedef std::basic_streambuf<Ch, Traits> base_type;
     typedef typename base_type::int_type int_type;
     typedef typename base_type::traits_type traits_type;

     virtual int_type overflow(int_type c) {
         return traits_type::not_eof(c);
     }
};

using nullbuf = basic_nullbuf<char>;
using wnullbuf = basic_nullbuf<wchar_t>;

/// Map from key to a numeric value
using propertyMap = std::map<const std::string, const double >;

/// Map from key to a string value
using propertyStringMap = std::map<const std::string, const std::string >;

/**
 * @class parseException
 * @brief An exception class for when a line can't be parsed as a file import or key = value.
 */
class parseException : public std::exception {
public:
  parseException(const std::string& _file, const std::string& _line, int _lineno);
  virtual const char* what() const noexcept {
    return msg.c_str();
  }
private:
  std::string file;
  std::string line;
  std::string msg;
  int lineno;
};

/**
 * @class fileException
 * @brief An exception class for when a file can't be loaded
 */
class fileException : public std::exception {
public:
  fileException(const std::string& _file);
  virtual const char* what() const noexcept {
    return msg.c_str();
  }
private:
  std::string file;
  std::string msg;
};

/**
 * @class keyException
 * @brief An exception class for when a key can't be found
 */
class keyException : public std::exception {
public:
  keyException(const std::string& _key);
  virtual const char* what() const noexcept {
    return msg.c_str();
  }
private:
  std::string key;
  std::string msg;
};


/**
 * @class valueException
 * @brief An exception class for when a value can't be parsed
 */
class valueException : public std::exception {
public:
  valueException(const std::string& _key);
  virtual const char* what() const noexcept {
    return msg.c_str();
  }
private:
  std::string key;
  std::string msg;
};

/**
 * @class numericException
 * @brief An exception class for when a value can't be cast to a number
 */
class numericException : public std::exception {
public:
  numericException(const std::string& _key, const std::string& _value);
  virtual const char* what() const noexcept {
    return msg.c_str();
  }
private:
  std::string key;
  std::string value;
  std::string msg;
};

/**
 * @class stringException
 * @brief An exception class for when a value can't be cast to a string (perhaps it is missing quotes?)
 */
class stringException : public std::exception {
public:
  stringException(const std::string& _key, const std::string& _value);
  virtual const char* what() const noexcept {
    return msg.c_str();
  }
private:
  std::string key;
  std::string value;
  std::string msg;
};

///////////////////////////////
///// BUFFERS AND STREAMS /////
///////////////////////////////

std::ostream& debug();
std::ostream& verbose();
std::ostream& info();
std::ostream& warning();
std::ostream& err();
std::ostream& fatal();

const std::string ROJO("\x1b[91m"),
	  ROJO2("\x1b[31m"),
	  VERDE("\x1b[32m"),
	  AMARILLO("\x1b[33m"),
	  AZUL("\x1b[94m"),
	  AZULCLARO("\x1b[36m"), 
	  VIOLETA("\x1b[95m"),
	  SUBROJO("\x1b[101m"),
	  SUBVERDE("\x1b[102m"),
	  SUBAMARILLO("\x1b[103m"), 
	  SUBAZUL("\x1b[104m"),
	  SUBVIOLETA("\x1b[105m"),
	  SUBBLANCO("\x1b[107m"),
	  BLANCO("\x1b[37m"), 
	  BOLD("\x1b[1m"),
	  ENDC("\x1b[m"),
	  FAINT("\x1b[2m"),
	  UNDERLINE("\x1b[4m"),
	  BLINK("\x1b[5m"),
	  CLEARSCREEN("\x1b[2J"),
	  DEL1L("\x1b[2A\n\n");

/////////////////////
///// FUNCTIONS /////
/////////////////////

/// Namespace for parameters configurable via the options files
namespace Config {
  extern propertyMap property; ///< Map from key to a numeric value
  extern propertyStringMap stringproperty; ///< Map from key to a string value
  extern std::string ConfigFileName; ///< Name of the options file
  extern std::string BinaryDir; ///< Location of binaries on disk (for relative paths)
  extern int DBGLEVEL; ///< Debug level for executables (not used consistently)
}

/**
 * @brief Bind C++ type to an options file key
 * @details This imitates Gaudi's declareProperty command
 * @param[in] variablenamae Name of key in option file
 * @param[in] value Reference to C++ type to bind to
 */
template<typename T>
void declareProperty( const std::string& variablename, T& value);

#endif
/**
 * @}
 */
