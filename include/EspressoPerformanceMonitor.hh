/**
 * @addtogroup gr_fttools
 * @{
 */

/**
 * @file
 * @brief This header file defines the EspressoPerformanceMonitor class
 */

#ifndef EspressoPerformanceMonitor_TAGGING_H 
#define EspressoPerformanceMonitor_TAGGING_H 1

// Include files
#include "Tagger.hh"
#include "Tagging.hh"
#include "GLMModel.hh"
#include "RegressionFunctions.hh"

// C++ files
#include <memory>
#include <string>
#include <functional>

// Calibration files
using namespace Espresso;

// Forward declarations
class EspressoPerformanceMonitorImpl;

/**
 * @class EspressoPerformanceMonitor
 * @brief Class that calculates and prints tagging metrics
 */
class EspressoPerformanceMonitor {
public:

  // constructors
  
  /**
   * Standard constructor.
   */
  EspressoPerformanceMonitor();

  /**
   * Adds an event and set of tag decisions
   * 
   * @param[in] taggers List of tagging algorithm decisions
   * @param[in] decayFlavour Flavour of decaying B meson
   * @param[in] tau Lifetime of B meson
   * @param[in] tauErr Uncertainty on lifetime of B meson
   * @param[in] theTag set of tagging decisions for the event
   * @param[in] weight Weight of the signal event (e.g. an sWeight)
   */
  void increment(const Taggers& taggers, Flavour decayFlavour, double Btau, double BtauErr, double weight = 1.0);

  /**
   * @brief Returns a reference to the tagging result for the specified tagger
   * @param[in] type Type of the tagger for find the tagging result for
   */
  const TaggingResult& GetTaggingResult(Tagger::Type type) const;

  /**
   * Prints statistics, assuming that EspressoPerformanceMonitor::increment is done being called
   */
  virtual void printStats();

  /**
   * Returns the total number of events added
   */
  int N();

  /** 
   * Controls verbosity
   */
  void SetVerbose(bool _verbose);

private:
  std::shared_ptr<EspressoPerformanceMonitorImpl> pImpl;

};

#endif 


/**
 * @}
 */
