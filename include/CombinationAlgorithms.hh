/**
 * @addtogroup gr_fttools
 * @{
 */

/**
 * @file
 * @brief This header file defines template algorithms for performing tagger combinations
 */

#ifndef INCLUDE_COMBINATIONALGORITHMS_HH 
#define INCLUDE_COMBINATIONALGORITHMS_HH 1

#include "Tagger.hh"

/**
 * @brief Combines a set of taggers defined by a specific rule
 * @param[in] taggers List of taggers, not all of which should be included
 * @param[out] decision The overall tag decision
 * @param[out] omega The overall mistag, assuming no correlations
 * @param[in] Rule A function from Tagger::Type to a boolean that determines whether to include the tagger in the combination
 * @return The set of included taggers that actually fire for the given event.
 */
template <typename Rule>
Tagger::TypeSet performCombination(const Taggers& taggers, int& decision, double& omega, Rule rule) {

  Tagger::TypeSet typeSet;

  double omegaPlus = 1.0;
  double omegaMinus = 1.0;
  
  int count = 0;
  for (Tagger *t : taggers) {
    
    // Get values
    auto type = static_cast<Tagger::Type>(t->type());
    
    if (Tagger::Name(type) == "none")
      continue;
    
    if (not rule(type))
      continue;
    
    int _dec = t->decision();
    double _omega = t->omega();
    
    if (_dec and _omega < 0.5) {
      count++;
      typeSet.insert(type);
      double om = _omega;
      double invom = 1-_omega;
      if (_dec > 0) {
        omegaPlus *= om;
        omegaMinus *= invom;
      } else {
        omegaPlus *= invom;
        omegaMinus *= om;
      }
    }
  }
  
  if (count > 0) {
    double norm = omegaPlus+omegaMinus;
    omegaPlus /= norm;
    omegaMinus /= norm;
    decision = (omegaPlus < omegaMinus) ? 1 : ((omegaPlus > omegaMinus) ? -1 : 0);
    omega = std::min(omegaPlus, omegaMinus);
    return typeSet;
  } else {
    decision = 0;
    omega = 0.5;
    return typeSet;
  }
}

/**
 * @brief Combines the set of opposite-side taggers
 * @details The function Tagger::InOSComb(Tagger::Type) determines whether the tagger should be included
 * @param[in] taggers List of taggers, not all of which should be included
 * @param[out] decision The overall tag decision
 * @param[out] omega The overall mistag, assuming no correlations
 * @return The set of included taggers that actually fire for the given event.
 */
Tagger::TypeSet performOSCombination(const Taggers& taggers, int& decision, double& omega);

/**
 * @brief Combines the set of same-side taggers
 * @details The function Tagger::InSSComb(Tagger::Type) determines whether the tagger should be included
 * @param[in] taggers List of taggers, not all of which should be included
 * @param[out] decision The overall tag decision
 * @param[out] omega The overall mistag, assuming no correlations
 * @return The set of included taggers that actually fire for the given event.
 */
Tagger::TypeSet performSSCombination(const Taggers& taggers, int& decision, double& omega);

/**
 * @brief Second level composition of OS tagger and SS tagger
 * @details The function Tagger::InComb(Tagger::Type) determines whether the tagger should be included
 * @param[in] taggers List of taggers, not all of which should be included
 * @param[out] decision The overall tag decision
 * @param[out] omega The overall mistag, assuming no correlations
 * @return The set of included taggers that actually fire for the given event.
 */
Tagger::TypeSet performOSplusSSCombination(const Taggers& taggers, int& decision, double& omega);

#endif // INCLUDE_COMBINATIONALGORITHMS_HH


/**
 * @}
 */
