/**
 * @addtogroup gr_fttools
 * @{
 */

/**
 * @file
 * @brief This header file defines the EspressoPerformanceMonitorImpl class
 */

#ifndef EspressoPerformanceMonitorImpl_TAGGING_H 
#define EspressoPerformanceMonitorImpl_TAGGING_H 1

// C++ files
#include <iostream>
#include <sstream>
#include <iomanip>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <vector>
#include <map>

// ROOT Files
#include "TH1F.h"
#include "TGraph.h"
#include "TGraphErrors.h"

// Include files
#include "globals.hh"
#include "GaudiIOTools.hh"
#include "Tagger.hh"
#include "PrintUtilities.hh"

// Calibration files
#include "TagCollector.hh"
#include "GLMModel.hh"
#include "RegressionFunctions.hh"
#include "TaggingResultMetrics.hh"

using namespace Utilities;
using namespace Espresso;

template<typename T>
void safe_increment(TaggerMap<T>& vec, const Tagger::Type& type, T inc) {
  if (inc != 0) {
    auto it = vec.find(type);
    if (it != vec.end()) {
      it->second += inc;
    } else {
      vec[type] = inc;
    }
  }
}

template<typename T>
void safe_increment(TaggerPairMap<T>& vec, const Tagger::Pair& type, T inc) {
  if (inc != 0) {
    auto it = vec.find(type);
    if (it != vec.end())
      vec[type] += inc;
    else
      vec[type] = inc;
  }
}

const int _numTaggerSlots = 20;

/**
 * @class EspressoPerformanceMonitorImpl
 * @brief Implementation of EspressoPerformanceMonitor via the pimpl idiom
 */
class EspressoPerformanceMonitorImpl {
public:

  // constructors
  EspressoPerformanceMonitorImpl();

  void increment(const Taggers& taggers, Flavour decayFlavour, double Btau, double BtauErr, double weight = 1.0);

  const TaggingResult& GetTaggingResult(Tagger::Type type) const;

  void printStats();

  int N();

  void SetVerbose(bool _verbose) { verbose = _verbose; };

private:

  void setStyle();

  void add_tag(Tagger::Type type, Flavour decayFlavour, Flavour predFlavour, double dil, double weight, double Btau, double BtauErr);
  void add_tag_os(Tagger::TypeSet typeSet, Flavour decayFlavour, Flavour predFlavour, double dil, double weight, double Btau, double BtauErr);
  void add_tag_ss(Tagger::TypeSet typeSet, Flavour decayFlavour, Flavour predFlavour, double dil, double weight, double Btau, double BtauErr);
  void add_tag_osss(Tagger::TypeSet typeSet, Flavour decayFlavour, Flavour predFlavour, double dil, double weight, double Btau, double BtauErr);
  void fill_hist(Tagger::Type type, Flavour decayFlavour, Flavour predFlavour, double dil, double weight);

  template <class T>
  std::tuple<std::string,std::string,std::string> printPerformanceTable(const std::map<T,TaggingResult>& _taggerResults);

  template <class T>
  std::tuple<std::string,std::string> printPercentilesTable(const std::map<T,TaggingResult>& _taggerResults);

private:

  BDecayModel model;

  // SETTINGS
  bool verbose = false;

  // TAGGING COUNTERS
  double _numEvents;
  double _numEventsW2;
  double _effNumEvents;

  // TAGGER CORRELATIONS
  TaggerMap<double>  _tag_vector, _dec_vector, _dil_vector, _decdil_vector, _decdil2_vector;
  TaggerPairMap<double> _tag_matrix, _dec_matrix, _dec_comatrix_left, _dec_comatrix_right, _decdil_matrix;

  // CALIBRATION
  mutable TaggerMap<TagCollector> _taggerCollectors;
  TaggerSetMap<TagCollector> _taggerOSCombCollectors;
  TaggerSetMap<TagCollector> _taggerSSCombCollectors;
  TaggerSetMap<TagCollector> _taggerOSSSCombCollectors;

};

template <class T>
std::tuple<std::string,std::string,std::string> EspressoPerformanceMonitorImpl::printPerformanceTable(const std::map<T,TaggingResult>& _taggerResults) {
      
  std::stringstream output;
  std::stringstream olatex;
  std::stringstream ocsv;
  output << BOLD << VERDE
  << std::setw(15) << "Tagger"
  << std::setw(24) << "Tagging Rate"
  << std::setw(24) << "Raw Mistag"
  << std::setw(43) << "Eff Mistag"
  << std::setw(43) << "Tagging Power"
  // << std::setw(24) << "Weight Power"
  << ENDC << FAINT << std::endl;

  olatex << "\\begin{table}\n"
  << "\\centering\n"
  << "\\begin{tabular}{rlllll}\n";
  olatex << "\\multicolumn{1}{c}{Tagger}"
  << " & \\multicolumn{1}{c}{$\\epsilon$}"
  << " & \\multicolumn{1}{c}{$\\omega$}"
  << " & \\multicolumn{1}{c}{$\\epsilon \\langle D^2 \\rangle = \\epsilon \\left( 1 - 2 \\omega \\right)^2$}"
  << " \\\\ \n";
  
  // Write csv header
  ocsv << "TaggerName;TaggingEfficiency;TaggingEfficiencyStatUncert;";
  ocsv << "MistagRate;MistagRateStatUncert;";
  ocsv << "EffectiveMistag;EffectiveMistagStatUncert;EffectiveMistagCalibUncert;";
  ocsv << "TaggingPower;TaggingPowerStatUncert;TaggingPowerCalibUncert\n";
  ocsv << std::setprecision(15);

  for (auto& pair : _taggerResults) {
    
    T type = pair.first;
    const TaggingResult& res = pair.second;
    
    /* - Hack -
     * BTaggingAnalysis skips taggers with null decision
     * So, NumEvents = NumTaggedEvents for these
     */
    
    double tag_rate, tag_rate_err;
    std::tie(tag_rate, tag_rate_err, std::ignore) = TaggingRate(res,_numEvents,_effNumEvents);
    double tag_omega, tag_omega_err;
    std::tie(tag_omega, tag_omega_err, std::ignore) = MistagRate(res);
    double tag_effom, tag_effom_err, tag_effom_cal_err;
    std::tie(tag_effom,tag_effom_err,tag_effom_cal_err) = EffectiveMistag(res);
    double tag_power, tag_power_err, tag_power_cal_err;
    std::tie(tag_power, tag_power_err, tag_power_cal_err) = TaggingPower(res,_numEvents,_effNumEvents);

    output.setf(std::ios::fixed);

    output << BOLD << VERDE
           << std::setw(15) << Tagger::Name(type)
           << ENDC << FAINT
           << std::setw(25) << formatPercentWithErr(tag_rate,tag_rate_err)
           << std::setw(25) << formatPercentWithErr(tag_omega,tag_omega_err)
           << std::setw(45) << formatPercentWithDoubleErr(tag_effom,
							  tag_effom_err,"stat",
							  tag_effom_cal_err,"cal")
           << std::setw(45) << formatPercentWithDoubleErr(tag_power,
							  tag_power_err,"stat",
							  tag_power_cal_err,"cal")
      // << std::setw(25) << formatPercentWithErr(tag_power_actual,tag_power_actual_err)
           << std::endl;

    olatex << Tagger::LatexName(type)
           << "& " << formatPercentWithErr(tag_rate,tag_rate_err,true)
           << "& " << formatPercentWithDoubleErr(tag_effom,
                                                 tag_effom_err,"stat",
                                                 tag_effom_cal_err,"cal",true)
           << "& " << formatPercentWithDoubleErr(tag_power,
                                                 tag_power_err,"stat",
                                                 tag_power_cal_err,"cal",true)
           << "\\\\" << std::endl;
    ocsv << Tagger::Name(type) << ';';
    ocsv << tag_rate  << ';' << tag_rate_err  << ';'
         << tag_omega << ';' << tag_omega_err << ';'
         << tag_effom << ';' << tag_effom_err << ';' << tag_effom_cal_err << ';'
         << tag_power << ';' << tag_power_err << ';' << tag_power_cal_err << '\n';
  }
  olatex << "\\end{tabular}\n"
  << "\\end{table}\n";

  return std::make_tuple(output.str(),olatex.str(),ocsv.str());
}
  
template <class T>
std::tuple<std::string,std::string> EspressoPerformanceMonitorImpl::printPercentilesTable(const std::map<T,TaggingResult>& _taggerResults) {
  
  std::stringstream output;
  std::stringstream olatex;
  output << BOLD << VERDE
  << std::setw(15) << "Tagger"
  << std::setw(15) << "0%"
  << std::setw(15) << "18%"
  << std::setw(15) << "50%"
  << std::setw(15) << "82%"
  << std::setw(15) << "100%"
  << ENDC << FAINT << std::endl;
      
  olatex << "\\begin{table}\n"
  << "\\centering\n"
  << "\\begin{tabular}{rlllll}\n";
  olatex << "\\multicolumn{1}{c}{Tagger}"
  << " & 0\\%"
  << " & 18\\%"
  << " & 50\\%"
  << " & 82\\%"
  << " & 100\\%"
  << " \\\\ \n";
  
  for (auto& pair : _taggerResults) {
        
    T type = pair.first;
    const TaggingResult& res = pair.second;
        
    output.setf(std::ios::fixed);
        
    output << BOLD << VERDE
	   << std::setw(15) << Tagger::Name(type)
	   << ENDC << FAINT
	   << std::setw(15) << std::setprecision(3) << res.EtaQuantile(0.00)
	   << std::setw(15) << std::setprecision(3) << res.EtaQuantile(0.18)
	   << std::setw(15) << std::setprecision(3) << res.EtaQuantile(0.50)
	   << std::setw(15) << std::setprecision(3) << res.EtaQuantile(0.82)
	   << std::setw(15) << std::setprecision(3) << res.EtaQuantile(1.00)
	   << std::endl;
        
    olatex << Tagger::LatexName(type)
	   << "& " << std::setprecision(3) << res.EtaQuantile(0.00)
	   << "& " << std::setprecision(3) << res.EtaQuantile(0.18)
	   << "& " << std::setprecision(3) << res.EtaQuantile(0.50)
	   << "& " << std::setprecision(3) << res.EtaQuantile(0.82)
	   << "& " << std::setprecision(3) << res.EtaQuantile(1.00)
	   << "\\\\" << std::endl;
  }
  olatex << "\\end{tabular}\n"
  << "\\end{table}\n";
      
  return std::make_tuple(output.str(),olatex.str());
}

#endif 


/**
 * @}
 */
