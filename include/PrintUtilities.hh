/**
 * @addtogroup gr_fttools
 * @{
 */

/**
 * @file
 * @brief This header file defines small functions useful for formatting output
 */

#ifndef PRINTUTILITIES_HH
#define PRINTUTILITIES_HH

#include <string>

namespace Utilities {

  /**
   * @brief Format value as a percent
   * @param[in] value The number (expressed as a fraction, 1 = 100%)
   * @param[in] latex Whether to format in LaTeX
   */
  std::string formatPercent(double value, bool latex = false);
  
  /**
   * @brief Format a selection efficiency as a percent with uncertainty, calculating from raw numbers
   * @param[in] _numPass Number of events passing cut/selection
   * @param[in] _numTot Total number of events
   * @param[in] latex Whether to format in LaTeX
   */
  std::string formatPercentWithBinomErr(double _numPass, double _numTot, bool latex = false);
  
  /**
   * @brief Format value as a percent with uncertainty
   * @param[in] value The number (expressed as a fraction, 1 = 100%)
   * @param[in] error The absolute uncertainty (expressed as a fraction, 1 = 100%)
   * @param[in] latex Whether to format in LaTeX
   */
  std::string formatPercentWithErr(double value, double error, bool latex = false);

  /**
   * @brief Format value as a percent with two types of uncertainty (e.g. stat/syst)
   * @param[in] value The number (expressed as a fraction, 1 = 100%)
   * @param[in] error1 The first absolute uncertainty (expressed as a fraction, 1 = 100%)
   * @param[in] name1 The name of the first type of uncertainty (e.g. stat)
   * @param[in] error2 The second absolute uncertainty (expressed as a fraction, 1 = 100%)
   * @param[in] name2 The name of the second type of uncertainty (e.g. syst)
   * @param[in] latex Whether to format in LaTeX
   */
  std::string formatPercentWithDoubleErr(double value,
					 double error1, std::string name1,
					 double error2, std::string name2,
					 bool latex = false);

  /**
   * @brief Format a raw number with uncertainty
   * @param[in] value The number
   * @param[in] error The absolute uncertainty (expressed as a fraction, 1 = 100%)
   * @param[in] latex Whether to format in LaTeX
   */
  std::string formatNumberWithErr(double value, double error, bool latex = false);

  /**
   * @brief Format a raw number with two types of uncertainty (e.g. stat/syst)
   * @param[in] value The number
   * @param[in] error1 The first absolute uncertainty (expressed as a fraction, 1 = 100%)
   * @param[in] name1 The name of the first type of uncertainty (e.g. stat)
   * @param[in] error2 The second absolute uncertainty (expressed as a fraction, 1 = 100%)
   * @param[in] name2 The name of the second type of uncertainty (e.g. syst)
   * @param[in] latex Whether to format in LaTeX
   */
  std::string formatNumberWithDoubleErr(double value,
					double error1, std::string name1,
					double error2, std::string name2,
					bool latex = false);
}

#endif

/**
 * @}
 */
