/**
 * @addtogroup gr_fttools
 * @{
 */

/**
 * @file
 * @brief This header file contains a list of the global configurable options
 * @details These options are stored in the Config namespace, and are bound via declareProperty calls.
 */

#ifndef globals_H 
#define globals_H 1

#include <string>
#include "TString.h"

#include "GaudiIOTools.hh"
#include "Tagging.hh"
#include "RegressionFunctions.hh"
#include "GLMModel.hh"

const double PI = 3.1415926536;

namespace Config {

  // GENERIC
  extern int NumFiles;
  extern std::vector<TString> filelist;
  extern TString m_tupleName;
  extern int Nmax;
  extern TString Selection;
  
  // CALIBRATION SETTINGS
  extern bool DoCalibrations;
  extern int CalibrationDegree;
  extern bool UseInputModel;
  extern Espresso::GLMModel::GLMType CalibrationModel;
  extern Espresso::Regression::LinkType CalibrationLink;
  extern Espresso::CalibrationMode CalibrationMode;
  extern unsigned int CovCorrMethod;
  extern bool UseNewtonRaphson;
  extern bool SaveCalibrationsToXML;

  extern bool PerformOfflineCombination_OS;
  extern bool PerformOfflineCombination_SS;
  extern bool PerformOfflineCombination_OSplusSS;

  extern TString CalibratedOutputFile;

  extern double ProductionAsymmetry;
  
  extern double ResolutionGaussian1_A;
  extern double ResolutionGaussian2_A;
  extern double ResolutionGaussian3_A;
  extern double ResolutionGaussian1_B;
  extern double ResolutionGaussian2_B;
  extern double ResolutionGaussian3_B;
  extern double ResolutionGaussian1_C;
  extern double ResolutionGaussian2_C;
  extern double ResolutionGaussian3_C;
  extern double ResolutionGaussian1_Frac;
  extern double ResolutionGaussian2_Frac;

  // PHYSICS
  extern double DeltaM;
  extern double DeltaMErr;
  extern double Lifetime;
  extern double LifetimeErr;
  extern double DeltaGamma;
  extern double DeltaGammaErr;

  // SIMPLEEVALUATOR
  extern bool WriteTaggerDecisionTable;
  extern int NumKernelSmoothingPoints;
  extern double KernelSmoothingWidth;
  extern bool DrawOscillationPlots;
  extern double OscillationPlotsMaximum;

  /* Weight formula */
  extern TString WeightFormula;

  /* branch names */
  extern TString BranchID;
  extern TString BranchTau;
  extern TString BranchTauErr;
  extern TString TauUnits;

  /* data type */
  extern TString TypeID;
  extern TString TypeTau;
  extern TString TypeTauErr;
  extern TString TypeT;
  extern TString TypeTaggerType;
  extern TString TypeTaggerDecision;
  extern TString TypeTaggerOmega;

  /* use or not */
  extern bool UseWeight;
  extern bool UseTau;
  extern bool UseTauErr;

  // BTAEVALUATOR

  // EVALUATOR

  extern TString NNetTrain, MLPtype, VarTransform;
  extern int     requireL0, requireHlt1, requireHlt2, requireTis, UseModBtime, usesWeights, useTMVA, useDaVinciTaggers;
  extern bool    IsControlChannel, checkDV, removeFiles;
  extern double  m_IPPU_cut, m_thetaMin, m_distphi_cut, m_Pmaxtot_cut, m_Ptmaxtot_cut;
  extern TString histoFileName;
  extern TString trainFileName;

  // TESTSTATCAL
  extern unsigned int RandomSeed;
  extern bool OscillationTest;
  extern bool AsymmetryNullTest;
  extern bool AsymmetryFittedTest;
  extern bool MetricNullTest;
  extern bool MetricFittedTest;
  extern bool CalibrationTest;
  extern int NumberOfToys;
  extern int EventsPerToy;
  extern double BackgroundFraction;
  extern double SidebandRatio;
  extern double ToyAsymmetry;
  extern bool CreateSmearedCalibration;
  extern int ToySmearDegree;
  extern Espresso::GLMModel::GLMType ToySmearModel;
  extern Espresso::Regression::LinkType ToySmearLink;
  extern double ToySmearP0Scale;
  extern double ToySmearP1Scale;
  extern double ToySmearP2Scale;
  extern double ToySmearP3Scale;
  extern double ToySmearP4Scale;
  extern double ToySmearP5Scale;
  extern double ToySmearP6Scale;
  extern double ToySmearP7Scale;
  extern double ToySmearP8Scale;
  extern double ToySmearP9Scale;
  extern double ToySmearP10Scale;
  extern double ToySmearDeltaP0Scale;
  extern double ToySmearDeltaP1Scale;
  extern double ToySmearDeltaP2Scale;
  extern double ToySmearDeltaP3Scale;
  extern double ToySmearDeltaP4Scale;
  extern double ToySmearDeltaP5Scale;
  extern double ToySmearDeltaP6Scale;
  extern double ToySmearDeltaP7Scale;
  extern double ToySmearDeltaP8Scale;
  extern double ToySmearDeltaP9Scale;
  extern double ToySmearDeltaP10Scale;

  // PLOTTING
  extern TString PlotLabel;
  extern TString PlotExtension;
  extern bool PlotTitle;
  extern bool PlotStatBox;

  // Read the parameters
  void readParameters();
}

#endif

/**
 * @}
 */
