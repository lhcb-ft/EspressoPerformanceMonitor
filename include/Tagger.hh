/**
 * @addtogroup gr_fttools
 * @{
 */

/**
 * @file
 * @brief This header file defines the Tagger class and associated enums and types
 */

#ifndef Tagger_H
#define Tagger_H 1

#include <memory>
#include <string>
#include <vector>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>

#include <TString.h>

#include "StandardCalibration.hh"

class Tagger;
using Taggers = std::vector<Tagger*>;

/**
 * @class Tagger
 * @brief A simple class representing a single Tag decision (Tag would be a better name than Tagger)
 */

class Tagger {

 public:

  /// This enum lists the various types of tagging algorithms
  enum Type { 
    None, ///< Placeholder for a null tagger
    Placeholder, ///< Another type of placeholder (could be removed)

    Toy, ///< The toy tagger
    OS_Muon, ///< OS Muon tagger
    OS_Electron, ///< OS Electron tagger
    OS_Kaon, ///< Cut-based OS Kaon tagger
    OS_nnetKaon, ///< Neural-net based OS Kaon tagger
    VtxCharge, ///< Vertex charge tagger
    OS_Charm, ///< OS Charm tagger
    SS_Kaon, ///< Cut-based SS Kaon tagger
    SS_nnetKaon, ///< Neural-net based SS Kaon tagger
    SS_Pion, ///< Cut-based SS Pion tagger
    SS_PionBDT, ///< BDT based SS Pion tagger
    SS_Proton, ///< BDT based SS Proton tagger

    Inclusive, ///< Inclusive tagger

    OS_Combination, ///< OS combination
    SS_Combination, ///< SS combination (e.g. kaon or pion + proton)
    Combination, ///< OS comb. + SS comb.

    Type_BMIN=None,
    Type_BMAX=Placeholder,
    Type_SMIN=Toy,
    Type_SMAX=SS_Proton,
    Type_CMIN=OS_Combination,
    Type_CMAX=Combination,
    Type_MAX=Combination
  };

  /// Underlying type of the Tagger::Type enum
  using Type_t = typename std::underlying_type<Tagger::Type>::type;
  
  /// A set of Tagger::Types, useful for combination decisions
  using TypeSet = std::set< Tagger::Type >;

  /// A pair of Tagger::Types, useful for correlations
  using Pair = std::pair<Tagger::Type, Tagger::Type>;
  
  // constructors

  /// Default constructor
  Tagger();

  /**
   * @brief Constructor taking an integer type
   * @param[in] type Type of tagger, as an integer (could be replaced with Tagger::Type)
   */
  Tagger(int type);

  /**
   * @brief Constructor taking an integer type and tagging decision
   * @param[in] type Type of tagger, as an integer (could be replaced with Tagger::Type)
   * @param[in] decision &plusmn; flavor decision (could be replaced with Espresso::Flavour)
   * @param[in] omega Mistag probability of the tag decision
   */
  Tagger(int type, int decision, double omega );

  /// Standard destructor
  ~Tagger() {} ///< Destructor

  /// Resets the type, decision, and mistag of the tagger
  virtual void reset();

  /// Returns the flavour decision (could be replaced with Espresso::Flavour)
  int    decision() { return m_Decision; }

  /// Returns the tagger type (could be replaced with Tagger::Type)
  int    type() { return m_Type; }

  /// Returns the uncalibrated mistag probability
  double eta();

  /// Returns the calibrated mistag probability
  double omega();

  /// Sets the uncalibrated mistag probability (a misnomer!)
  void setOmega(double a)   { m_Omega = a; }

  /// Sets the tag decision (could be replaced with Espresso::Flavour)
  void setDecision(int a)   { m_Decision = a; }

  /// Sets the tagger type (could be repalced with Tagger::Type)
  void setType(int a)       { m_Type = a; }

  /// Prints the tagger to std::cout
  void Print();

  /// Returns the type of tagger corresponding to the BTaggingAnalysis code
  static Tagger::Type GetTaggerFromBTACode(int code) {
    auto it = m_numBTA.find(code);
    if (it != m_numBTA.end())
      return it->second;
    else
      return Tagger::None;
  }

  /// Returns a short name for the tagger (by reference)
  static std::string& ShortName(Tagger::Type m_Type) {
    return m_shortNameMap[m_Type];
  }

  /// Returns a verbose name for the tagger (by reference)
  static std::string& Name(Tagger::Type m_Type) {
    return m_nameMap[m_Type];
  }

  /// Returns a verbose name for a set of taggers
  static std::string Name(const Tagger::TypeSet& typeSet);

  /// Returns a name for the tagger formatted for ROOT
  static std::string& RootName(Tagger::Type m_Type) {
    return m_rootNameMap[m_Type];
  }

  /// Returns a name for the tagger formatted for LaTeX
  static std::string& LatexName(Tagger::Type m_Type) {
    return m_latexNameMap[m_Type];
  }

  /// Returns a name for the set of taggers formatted for LaTeX
  static std::string LatexName(const Tagger::TypeSet& typeSet);

  /// Returns whether the tagger is active for this ROOT file (by reference)
  static bool& IsActive(Tagger::Type m_Type) {
    return m_isActive[m_Type];
  }

  /// Returns the ROOT branch for this tagger's flavour decision (by reference)
  static TString& BranchDec(Tagger::Type m_Type) {
    return m_BranchDec[m_Type];
  }

  /// Returns the ROOT branch for this tagger's mistag (by reference)
  static TString& BranchProb(Tagger::Type m_Type) {
    return m_BranchProb[m_Type];
  }

  /// Returns the ROOT branch type for this tagger's flavour decision (by reference)
  static TString& TypeDec(Tagger::Type m_Type) {
    return m_TypeDec[m_Type];
  }

  /// Returns the ROOT branch type for this tagger's mistag (by reference)
  static TString& TypeProb(Tagger::Type m_Type) {
    return m_TypeProb[m_Type];
  }

  /// Returns whether the specified tagger belongs to the OS combination (by reference)
  static bool& InOSComb(Tagger::Type m_Type) {
    return m_inOSComb[m_Type];
  }
  
  /// Returns whether the specified tagger belongs to the SS combination (by reference)
  static bool& InSSComb(Tagger::Type m_Type) {
    return m_inSSComb[m_Type];
  }
  
  /// Returns whether the specified tagger belongs to the OS+SS combination (by reference)
  static bool& InComb(Tagger::Type m_Type) {
    return m_inComb[m_Type];
  }

  /**
   * @brief Returns a weight for the specified tagger in a combination
   * @details This idea is not yet fully pursued in the EPM,
   * but a general replacement for the naive combination algorithm,
   * which is equivalent to
   * \f[ \log \left( \frac{\omega}{1-\omega} \right) = \sum_{t} \log \left( \frac{\omega_{t}}{1-\omega_{t}} \right), \f]
   * is a logistic regression of the form
   * \f[ \log \left( \frac{\omega}{1-\omega} \right) = \beta_0 + \sum_{t} \beta_t \log \left( \frac{\omega_{t}}{1-\omega_{t}} \right). \f]
   * The various mistag probabilities appear in the combination with powers &beta;<sub>t</sub>.
   * The code here isn't used, but is left as a reminder of this potential improvement.
   */
  static double& CombPow(Tagger::Type m_Type) {
    return m_combPowMap[m_Type];
  }

  /// Returns whether to write calibrated values for the tagger to a ROOT file (by reference)
  static bool& Write(Tagger::Type m_Type) {
    return m_write[m_Type];
  }

  /// Returns the number of bins to display in &omega; vs. &eta; plots (by reference)
  static int& Binning(Tagger::Type m_Type) {
    return m_binningMap[m_Type];
  }
  
  /// Returns the StandardCalibration object associated with the tagger (by reference)
  static Espresso::StandardCalibration& StandardCalibration(Tagger::Type m_Type) {
    return m_calMap[m_Type];
  }

  /// Returns the location of the Calibration archive associated with the tagger (by reference)
  static TString& CalibrationArchive(Tagger::Type m_Type) {
    return m_archiveMap[m_Type];
  }

  /// Returns the Calibration object associated with the tagger (by pointer)
  static std::shared_ptr<Espresso::Calibration> Calibration(Tagger::Type m_Type);

 private:
  int m_Type;
  double m_Omega;
  int m_Decision;

  static std::map<int,Type> m_numBTA;
  static std::map<Type,std::string> m_shortNameMap;
  static std::map<Type,std::string> m_nameMap;
  static std::map<Type,std::string> m_latexNameMap;
  static std::map<Type,std::string> m_rootNameMap;
  static std::map<Type,bool> m_isActive;
  static std::map<Type,TString> m_BranchDec;
  static std::map<Type,TString> m_BranchProb;
  static std::map<Type,TString> m_TypeDec;
  static std::map<Type,TString> m_TypeProb;
  static std::map<Type,bool> m_inComb;
  static std::map<Type,bool> m_write;
  static std::map<Type,bool> m_inOSComb;
  static std::map<Type,bool> m_inSSComb;
  static std::map<Type,TString> m_archiveMap;
  static std::map<Type,Espresso::StandardCalibration> m_calMap;
  static std::map<Type,std::shared_ptr<Espresso::Calibration> > m_gcalMap;
  static std::map<Type,double> m_combPowMap;
  static std::map<Type,int> m_binningMap;

};

namespace std {

  template<>
  struct less<Tagger::TypeSet> {
    bool operator()(const Tagger::TypeSet& lhs, const Tagger::TypeSet& rhs) const {
      unsigned long key_lhs = 0L;
      unsigned long key_rhs = 0L;
      for (auto& val : lhs) {
	key_lhs += 1<<val;
      }
      for (auto& val : rhs) {
	key_rhs += 1<<val;
      }
      return (key_lhs < key_rhs);
    }
  };

}
  
template<typename T>
using TaggerMap = std::map<Tagger::Type, T>;

template<typename T>
using TaggerSetMap = std::map< Tagger::TypeSet, T >;

template<typename T>
using TaggerPairMap = std::map< Tagger::Pair, T >;


#endif 

/**
 * @}
 */
